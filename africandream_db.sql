-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 01, 2019 at 01:26 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `africandream_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `absorbed_drinks`
--

DROP TABLE IF EXISTS `absorbed_drinks`;
CREATE TABLE IF NOT EXISTS `absorbed_drinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drink_id` (`drink_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absorbed_drinks`
--

INSERT INTO `absorbed_drinks` (`id`, `drink_id`, `qty`, `location`, `emp_id`, `day`) VALUES
(2, 5, 5, 'conference', 1, '2018-05-17'),
(3, 15, 5, 'conference', 1, '2018-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `complementary`
--

DROP TABLE IF EXISTS `complementary`;
CREATE TABLE IF NOT EXISTS `complementary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complementary`
--

INSERT INTO `complementary` (`id`, `drink_id`, `quantity`, `receiver`, `saler_id`, `day`) VALUES
(1, 5, 10, 'juma', 1, '2018-05-08'),
(2, 0, 10, 'cafe', 1, '2018-05-08'),
(3, 11, 4, 'juma', 1, '2018-05-08'),
(4, 5, 10, 'macha', 1, '2018-05-08'),
(5, 5, 60, 'cafe', 1, '2018-05-08'),
(6, 5, 10, 'cafe', 1, '2018-05-08'),
(7, 5, 1, 'Duma', 1, '2018-05-10'),
(8, 5, 50, 'hamisi', 1, '2018-05-14'),
(9, 15, 2, 'Ali', 1, '2018-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

DROP TABLE IF EXISTS `drinks`;
CREATE TABLE IF NOT EXISTS `drinks` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `price` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cost` float NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `name` (`name`),
  KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`name`, `category`, `size`, `item_id`, `price`, `cost`) VALUES
('Safari', 'Bear', '357', 15, '2000', 1500),
('Savana', 'Bear', '478', 18, '5000', 2000),
('Novida', 'Soda', '23', 19, '1000', 500),
('Balimi', 'Bear', '100', 20, '1500', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `drinksales`
--

DROP TABLE IF EXISTS `drinksales`;
CREATE TABLE IF NOT EXISTS `drinksales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_time` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `amount` int(20) NOT NULL,
  `saler_id` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `customer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `item_id` (`item_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drinksales`
--

INSERT INTO `drinksales` (`sale_id`, `sale_time`, `name`, `item_id`, `category`, `amount`, `saler_id`, `price`, `status`, `customer`) VALUES
(2, '2018-05-18', 'Savana', 12, 'Bear', 2, '1', 5000, 1, 'Duma'),
(3, '2018-05-18', 'Maji Kilimanjaro', 11, 'Madogo', 5, '1', 5000, 1, 'Ema'),
(4, '2018-05-19', 'Bavaria', 5, 'Bear', 8, '1', 20000, 1, 'Hamisa'),
(5, '2018-05-19', 'Bavaria', 5, 'Bear', 1, '1', 2500, 1, 'Alfa'),
(6, '2018-05-19', 'Bavaria', 5, 'Bear', 2, '1', 5000, 1, 'Hamisi'),
(7, '2018-05-23', 'Bavaria', 5, 'Bear', 8, '1', 2500, 1, 'Babuu'),
(8, '2018-05-24', 'Deperados', 13, 'Bear', 3, '1', 9000, 1, 'John'),
(9, '2018-05-24', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Avin'),
(10, '2018-05-24', 'Savana', 14, 'Joice', 2, '1', 6000, 1, 'Babuu'),
(11, '2018-05-24', 'Savana', 14, 'Joice', 2, '1', 6000, 1, 'Babuu'),
(12, '2018-05-24', 'Deperados', 13, 'Bear', 7, '1', 21000, 1, 'Kevi'),
(13, '2018-05-25', 'Savana', 14, 'Joice', 5, '1', 15000, 1, 'KELU'),
(14, '2018-05-25', 'Savana', 14, 'Joice', 2, '1', 3000, 1, 'Babuu'),
(15, '2018-05-28', 'Savana', 14, 'Joice', 8, '1', 24000, 1, 'Omar'),
(16, '2018-05-28', 'Savana', 14, 'Joice', 8, '1', 24000, 1, 'omar'),
(17, '2018-05-28', 'Savana', 14, 'Joice', 8, '1', 24000, 1, 'Babuu'),
(18, '2018-05-28', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Babuu'),
(19, '2018-05-28', 'Safari', 15, 'Bear', 5, '1', 10000, 1, 'Osam'),
(20, '2018-05-28', 'Savana', 14, 'Joice', 2, '1', 6000, 1, 'Osam'),
(21, '2018-05-28', 'Safari', 15, 'Bear', 5, '1', 2000, 1, 'Kevi'),
(22, '2018-05-29', 'Safari', 15, 'Bear', 5, '1', 10000, 1, 'Alfa'),
(23, '2018-05-28', 'Savana', 14, 'Joice', 2, '1', 6000, 1, 'Babuu'),
(24, '2018-05-28', 'Deperados', 13, 'Bear', 3, '1', 9000, 1, 'Babuu'),
(25, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Alfa'),
(26, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'ali'),
(27, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Babuu'),
(28, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Babuu'),
(29, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Babuu'),
(30, '2018-05-29', 'Savana', 14, 'Joice', 3, '1', 9000, 1, 'Babuu'),
(31, '2018-05-29', 'Deperados', 13, 'Bear', 7, '1', 21000, 1, 'Kevi'),
(32, '2018-05-29', 'Deperados', 13, 'Bear', 3, '1', 9000, 1, 'Alfa'),
(33, '2018-05-29', 'Deperados', 13, 'Bear', 3, '1', 9000, 1, 'Ema'),
(34, '2018-05-31', 'Savana', 14, 'Joice', 1, '1', 3000, 1, 'Babuu'),
(35, '2018-05-31', 'Savana', 14, 'Joice', 2, '1', 3000, 0, 'Alfa'),
(36, '2018-05-31', ' Kilimanjaro', 16, 'ndogo', 2, '1', 4000, 1, 'Alfa'),
(37, '2018-05-31', 'Kilimanjaro', 17, 'kubwa', 5, '1', 15000, 1, 'HAMZA'),
(38, '2018-05-31', ' Kilimanjaro', 16, 'ndogo', 2, '1', 4000, 1, 'Kevi'),
(39, '2018-05-31', 'Kilimanjaro', 17, 'kubwa', 2, '1', 6000, 1, 'John'),
(40, '2018-05-31', ' Kilimanjaro', 16, 'ndogo', 2, '1', 4000, 1, 'Babuu'),
(41, '2018-06-30', ' Kilimanjaro', 16, 'ndogo', 3, '1', 6000, 1, 'joy'),
(42, '2018-06-20', 'Savana', 14, 'Joice', 1, '1', 3000, 1, 'Avin'),
(43, '2018-06-20', 'Safari', 15, 'Bear', 5, '1', 10000, 1, 'Kevi'),
(44, '2018-06-29', ' Kilimanjaro', 16, 'Ndogo', 3, '1', 6000, 1, 'abu'),
(45, '2018-06-30', 'Kilimanjaro', 17, 'kubwa', 1, '1', 3000, 1, 'ema'),
(46, '2018-07-01', ' Kilimanjaro', 16, 'Ndogo', 1, '1', 2000, 1, 'joy'),
(47, '2018-07-02', ' Kilimanjaro', 16, 'Ndogo', 1, '1', 2000, 1, 'abu'),
(48, '2018-07-04', ' Kilimanjaro', 16, 'Ndogo', 1, '1', 2000, 1, 'macha'),
(49, '2018-07-04', 'Safari', 15, 'Bear', 2, '1', 4000, 1, 'joy'),
(50, '2018-07-10', 'Safari', 15, 'Bear', 2, '1', 4000, 1, 'ema'),
(51, '2018-07-10', 'Safari', 15, 'Bear', 2, '1', 4000, 1, 'sdsd'),
(52, '2018-07-19', 'Safari', 15, 'Bear', 2, '1', 4000, 1, 'ema'),
(53, '2018-07-19', ' Kilimanjaro', 16, 'Ndogo', 8, '1', 16000, 1, 'macha'),
(54, '2018-07-20', ' Kilimanjaro', 16, 'Ndogo', 2, '1', 4000, 1, 'joy'),
(55, '2018-07-20', 'Safari', 15, 'Bear', 4, '1', 8000, 1, 'joy'),
(56, '2018-07-21', 'Savana', 18, 'Bear', 5, '1', 25000, 1, 'joy'),
(57, '2018-07-31', 'Safari', 15, 'Bear', 3, '1', 6000, 0, 'lonac'),
(58, '2018-08-01', 'Novida', 19, 'Soda', 2, '1', 2000, 1, 'abu'),
(59, '2018-08-01', 'Kilimanjaro', 17, 'kubwa', 2, '1', 6000, 1, '5'),
(60, '2018-08-01', 'Savana', 18, 'Bear', 5, '1', 25000, 1, 'joy');

-- --------------------------------------------------------

--
-- Table structure for table `drink_order`
--

DROP TABLE IF EXISTS `drink_order`;
CREATE TABLE IF NOT EXISTS `drink_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `saler_id` (`saler_id`),
  KEY `drink_id` (`drink_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drink_order`
--

INSERT INTO `drink_order` (`order_id`, `drink_id`, `qty`, `price`, `saler_id`, `customer`, `day`, `status`) VALUES
(1, 16, 1, 2000, 1, 'abu', '2018-07-02', 1),
(5, 19, 2, 2000, 1, 'joy', '2018-08-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `drink_receives`
--

DROP TABLE IF EXISTS `drink_receives`;
CREATE TABLE IF NOT EXISTS `drink_receives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drink_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `order_cost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `drink_id` (`drink_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drink_receives`
--

INSERT INTO `drink_receives` (`id`, `drink_id`, `qty`, `day`, `emp_id`, `order_cost`) VALUES
(1, 5, 15, '2018-05-16', 1, 20000),
(2, 5, 30, '2018-05-16', 1, 50000),
(3, 15, 10, '2018-07-21', 1, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`firstname`, `lastname`, `username`, `password`, `employee_id`) VALUES
('Abraham', 'Macha', 'Admin', '827ccb0eea8a706c4c34a16891f84e7b', 1),
('Mery', 'Suleman', 'mery', 'd645710ed9e8cfacb6e25d480ea8ccea', 14);

-- --------------------------------------------------------

--
-- Table structure for table `expenditure`
--

DROP TABLE IF EXISTS `expenditure`;
CREATE TABLE IF NOT EXISTS `expenditure` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `des` varchar(500) NOT NULL,
  `cost` float NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` varchar(11) NOT NULL,
  `source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`exp_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenditure`
--

INSERT INTO `expenditure` (`exp_id`, `des`, `cost`, `day`, `emp_id`, `source`) VALUES
(16, 'Travel to town', 200000, '2018-07-27', '1', 'Staff_Transport'),
(15, 'Rent', 300000, '2018-07-27', '1', 'Construction'),
(14, 'Tigo', 50000, '2018-07-27', '1', 'Internet'),
(17, 'ELectricity', 400000, '2018-07-27', '1', 'Electricity'),
(18, 'Juma Na Hawa', 1000000, '2018-07-27', '1', 'Salary'),
(19, 'ALL employees', 1000000, '2018-07-27', '1', 'Salary');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
CREATE TABLE IF NOT EXISTS `food` (
  `food_id` int(11) NOT NULL AUTO_INCREMENT,
  `foodname` varchar(500) NOT NULL,
  `category` varchar(500) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`food_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_id`, `foodname`, `category`, `price`) VALUES
(3, 'Bagia', 'Bites', 300),
(5, 'Wali Kuku', 'msos', 15000);

-- --------------------------------------------------------

--
-- Table structure for table `foodsales`
--

DROP TABLE IF EXISTS `foodsales`;
CREATE TABLE IF NOT EXISTS `foodsales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_time` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `amount` int(20) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `customer` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foodsales`
--

INSERT INTO `foodsales` (`sale_id`, `sale_time`, `name`, `item_id`, `category`, `amount`, `saler_id`, `price`, `status`, `customer`) VALUES
(1, '2018-09-05', 'Bagia', 3, 'Bites', 1, 1, 300, 1, 'joy'),
(2, '2018-09-05', 'Wali Kuku', 5, 'msos', 2, 1, 30000, 1, 'Suzi'),
(3, '2018-09-08', 'Wali Kuku', 5, 'msos', 2, 1, 30000, 0, 'Osca'),
(4, '2018-09-08', 'Wali Kuku', 5, 'msos', 1, 1, 15000, 2, 'Vicky');

-- --------------------------------------------------------

--
-- Table structure for table `food_order`
--

DROP TABLE IF EXISTS `food_order`;
CREATE TABLE IF NOT EXISTS `food_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `food_id` (`food_id`),
  KEY `saler_id` (`saler_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_order`
--

INSERT INTO `food_order` (`order_id`, `food_id`, `qty`, `price`, `saler_id`, `customer`, `day`, `status`) VALUES
(1, 3, 5, 1500, 1, 'Alfa', '2018-05-20', 1),
(2, 5, 1, 15000, 1, 'ema', '2018-07-02', 1),
(3, 5, 1, 15000, 1, 'b', '2018-08-27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

DROP TABLE IF EXISTS `income`;
CREATE TABLE IF NOT EXISTS `income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `des` varchar(100) NOT NULL,
  `fund` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`income_id`),
  KEY `emp_id` (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`income_id`, `des`, `fund`, `day`, `emp_id`, `source`) VALUES
(7, 'Tigo', 500000, '2018-07-26', '1', 'CONFERENCE P'),
(6, 'voda', 500000, '2018-07-26', '1', 'SWIMMING POOL'),
(8, 'Tigo', 600000, '2018-07-26', '1', 'BOARD ROOM');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `classname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `classname` (`classname`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `name`, `status`, `classname`) VALUES
(1, 'Drinks', 1, 'Drinks'),
(2, 'Accomodation', 1, 'Accomodation'),
(3, 'Counter', 1, 'Counter'),
(4, 'Staff', 1, 'Staff_class'),
(5, 'Receiving', 1, 'Receiving_class'),
(6, 'Restaurant', 1, 'Restaurant'),
(13, 'Food_orders', 1, 'Food_orders'),
(8, 'Reports', 1, 'Reports'),
(9, 'Expenditure', 1, 'Expenditure'),
(10, 'Other_income', 1, 'Other_income'),
(11, 'Sale_food', 1, 'Sale_food'),
(12, 'Drinks_order', 1, 'Drinks_order');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module_id`, `employee_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 6, 1),
(5, 5, 1),
(51, 13, 1),
(7, 4, 1),
(34, 10, 1),
(33, 9, 1),
(32, 8, 1),
(65, 2, 14),
(64, 3, 14),
(63, 12, 14),
(45, 11, 1),
(48, 12, 1),
(66, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `fee` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`room_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `name`, `category`, `fee`, `status`) VALUES
(6, 'GT 200', 'Executive Deluxe', '5000', 'EMPTY'),
(8, 'AB 100', 'Deluxe', '500000', 'EMPTY'),
(5, 'G20', 'Deluxe', '30000', 'FULL'),
(10, 'TX 100', 'Suite', '70000', 'EMPTY'),
(11, 'Anex', 'Executive Deluxe', '500000', 'EMPTY');

-- --------------------------------------------------------

--
-- Table structure for table `room_booking`
--

DROP TABLE IF EXISTS `room_booking`;
CREATE TABLE IF NOT EXISTS `room_booking` (
  `tras_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `day` varchar(50) NOT NULL,
  `booked_day` varchar(50) NOT NULL,
  PRIMARY KEY (`tras_id`),
  KEY `saler_id` (`saler_id`),
  KEY `room_id` (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_booking`
--

INSERT INTO `room_booking` (`tras_id`, `room_id`, `saler_id`, `customer`, `price`, `day`, `booked_day`) VALUES
(3, 2, 1, 'b', 20000, '2018-07-11', '2018-07-10'),
(4, 7, 1, 'lonac', 73747, '2018-07-11', '2018-07-10'),
(5, 4, 1, 'xsxsx', 1000, '2018-07-11', '2018-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `soldroom`
--

DROP TABLE IF EXISTS `soldroom`;
CREATE TABLE IF NOT EXISTS `soldroom` (
  `roomsale_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_ID` int(11) NOT NULL,
  `roomsaler_id` int(11) NOT NULL,
  `roomcustomer` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `checkout_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`roomsale_id`),
  KEY `room_ID` (`room_ID`),
  KEY `roomsaler_id` (`roomsaler_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soldroom`
--

INSERT INTO `soldroom` (`roomsale_id`, `room_ID`, `roomsaler_id`, `roomcustomer`, `price`, `day`, `status`, `checkout_time`) VALUES
(1, 6, 1, 'abu', 5000, '2018-06-27', 0, NULL),
(2, 2, 1, 'abu', 20000, '2018-06-27', 0, NULL),
(3, 6, 1, 'abu', 5000, '2018-06-27', 0, NULL),
(4, 2, 1, 'ema', 20000, '2018-07-01', 0, NULL),
(5, 2, 1, 'macha', 20000, '2018-07-02', 0, NULL),
(6, 6, 1, 'joy', 5000, '2018-07-02', 0, NULL),
(7, 6, 1, 'macha', 5000, '2018-07-04', 0, NULL),
(8, 6, 1, 'joy', 5000, '2018-07-07', 0, NULL),
(9, 6, 1, 'lonac', 5000, '2018-07-07', 0, NULL),
(10, 6, 1, 'lonac', 5000, '2018-07-10', 0, ''),
(11, 5, 1, 'baraka', 30000, '2018-07-07', 0, '2018-07-09'),
(12, 6, 1, 'edccd', 5000, '2018-07-10', 0, '2018-07-10'),
(13, 4, 1, 'xcx', 1000, '2018-07-10', 0, ''),
(14, 6, 1, 'dwsds', 5000, '2018-07-10', 0, ''),
(15, 4, 1, 'zdsdsd', 1000, '2018-07-10', 0, '2018-07-10'),
(16, 5, 1, 'b n ', 30000, '2018-07-10', 0, ''),
(17, 4, 1, 'dfdf', 1000, '2018-07-10', 0, ''),
(18, 4, 1, 'dcvdfv', 1000, '2018-07-10', 0, '2018-07-10'),
(19, 6, 1, 'zxca', 5000, '2018-07-10', 0, ''),
(20, 6, 1, 'joy', 5000, '2018-08-30', 0, '2018-08-30'),
(21, 6, 1, 'lonac', 5000, '2018-08-30', 0, ''),
(22, 5, 1, 'abu', 30000, '2018-08-30', 2, ''),
(23, 10, 1, 'macha', 70000, '2018-09-02', 0, ''),
(24, 8, 1, 'kadiva', 300000, '2018-12-31', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `spoil`
--

DROP TABLE IF EXISTS `spoil`;
CREATE TABLE IF NOT EXISTS `spoil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `saler_id` int(11) NOT NULL,
  `day` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `saler_id` (`saler_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spoil`
--

INSERT INTO `spoil` (`id`, `item_id`, `qty`, `saler_id`, `day`) VALUES
(1, 5, 2, 1, '2018-05-09'),
(2, 5, 1, 1, '2018-05-09'),
(3, 10, 2, 1, '2018-05-10'),
(4, 12, 50, 1, '2018-05-14'),
(5, 18, 3, 1, '2018-07-20'),
(6, 18, 7, 1, '2018-07-20'),
(7, 18, 2, 1, '2018-08-14'),
(8, 15, 3, 1, '2018-08-14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
