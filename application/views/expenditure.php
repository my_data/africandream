<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('expenditure/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('expenditure/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	
			<h5 style="text-align: center;text-transform: uppercase; margin-bottom: 2%;"><strong>
			<?php echo $this->lang->line('expenditures',FALSE);?></strong></h5>
		<table class="table  table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th>
							<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search" aria-label="Search" name="keyword" disabled />
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control" disabled="">
						
						</select></th>
						<!-- add new expenditure -->
					<th><div class="pull-right">
					<a class="btn btn-warning" data-toggle="modal" href='#modal-id1'><i class="fa fa-plus"></i>&nbspExpenditure</a>
					<div class="modal fade" id="modal-id1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color: #337ab7;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;">ADD</h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('expenditure/save',$attrib);?>
										<div class="form-group">
										<label for=""><?php echo $this->lang->line('description',FALSE);?></label>
											<textarea name="description" class="form-control input-sm" rows="3" required="required" autofocus="autofocus"></textarea>
										</div>
										<div class="form-group">
										<label>Source</label>
											<select name="source" id="input" class="form-control" required>
												<option value="Electricity">Electricity</option>
												<option value="Staff_Transport">Staff Transport</option>
												<option value="Salary">Salary</option>
												<option value="Drinks">Drinks</option>
												<option value="Electronic">Electronic</option>
												<option value="Internet">Internet</option>
												<option value="Maintenance">Maintenance</option>
												<option value="Food_Items">Food Items</option>
												<option value="Construction">Construction</option>
												<option value="Airtime">Airtime</option>
												<option value="Restaurant_Need">Restaurant Need</option>
												<option value="Petrol/Diesel/Gas">Petrol/Diesel/Gas</option>
												<option value="Reward">Reward</option>
												<option value="Director">Director</option>
											<option value="HouseKeeping_Need">HouseKeeping Need"</option>
											<option value="Stationary">Stationary</option>
											<option value="Pool_Chemicals">Pool Chemicals</option>
											<option value="Labour_Charge">Labour Charge</option>
											<option value="Cable">Cable</option>
											<option value="TFDA/MUNICIPAL">TFDA/MUNICIPAL</option>
											<option value="MUNICIPAL">MUNICIPAL</option>
											<option value="Office_Equipment">Office Equipment</option>
											<option value="Duwasa">Duwasa</option>
											<option value="Medicine">Medicine</option>
											<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('cost',FALSE);?></label>
											<input type="number" class="form-control input-sm"   name="cost" required="required">
										</div>
									
								</div>
								<div class="modal-footer">
										<button type="submit" name="save_exp" class="btn btn-success btn-block"><i class="fa fa-plus"></i></button>
									</form>
								</div>
							</div>
						</div>
					</div>
					</div></th>
					<!-- Adding complementary drinks -->
					<th><a class="btn btn-primary pull-right" data-toggle="modal" href='#modal-id2'><i class="fa fa-plus"></i>&nbspComplementary</a>
					<div class="modal fade" id="modal-id2">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Complementary</h4>
								</div>
								<div class="modal-body">
									<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/addcomplementary',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td>Drink Name</td>
					 			<td><select name="drink_id" class="form-control input-sm" required="required">
					 				<?php $result=$this->db->get('drinks');
					 					$res=$result->result();
					 					foreach ($res as $drink) {
					 				?>
			    				<option value="<?php echo $drink->item_id;?>"><?php echo $drink->name.' /'.$drink->category;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('quantity',FALSE);?></td>
					 			<td><input type="number" name="quantity" class="form-control input-sm" min="1" required="required">
			    				</td>
					 			</tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('receiver',FALSE);?></td>
					 			<td><input type="text" name="receiver" class="form-control input-sm" value="" required="required">
			    				</td>
					 			</tr>
					 			<tr><td colspan="5"><button type="submit" name="addcomplementary" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
					 		</tbody>
					 	</table>
 					</form>
								</div>
								<div class="modal-footer">
									
								</div>
							</div>
						</div>
					</div></th>
					<!-- Returning spoil drinks -->
					<th>
						<a class="btn btn-warning pull-right" data-toggle="modal" href='#modal-id3'><i class="fa fa-plus"></i>&nbspSpoil</a>
					<div class="modal fade" id="modal-id3">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Spoil</h4>
								</div>
								<div class="modal-body">
									<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/addSpoil',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td>Drink Name</td>
					 			<td><select name="drink_id" class="form-control input-sm" required="required">
					 				<?php $result=$this->db->get('drinks');
					 					$res=$result->result();
					 					foreach ($res as $drink) {
					 				?>
			    				<option value="<?php echo $drink->item_id;?>"><?php echo $drink->name.' /'.$drink->category;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td>Quantity</td>
					 			<td><input type="number" name="quantity" class="form-control input-sm" min="1" required="required">
			    				</td>
					 			</tr>
					 			<tr><td colspan="5"><button type="submit" name="addspoil" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
					 		</tbody>
					 	</table>
 					</form>
								</div>
							</div>
						</div>
					</div>
					</th>
					<th><a class="btn btn-primary pull-right" data-toggle="modal" href='#modal-id5'><i class="fa fa-plus"></i>&nbspOthers</a>
					<div class="modal fade" id="modal-id5">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;">Others</h4>
								</div>
								<div class="modal-body">
									<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('expenditure/addOther',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td>Drink Name</td>
					 			<td><select name="drink_id" class="form-control input-sm" required="required">
					 				<?php $result=$this->db->get('drinks');
					 					$res=$result->result();
					 					foreach ($res as $drink) {
					 				?>
			    				<option value="<?php echo $drink->item_id;?>"><?php echo $drink->name.' /'.$drink->category;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('quantity',FALSE);?></td>
					 			<td><input type="number" name="quantity" class="form-control input-sm" min="1" required="required">
			    				</td>
					 			</tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('absorbed_to',FALSE);?></td>
					 			<td><select name="location" id="input" class="form-control" required="required">
					 				<option value="conference">Conference</option>
					 				<option value="restaurant">Restaurant</option>
					 				<?php $res=$this->db->get('rooms');
					 					if ($res !=null) {
					 					foreach ($res->result() as $room) {
					 				?>
			    				<option value="<?php echo $room->name;?>"><?php echo $room->name.' /'.$room->category;?></option>
			    					<?php
			    					}
					 					}
			    					?>
					 				<option value="others">Others</option>
					 			</select>
			    				</td>
					 			</tr>
					 			<tr><td colspan="5"><button type="submit" name="addOther" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
					 		</tbody>
					 	</table>
 					</form>
								</div>
								<div class="modal-footer">
									
								</div>
							</div>
						</div>
					</div></th>
				</tr>
			</thead>
		</table>
	</div>
</div>