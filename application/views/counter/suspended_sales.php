<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
			
				<h3 style="text-align: center;"><?php echo $this->lang->line('credit_sales',FALSE);?></h3>
			<div class="table-responsive">
				 	<table class="table table-hover table-striped table-bordered" id="mytable2">
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('quantity',FALSE);?></th><th><?php echo $this->lang->line('sold_date',FALSE);?></th><th><?php echo $this->lang->line('customer_name',FALSE);?></th><th><?php echo $this->lang->line('cash',FALSE);?></th><th><?php echo $this->lang->line('employee',FALSE);?></th><th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($suspended)) {
					if (!$suspended){
							?>
				 		<tr><td colspan=""><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 			No Records Found!
				 		</div></td></tr>
				
				 <?php
						}else{
					foreach ($suspended->result() as $item) {
	
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->amount;?></td><td><?php echo $item->sale_time;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo $item->price;?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td><td><a href="<?php echo site_url('counter/unsuspend/'.$item->sale_id);?>" class="btn btn-success">Sale</a></td></tr>
				 		
					<?php
				}}
				?>
				<?php }
				?>
				</tbody>
				</table>
		</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>