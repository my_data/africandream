<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>

<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	 <form action="<?php echo site_url('counter/allSales');?>" method="POST" class="form-inline" role="form">
					<button type="submit" name="allSales" class="btn btn-success btn-sm">ALL SALES</button>
				</form>
	</div>
			
				<h3 style="text-align: center;">List of Drinks Orders</h3>
		 		 			
			<div class="table-responsive">
				 	<table class="table table-hover table-striped table-bordered" id="mytable1">
				 		<thead>
				 			<tr>
				 				<th>NAME</th><th>CATEGORY</th><th>QUANTITY</th><th>DATE</th><th>Customer</th><th>PRICE</th><th>SOLD BY</th><th>..</th><th>..</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php

				$result_obj=$this->db->query("SELECT * FROM drink_order INNER JOIN drinks ON drink_order.drink_id=drinks.item_id INNER JOIN employees ON employee_id=saler_id WHERE drink_order.status=0");

				if ($result_obj) {
					if ($result_obj->num_rows()==0){
							?>
				 		<tr><td colspan=""><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 			No Records Found!
				 		</div></td></tr>
				
				 <?php
						}else{
					foreach ($result_obj->result() as $item) {
	
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->qty;?></td><td><?php echo $item->day;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo $item->price;?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td><td><a href="<?php echo site_url('order/sale/'.$item->order_id);?>" class="btn btn-success btn-sm">Cash Sale</a></td><td>
				 			<a href="<?php echo site_url('creditorder/sale/'.$item->order_id);?>" class="btn btn-warning btn-sm">Credit Sale</a>
				 			<a href="<?php echo site_url('order/remove/'.$item->order_id);?>"  title="Cancel Order"><button style="color: red;" class="btn btn-default btn-sm"><i class="fa fa-close"></i></button></a></td></tr>
				 		
					<?php
				}
			}
				?>
				<?php }
				?>
				</tbody>
				</table>
		</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable1").DataTable();
			});
		</script>
</div>
