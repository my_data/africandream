<div class="panel panel-default">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="col-lg-7 col-sm-12 col-md-7">
	<div class="well well-sm" style="background-color: #778899;">
		<h5 style="text-align: center;text-transform: uppercase;color: white;">
		<strong><?php echo $this->lang->line('counter',FALSE);?></strong>
		</h5>
	</div>
		<script type="text/javascript">
				$(document).ready(function(){
					$("#search").keyup(function(){
					var value=this.value;
					if (value.length>10) {
						window.location.href="<?php echo site_url('counter/search/')?>"+this.value;
						}	
					});	
				});
		</script>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>
						<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('counter/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input list="drinks" class="form-control mr-sm-2" type="search" id="search" placeholder="Search Item" aria-label="Search" name="keyword" autofocus="autofocus"/>
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" name="search" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					<datalist id="drinks">
			    	<?php 
			    		$res=$this->db->query("SELECT name FROM drinks");
			    		if ($res->num_rows()>0) {
			    				foreach($res->result() as $res){
			    			?>
			    				<option value="<?php echo $res->name;?>">
			    			<?php
			    			}
			    			}
			    			?>		 
					</datalist>
 					</form>
 					</th>
					<th><form class="form-inline" method="POST" action="<?php echo site_url('counter/todaySales');?>">
			    		<button type="submit" class="btn btn-success btn-sm" name="todaySales"><?php echo $this->lang->line('my_cash_sales',FALSE);?></button>
			  		</form>
			  		</th>
					<th>
					<form class="form-inline" method="POST" action="<?php echo site_url('counter/suspended');?>">
			    		<button type="submit" class="btn btn-danger btn-sm" name="suspended"><?php echo $this->lang->line('my_credit_sales',FALSE);?></button>
			  		</form>
					</th>
				</tr>
			</thead>
		</table>
			<table class="table table-striped table-hover table-bordered">
				<thead>
				<tr>
				<th colspan=""><?php echo $this->lang->line('name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('stock',FALSE);?></th><th><?php echo $this->lang->line('price',FALSE);?></th><th><?php echo $this->lang->line('quantity',FALSE);?></th>
				</tr>
				</thead>
				<tbody>
					<?php
					$attrib=array('class'=>'form-inline','role'=>'form');
					if(isset($result)){ 
					foreach ($result as $item){
					echo form_open('counter/add_to_cart',$attrib);
					?>
					<tr>
					<td colspan="">
					<div class="form-group">
					<input type="text" class="form-control" name="itemname" value="<?php echo $item->name;?>" readonly="readonly" size="60"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" name="category" value="<?php echo $item->category;?>" readonly="readonly" />
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" name="stock" value="<?php echo $item->size;?>" readonly="readonly" />
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" value="<?php echo $item->price;?>" readonly="readonly" id="price" name="price"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="number" class="form-control" name="qty" id="amount" required="required" min="1" />
					<input type="hidden" name="item_id" value="<?php echo $item->item_id;?>" required="required" />
					</div>
					</td>
					</tr>
					<tr>
					<td colspan="4">
					<button type="submit" class="btn btn-success btn-block" name="add"><i class="fa fa-plus"></i></button>
					</td>
					</tr>
					</form>
					<?php	
					}}
					else{
						?>
						<tr><td colspan="4"><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>No Recod Selected</strong>
						</div></td></tr>
						<?php
					}
					?>
				</tbody>	
				</table>	
				
	</div>
	<div class="col-lg-5 col-sm-12 col-md-5 pull-right">
		<div class="well well-lg">
			<?php require_once('shopping_cart.php');?>
		</div>
	</div>
	</div>
</div>