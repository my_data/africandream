<?php
$attrib=array('role'=>'form','class'=>'form-inline');
 echo form_open('sale_food/sale',$attrib);
 ?>
        <h4 style="text-align: center;color:white;"><i class="fa fa-cart-plus fa-15x" style="font-size:60px"></i></h4>

        <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered">
                        <thead>
                                <tr style="background-color: #778899; color: white;">
                                    <th><?php echo $this->lang->line('food_name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('amount',FALSE);?></th><th><?php echo $this->lang->line('price',FALSE);?></th><th>...</th>
                                </tr>
                        </thead>
                        <tbody>
                        <?php 
                        if(!$this->cart->contents()){
                                ?>
                                 <tr style="background-color: #778899;"><td colspan="3">
                                <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       <strong>No content</strong>
                                </div>
                                 </td></tr>
                                <?php
                        }
                        foreach ($this->cart->contents() as $item) {
                          ?>
                        <tr style="background-color: #778899;olor: white;">
                             <td><?php echo $item['name'];?></td>
                             <td><?php echo $item['category'];?></td>
                            <td><?php  echo $item['qty'];?></td>
                            <td><?php echo $this->cart->format_number($item['price']); ?></td>
                <td><a href="<?php echo site_url('sale_food/remove_cart_item/'.$item['rowid'])?>"><i class=" fa fa-remove" style="color: red;"></i></a></td>

                        </tr>
                          <?php     
                        }
                        ?>
        <tr style="background-color: #778899;color: white;">
        <td class="right"><strong>Total Price</strong></td>
        <td class="right"><?php echo $this->cart->format_number($this->cart->total()); ?></td>
        </tr>
        <tr style="background-color: #778899;olor: white;">
            <td>
                <div>
                    Receipt
                    <div class="radio">
                        <label>
                            <input type="radio" name="receipt" value="0" checked="checked">No   
                        </label>      
                    </div>
                </div>
                </td>
                <td><label>
                            <input type="radio" name="receipt" value="1">Yes
                        </label></td>
        </tr>
        </tbody>                     
    </table>
        </div>

        <input type="text" name="customer" placeholder="customer" class="form-control" required style="margin-bottom: 2%" required>
    <button type="submit" class="btn btn-primary btn-block btn-sm" name="salefood"><?php echo $this->lang->line('cash_sale',FALSE);?></button>
    <button type="submit" class="btn btn-danger btn-block btn-sm" name="suspendfood"><?php echo $this->lang->line('credit_sale',FALSE);?></button>
    <button type="submit" class="btn btn-warning btn-block btn-sm" name="complementary"><?php echo $this->lang->line('complement',FALSE);?></button>
    <button type="submit" class="btn btn-success btn-block btn-sm" name="order_food"><?php echo $this->lang->line('make_order',FALSE);?></button>
</form>