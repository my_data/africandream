<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('restaurant');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong><?php echo $this->lang->line('foods',FALSE);?></strong></h5>
	</div>
	<div class="panel-body">
	<div class="jumbotron">
		<h6>Edit Food</h6>
		<?php 
									$attrib=array('role'=>'form');
									echo form_open('restaurant/update',$attrib);?>
									
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('food_name',FALSE);?></label>
										<input type="text" class="form-control"  name="foodname" required="required" value="<?php echo $row[0]->foodname;?>" >
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<input type="text" class="form-control"  name="category" required="required" value="<?php echo $row[0]->category;?>" >
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('retail_price',FALSE);?></label>
											<input type="number" class="form-control"   name="price" required="required" value="<?php echo $row[0]->price;?>" >
										</div>
										<input type="hidden" name="food_id" required="required" value="<?php echo $row[0]->food_id;?>" />
			<button type="button" class="btn btn-danger" id="concel">Back</button>
			<button type="submit" class="btn btn-success" name="update_food">Update</button>
								</div>
		</form>
		</div>
	</div>
</div>