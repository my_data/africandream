<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('restaurant/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>7) {
		window.location.href="<?php echo site_url('restaurant/search/')?>"+this.value;
	}	
});

});
</script>
<div class="panel panel-default">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="">
		<h5 style="text-transform: uppercase;"><strong>Food Menu</strong></h5>
	</div>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>
						<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('restaurant/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input list="foods" class="form-control mr-sm-2 input-sm" type="search" id="search" placeholder="Search Food" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary btn-sm" name="search_food" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control input-sm">
						<option value="">Filter</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select></th>
					<th><div class="pull-right">
					<a class="btn btn-warning btn-sm" data-toggle="modal" href='#modal-id'><i class="fa fa-plus"></i>Food
					</a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color: #337ab7;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;">ADD FOOD</h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('restaurant/save',$attrib);?>
									
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('food_name',FALSE);?></label>
											<input type="text" class="form-control"  name="foodname" required="required">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<input type="text" class="form-control"  name="category" required="required">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('retail_price',FALSE);?></label>
											<input type="number" class="form-control"   name="price" required="required">
										</div>
									
								</div>
								<div class="modal-footer">
										<button type="submit" name="save_food" class="btn btn-primary">Save</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					</div></th>
				</tr>
			</thead>
		</table>
		<table class="table table-striped table-hover table-bordered">
			<tbody>
			<tr><td></td><td><?php echo $this->lang->line('food_name',FALSE);?></td><td><?php echo $this->lang->line('category',FALSE);?></td><td><?php echo $this->lang->line('price',FALSE);?></td><td colspan="2"><?php echo $this->lang->line('action',FALSE);?></td></tr>
				<?php
				if(isset($food_result)){
					if($food_result){
					if (empty($food_result)) {
						?>
						<tr>
						<td colspan="4"><div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 <h3>No Record Found!.</h3>
						</div></tr></td>
						<?php
					}
					else{
				foreach ($food_result as $row) {
					?>

				<tr>
					<td><input type="checkbox" name="<?php //echo $row->name?>" value="<?php //echo $row->item_id?>"></td><td><?php echo $row->foodname;?></td><td><?php echo $row->category;?></td><td><?php echo $row->price;?></td><td colspan="2"><a href="<?php echo site_url('restaurant/update/'.$row->food_id);?>"><i class="fa fa-edit" style="color:green"></i></a>&nbsp &nbsp
					<a href="<?php echo site_url('restaurant/delete/'.$row->food_id);?>"><i class="fa fa-trash-o" style="color:red"></i></a></td>
				</tr>
					<?php
				}
				}
				}}?>
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>