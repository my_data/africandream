<script type="text/javascript">
	$(document).ready(function(){
		$("#detailed-sale").slideUp();
		$("#summary-table").slideUp();
	});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		
		 <h4 style="text-align: center;color: green;"><?php echo $this->lang->line('today_food_sales',FALSE);?></h4>
		 <!-- toggle detailed sales  -->
				 <script type="text/javascript">
				 	$(document).ready(function(){
				 		$("#toggle-table-summary").click(function(){
				 			$("#summary-table").slideToggle();
				 			$("#detailed-sale").slideUp();
				 		});
				 	});
				 </script>
		<div class="panel panel-default">
		<div class="panel-heading">
		<?php echo $this->lang->line('summary',FALSE);?>
		</div>
			<div class="panel-body">
			<button type="button" id="toggle-table-summary" class="btn btn-default"><?php echo $this->lang->line('summary',FALSE);?></button>
			<div class="table-responsive" id="summary-table">
				 	<table class="table table-hover">
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('food_name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('quantity_sold',FALSE);?></th><th><?php echo $this->lang->line('cash',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($cash) && isset($sold_items_id)){
					$total_sale=$cash->row();
					if ($sold_items_id) {
					
					foreach ($sold_items_id->result() as $item) {
						$from=date("Y-m-d",time());
						$to=date("Y-m-d",time());
					$item=get_sold_food_particular($item->item_id,$from,$to);
					
					?>
				 		
				 			<tr><td><?php echo $item[0]->foodname;?></td><td><?php echo $item[0]->category;?></td><td><?php echo $item[0]->qty;?></td><td><?php echo $item[0]->cash;?></td></tr>
				 		
					<?php
				}}
				
				?>
				
				<tr style="color: green;">
				 	<th colspan=""><h3>Money Cash(Tshs):</h3></th><th><h3><?php echo $total_sale->value_sum;?></h3>
				 	</th>
				 </tr>
				<?php 

				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
				 </div>
				 </div>
				 <!-- toggle detailed sales  -->
				 <script type="text/javascript">
				 	$(document).ready(function(){
				 		$("#toggle-table-detail").click(function(){
				 			$("#detailed-sale").slideToggle();
				 			$("#summary-table").slideUp();
				 		});

				 		$("#toggle-table-summary").click(function(){
				 			$("#detailed-sale").slideUp();
				 		});
				 	});
				 </script>
				 <!-- table for display drink sales -->
				 <div class="panel panel-default">
		<div class="panel-heading">
		</div>
			<div class="panel-body">
				  <button type="button" id="toggle-table-detail" class="btn btn-default">Detailed</button>
				 <div class="table-responsive" id="detailed-sale"> 
				 	<table class="table table-hover">
				 		<thead>
				 			<tr>
				 				<th>Food</th><th>Category</th><th>Customer</th><th>Quantity</th><th>Price(Tshs)</th><th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				 		<?php 
				 			$saler_id=(int)$this->session->userdata('person_id');
				 			$day=date("Y-m-d",time());
				 $query_sales=$this->db->query("SELECT * FROM foodSales WHERE saler_id=$saler_id AND sale_time='$day'");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		?>
				 			<tr>
				 			<?php
				 			$attrib=array('class'=>'form-inline','role'=>'form');
				 			echo form_open('',$attrib);
				 			?>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->name; ?>" readonly ></td>
				 			<td><input type="text" name="category" class="form-control" 
				 			value="<?php echo $sale->category; ?>" readonly ></td>
				 			<td><input type="text" name="customer" class="form-control" 
				 			value="<?php echo $sale->customer; ?>" required="required"></td>
				 			<input type="hidden" name="old_sale_qty" value="<?php echo $sale->amount; ?>" required="required">
				 			<td><input type="number" min="1" name="new_sale_qty" class="form-control"
				 			value="<?php echo $sale->amount; ?>" required="required"></td>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->price; ?>" readonly></td>
				 			<input type="hidden" name="food_id" value="<?php echo $sale->item_id; ?>" required="required">
				 			<input type="hidden" name="sale_id" value="<?php echo $sale->sale_id; ?>" required="required">
				 			</form>
				 			</tr>
				 	<?php
				 			}} ?>
				 			<tr><td><a href="<?php echo base_url('home');?>" class="btn btn-danger"><?php echo $this->lang->line('close',FALSE);?></a></td></tr>
				 		</tbody>
				 	</table>
				 </div>
				 </div>
				 </div>
				 </div>
			</div>	
		</div>
	</div>
</div>