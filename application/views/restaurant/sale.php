<div class="panel panel-primary">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="col-lg-7 col-sm-12 col-md-7">
	<div class="well well-sm" style="background-color: #778899;">
		<h5 style="text-align: center;text-transform: uppercase;">
		<strong style="color: white;">WELCOME TO SALE FOOD</strong>
		</h5>
	</div>
		<script type="text/javascript">
				$(document).ready(function(){
					$("#search").keyup(function(){
					var value=this.value;
					if (value.length>10) {
						window.location.href="<?php echo site_url('sale_food/search/')?>"+this.value;
						}	
					});	
				});
		</script>
		<table class="table table-responsive">
			<thead>
				<tr>
					<th>
						<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('sale_food/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input list="foods" class="form-control mr-sm-2" type="search" id="search" placeholder="Search" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" name="search_food" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					<datalist id="foods">
			    	<?php 
			    		$res=$this->db->query("SELECT foodname FROM food");
			    		if ($res->num_rows()>0) {
			    				foreach($res->result() as $res){
			    			?>
			    				<option value="<?php echo $res->foodname;?>">
			    			<?php
			    			}
			    			}
			    			?>		 
					</datalist>
 					</form>
 					</th>
					<th><form class="form-inline" method="POST" action="<?php echo site_url('sale_food/todaySales');?>">
			    		<button type="submit" class="btn btn-success btn-sm" name="todaySales"><?php echo $this->lang->line('my_cash_sales',FALSE);?></button>
			  		</form>
			  		</th>
					<th>
					<form class="form-inline" method="POST" action="<?php echo site_url('sale_food/creditSales');?>">
			    		<button type="submit" class="btn btn-danger btn-sm" name="creditSales"><?php echo $this->lang->line('my_credit_sales',FALSE);?></button>
			  		</form>
					</th>
				</tr>
			</thead>
		</table>
			<table class="table table-striped table-hover">
				<thead>
				<tr>
				<th><?php echo $this->lang->line('food_name',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('price',FALSE);?></th><th><?php echo $this->lang->line('amount',FALSE);?></th>
				</tr>
				</thead>
				<tbody>
					<?php
					$attrib=array('class'=>'form-inline','role'=>'form');
					if(isset($food_result)){ 
					foreach ($food_result as $item){
					echo form_open('sale_food/add_to_cart',$attrib);
					?>
					<tr>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" name="foodname" value="<?php echo $item->foodname;?>" readonly="readonly" size="50"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" name="category" value="<?php echo $item->category;?>" readonly="readonly" />
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="text" class="form-control" value="<?php echo $item->price;?>" readonly="readonly" id="price" name="price"/>
					</div>
					</td>
					<td>
					<div class="form-group">
					<input type="number" class="form-control" name="amount" id="amount" required="required" min="1" />
					<input type="hidden" name="food_id" value="<?php echo $item->food_id;?>" required="required" />
					</div>
					</td>
					</tr>
					<tr>
					<td colspan="4">
					<button type="submit" class="btn btn-success btn-block" name="add_to_cart"><i class="fa fa-plus"></i></button>
					</td>
					</tr>
					</form>
					<?php	
					}}
					else{
						?>
						<tr><td colspan="4"><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>No Recod Selected</strong>
						</div></td></tr>
						<?php
					}
					?>
				</tbody>	
				</table>	
				
	</div>
	<div class="col-lg-5 col-sm-12 col-md-5 pull-right">
		<div class="well well-lg" style="background-color: #778899;">
			<?php require_once('food_cart.php');?>
		</div>
	</div>
	</div>
</div>