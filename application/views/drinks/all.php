<script type="text/javascript">
$(document).ready(function() {
//filtering of item
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('drinks/filter/')?>"+this.value;
});
//searching of item
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('drinks/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5 style="text-align: center;text-transform: uppercase;text-shadow: 2px;"><strong><?php echo $this->lang->line('drinks_inventory',FALSE);?></strong></h5>
	</div>
	<div class="panel-body">	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>
					<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('drinks/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search Sale" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" name="search_drink" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
				 	</th>
					<th><select name="sortby" id="sortby" class="form-control">
						<option value="">Filter</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select></th>
					<th><div class="pull-right">
					<a class="btn btn-warning" data-toggle="modal" href='#modal-id'><i class="fa fa-plus"></i>&nbsp <?php echo $this->lang->line('drink',FALSE);?></a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color: #337ab7;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;"><i class="fa fa-plus"></i><?php echo $this->lang->line('drink',FALSE);?></h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('drinks/save',$attrib);?>
									<div class="table-responsive">
										<table class="table table-hover table-striped table-bordered">
											<tbody>
												<tr>
													<td><div class="form-group">
											<label for=""><?php echo $this->lang->line('drink',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="name" required="required">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="category" required="required">
										</div></td><td>
												</tr>
												<tr><td>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('quantity',FALSE);?></label>
											<input type="number" class="form-control input-sm"   name="amount" required="required">
										</div></td>
										<td><div class="form-group">
											<label for=""><?php echo $this->lang->line('item_cost',FALSE);?></label>
											<input type="number" step="0.01" class="form-control input-sm"   name="cost" required="required">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('retail_price',FALSE);?></label>
											<input type="number" class="form-control input-sm"   name="price" required="required">
										</div></td>
										</tr>
											<tr><td colspan="2"><button type="submit" name="save_item" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i></button></td></tr>
											</tbody>
										</table>
									</div>
									</form>	
								</div>
								<div class="modal-footer">
										
									
								</div>
							</div>
						</div>
					</div>
				</tr>
			</thead>
		</table>
		<table class="table table-striped table-bordered">
			<tbody>
			<tr><td><?php echo $this->lang->line('drink',FALSE);?></td><td><?php echo $this->lang->line('category',FALSE);?></td><td><?php echo $this->lang->line('stock',FALSE);?></td><td><?php echo $this->lang->line('price',FALSE);?></td><td><?php echo $this->lang->line('item_cost',FALSE);?></td><td><?php echo $this->lang->line('capital',FALSE);?></td><td colspan="2"><?php echo $this->lang->line('action',FALSE);?></td></tr>
				<?php
				foreach ($item_result as $row) {
					?>

				<tr>
					<td><?php echo $row->name;?></td><td><?php echo $row->category;?></td><td><?php echo $row->size;?></td><td><?php echo number_format($row->price);?>/=</td>
					<td><?php echo number_format($row->cost);?>/=</td><td><?php echo number_format($row->cost*$row->size);?>/=</td>
					<td colspan="2"><a href="<?php echo site_url('drinks/update/'.$row->item_id);?>"><i class="fa fa-edit" style="color:green"></i></a>&nbsp &nbsp
					<a href="<?php echo site_url('drinks/delete/'.$row->item_id);?>"><i class="fa fa-trash-o" style="color:red"></i></a></td>
				</tr>
					<?php
				}

				?>
				
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>