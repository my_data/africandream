<script type="text/javascript">
$(document).ready(function() {
//filtering of item
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('drinks/filter/')?>"+this.value;
});
//searching of item
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('receiving/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5 style="text-align: center;text-transform: uppercase;text-shadow: 2px;"><strong>Item Inventory</strong></h5>
	</div>
	<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>
					<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('receiving/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search Sale" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" name="search_drink" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control">
						<option value="">Filter</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select></th>

				</tr>
			</thead>
		</table>
		<table class="table table-striped table-bordered">
			<tbody>
			<tr><td></td><td><?php echo $this->lang->line('drink',FALSE);?></td><td><?php echo $this->lang->line('category',FALSE);?></td><td><?php echo $this->lang->line('stock',FALSE);?></td><td><?php echo $this->lang->line('price',FALSE);?></td><td><?php echo $this->lang->line('capital',FALSE);?></td><td colspan="2"><?php echo $this->lang->line('action',FALSE);?></td></tr>
				<?php
				if ($item_result!=null) {
				foreach ($item_result as $row) {
					?>

				<tr>
					<td><input type="checkbox" name="<?php echo $row->name?>" value="<?php echo $row->item_id?>"></td><td><?php echo $row->name;?></td><td><?php echo $row->category;?></td><td><?php echo $row->size;?></td><td><?php echo $row->price;?></td><td><?php echo $row->cost;?></td><td colspan="2"><a href="<?php echo site_url('receiving/add/'.$row->item_id);?>"><i class="fa fa-plus" style="color:green"></i></a></td>
				</tr>
					<?php
				}
			}
				?>
				
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>