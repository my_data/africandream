<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('drinks');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong><?php echo $this->lang->line('drink',FALSE);?></strong></h5>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="">
		<h6>Edit Item</h6>
		<?php 
									$attrib=array('role'=>'form','class'=>'form');
									echo form_open('drinks/update',$attrib);?>
									
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('drink',FALSE);?></label>
											<input type="text" class="form-control"  name="name" required="required" value="<?php echo $row[0]->name;?>">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<input type="text" class="form-control"  name="category" required="required" value="<?php echo $row[0]->category;?>">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('stock',FALSE);?></label>
											<input type="number" class="form-control"   name="amount" required="required" value="<?php echo $row[0]->size;?>" min="0">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('retail_price',FALSE);?></label>
											<input type="number" step="0.01" class="form-control"   name="price" required="required" value="<?php echo $row[0]->price;?>">
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('item_cost',FALSE);?></label>
											<input type="number" step="0.01" class="form-control"   name="cost" required="required" value="<?php echo $row[0]->cost;?>">
										</div>	
										<input type="hidden" name="item_id" required="required" value="<?php echo $row[0]->item_id;?>" />
			<button type="button" class="btn btn-danger" id="concel">Back</button>
			<button type="submit" class="btn btn-success" name="update_item">Update</button>
								</div>
		</form>
		</div>
	</div>
</div>