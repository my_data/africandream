<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('receiving');?>";
		});

	});
</script>
<style type="text/css">
	h4{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong>ITEM</strong></h5>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="panel-default" style="margin-top: 3%;">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
		<h4>Receive Drink</h4>
		<?php 
									$attrib=array('role'=>'form');
									echo form_open('receiving/add',$attrib);?>
									
										<div class="form-group">
											<label for="">Name</label>
											<input type="text" class="form-control"  name="name" readonly="readonly" value="<?php echo $row[0]->name;?>">
										</div>
										<div class="form-group">
											<label for="">Category</label>
											<input type="text" class="form-control" value="<?php echo $row[0]->category;?>" readonly />
										</div>
										<div class="form-group">
											<label for="">Quantity</label>
											<input type="number" class="form-control"  name="quantity" required="required" min="1">
										</div>
										<div class="form-group">
											<label for=""><?php echo lang('item_cost');?></label>
											<input type="number" class="form-control"   name="cost" required="required" min="1">
										</div>
										<input type="hidden" name="item_id" required="required" value="<?php echo $row[0]->item_id;?>" />
										<input type="hidden" name="old_cost" required="required" value="<?php echo $row[0]->cost;?>" />
										<input type="hidden" name="old_stock" required="required" value="<?php echo $row[0]->size;?>"/>
			<button type="button" class="btn btn-danger" id="concel">Back</button>
			<button type="submit" class="btn btn-success" name="receive_item"><i class="fa fa-plus"></i></button>
								</div>
		</form>
		</div></div>
	</div>
</div>