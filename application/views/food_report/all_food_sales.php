<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<script type="text/javascript">
	$(document).ready(function(){
		$("#detailed-sale").slideUp();
		$("#summary-table").slideUp();
		
	});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		
		 <h4 style="text-align: center;color: green;">ALL FOOD SALES</h4>		
			 <!-- toggle detailed sales  -->
				 <script type="text/javascript">
				 	$(document).ready(function(){
				 		$("#toggle-table-detail").click(function(){
				 			$("#detailed-sale").slideToggle();
				 		});
				 	});
				 </script>
				 <!-- table for display drink sales -->
				 <div class="panel panel-default">
		<div class="panel-heading">
		</div>
			<div class="panel-body">
				  
				 <div class="table-responsive"> 
				 	<table class="table table-hover table-bordered" id="all-sales-tbl">
				 		<thead>
				 			<tr>
				 				<th>Name</th><th>Category</th><th>Customer</th><th>Saler</th><th>Quantity</th><th>Price(Tshs)</th><th>DATE</th><th>...</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				 <?php 
				 			
				 $query_sales=$this->db->query("SELECT * FROM foodSales inner join employees on saler_id=employee_id");
				 if($query_sales->num_rows()>0){ 
				 	foreach ($query_sales->result() as $sale) {
				 		?>
				 			<tr>
				 			<?php
				 			$attrib=array('class'=>'form-inline','role'=>'form');
				 			echo form_open('sale_food/updateSaleFood',$attrib);
				 			?>
				 			<td><input type="text" class="form-control" 
				 			value="<?php echo $sale->name; ?>" readonly size="80">
				 			</td>
				 			<td><input type="text" name="category" class="form-control" 
				 			value="<?php echo $sale->category; ?>" readonly></td>
				 			<td><input type="text" name="customer" class="form-control" 
				 			value="<?php echo $sale->customer; ?>" readonly></td>
				 			<td><input type="text" name="saler" class="form-control" 
				 			value="<?php echo $sale->firstname.' '.$sale->lastname; ?>" readonly size="120">
				 			</td>
				 			<td><input type="number" min="1" name="qty" class="form-control" value="<?php echo $sale->amount;?>"></td>
				 			<td><input type="text" class="form-control" value="<?php echo $sale->price; ?>" readonly></td>
				 			<td><input type="text" class="form-control" value="<?php echo $sale->sale_time; ?>" readonly size="80"></td>
				 			<td><button type="submit" name="updateSale" class="btn btn-success btn-sm">Update</button>
				 			</td>
				 			<input type="hidden" name="food_id" value="<?php echo $sale->item_id;?>" required="required">
				 			<input type="hidden" name="sale_id" value="<?php echo $sale->sale_id; ?>" required="required">
				 			</form>
				 			</tr>
				 	<?php
				 			}} ?>
				 		</tbody>
				 	</table>
				 </div>
				 </div>
				 </div>
				 </div>
			</div>	
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#all-sales-tbl").DataTable();
			});
		</script>
</div>