<div class="panel panel-primary">
	<div class="panel-heading">
	<?php
	$from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
	Summary Food Report
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/getSummaryFoodSales',$attr);?>
		<div class="table">
			<table class="table table-sm">
				<thead>
					<tr>
						<th>
							<?php echo $this->lang->line('from',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span>
							<input type="date" name="from" id="from" class="form-control" required>
							</div>
						</th>
						<th>
							<?php echo $this->lang->line('to',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span>
							<input type="date" name="to" id="to" class="form-control" required>
							</div>
						</th>
						<th>
							<select name="employee_id" class="form-control">
								<option value="">-- Select Employee --</option>
								<?php 
								$employees=$this->db->get('employees');
								if ($employees->num_rows()>0) {
									foreach ($employees->result() as $employees) {
								?>
								<option value="<?php echo $employees->employee_id?>"><?php echo $employees->firstname.' '.$employees->lastname;?></option>
								<?php	
								}}
								?>
							</select>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="show_summary_foods"><?php echo lang('generate_button',false);?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body">
			<div class="table-responsive" id="printArea">
				 	<table border="1" class="table table-hover table-striped table-bordered table-sm">
				 	<caption style="color:black; padding-bottom:2%;"><center><h4><?php echo $this->lang->line('summary_food_sales').' '.'FROM'.' ('.$from.') '.'TO'.' ('.$to.')';?></h4></center></caption>
				 		<thead>
				 			<tr>
				 				<th>S/N:</th><th>FOOD ITEM</th><th>CATEGORY</th><th>UNIT SOLD</th>
				 				<th>INGREDIENTS COST</th><th>SELL PRICE/UNIT</th><th>CASH</th>
				 				<th>CREDIT</th><th>COMPLNT</th><th>PROFIT (CASH + CREDIT)</th><th>PROFIT - INGREDIENTS</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$counter=0;
				$max_qty=0;
				$complementary=0;
				$total_complementary=0;
				$total_ingredients_cost=0;
				$ingredients_per_qty_sold=0;
				$total_credit_per_food=0;
				$tatal_cash_per_food=0;
				$total_cash=0;
				$total_credit=0;
				$total_profit=0;
				$profit_per_food=0;
				$profit_min_ingrediets=0;
				$total_profit_min_ingrediets=0;
				$max_name="None";
				if (isset($cash) && isset($sold_items_id)){
					if ($sold_items_id) {
					$total_sale=$cash->row();
					$credit_sale=$total_credit_sale->row();
					foreach ($sold_items_id->result() as $item) { 
				$res=get_sold_food_particular($item->item_id,$this->session->userdata('from'),$this->session->userdata('to'));
					$counter++;
					 foreach ($res as $food) {
					 		//finding most sold food algorism
					 		if($max_qty<$food->qty){
					 			$max_qty=$food->qty;
					 			$max_name=$food->foodname;
					 		} 
					 		$ingredients_per_qty_sold=($food->sale_price*0.25)*$food->qty;
					 		$profit_per_food=$food->sale_price*$food->qty;
					 		$total_profit+=$profit_per_food;
					 		$total_credit_per_food=credit_per_food($item->item_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 		$complementary=complementary($item->item_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 		//Calculatin of cash per food item
					 		$total_cash_per_food=$profit_per_food-$total_credit_per_food;
					 		$profit_min_ingrediets=$profit_per_food-$ingredients_per_qty_sold;
					 		$total_cash+=$total_cash_per_food;
					 		$total_credit+=$total_credit_per_food;
					 		$total_ingredients_cost+=$ingredients_per_qty_sold;
					 		$total_profit_min_ingrediets+=$profit_min_ingrediets;	
					 		$total_complementary+=$complementary;	
					?>	 			
				 			<tr>
				 			<td><?php echo $counter;?></td><td><?php echo $food->foodname;?></td>
				 			<td><?php echo $food->category;?></td><td><?php echo $food->qty;?></td>
				 			<td><?php echo ($ingredients_per_qty_sold);?>/=</td>
				 			<td><?php echo number_format($food->sale_price);?>/=</td>
				 			<td><?php echo number_format($total_cash_per_food);?>/=</td>
				 			<td><?php echo number_format($total_credit_per_food);?>/=</td>
				 			<td><?php echo number_format($complementary);?>/=</td>
				 			<td><?php echo number_format($profit_per_food);?>/=</td>
				 			<td><?php echo number_format($profit_min_ingrediets);?>/=</td>
				 			</tr>
				 		
					<?php
				}}
				
				?>
				<tr style="background-color:silver;"><td>Total</td><td>--</td><td>--</td><td></td>
				<td><?php echo number_format($total_ingredients_cost);?>/=</td>
				<td></td><td><?php echo number_format($total_cash);?>/=</td>
				<td><?php echo number_format($total_credit);?>/=</td>
				<td><?php echo number_format($total_complementary);?>/=</td>
				<td><?php echo number_format($total_profit);?>/=</td>
				<td><?php echo number_format($total_profit_min_ingrediets);?>/=</td>
				</tr>
				<tr style="color: blue;">
				 	<td colspan="2">Most sold Food:</td><td></td><td></td><td></td><td></td><td></td><td></td>
				 	<td></td><td></td><td><?php echo $max_name;?></td>
				 </tr>
				<?php 
				}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>