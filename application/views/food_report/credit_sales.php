<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">
		
				<h5 style="text-align: center;">LIST OF FOOD CREDIT SALES</h5>
			<hr>
			<div class="table-responsive">
				 	<table class="table table-hover" id="mytable2">
				 		<thead>
				 			<tr>
				 				<th>FOOD</th><th>CATEGORY</th><th>QUANTITY</th><th>SOLD DATE</th><th>CUSTOMER</th><th>CASH</th><th>SOLD BY</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($suspended)) {
					if ($suspended->num_rows()==0) {
							?>
				 		<tr><td colspan="6"><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>No Records Found!
				 		</div></td></tr>
				 <?php
						}else{
					foreach ($suspended->result() as $item) {
	
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->amount;?></td><td><?php echo $item->sale_time;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo $item->price;?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td></tr>
				 		
					<?php
				}}
				?>
				<?php }
				?>
				</tbody>
				</table>
		</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>