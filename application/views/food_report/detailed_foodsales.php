<div class="panel panel-primary">
	<div class="panel-heading">
	Detailed Food Report
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/getDetailedFoodSales',$attr);?>
		<div class="table">
			<table class="table table-sm">
				<thead>
					<tr>
						<th><div class="form-group">
						Today:<input type="checkbox" name="today" id="today" class="form-control input-sm">
						</div>
						</th>
						<th>
							<div class="input-group">
							From:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
							</div>
						</th>
						<th>
							<div class="input-group">
							To:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
							</div>
						</th>
						<th>
							<select name="employee_id" class="form-control">
								<option value="">-- Select Employee --</option>
								<?php 
								$employees=$this->db->get('employees');
								if ($employees->num_rows()>0) {
									foreach ($employees->result() as $employees) {
								?>
								<option value="<?php echo $employees->employee_id?>"><?php echo $employees->firstname.' '.$employees->lastname;?></option>
								<?php	
								}}
								?>
							</select>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="show_detailed_foods"><?php echo lang('generate_button',false);?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body">
			<div class="table-responsive" id="printArea">
				 	<table border="1" class="table table-hover table-striped table-bordered table-sm">
				 	<caption><center><font size="5px;" color="blue";> Detailed Food Sales Report </font></center></caption>
				 		<thead>
				 			<tr>
				 				<th>FOOD</th><th>CATEGORY</th><th>QUANTITY SOLD</th><th>CASH</th><th>CUSTOMER</th><th>SALER</th><th>DATE</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$max_qty=0;
				$max_name="None";
				if (isset($cash) && isset($sold_items_id)){
					if ($sold_items_id) {
					$total_sale=$cash->row();
					$credit_sale=$total_credit_sale->row();
					 foreach ($sold_items_id->result() as $food) {
					 		//finding most sold food algorism
					 		if($max_qty<$food->qty){
					 			$max_qty=$food->qty;
					 			$max_name=$food->foodname;
					 		}
					 		
					?>	 			
				 			<tr><td><?php echo $food->foodname;?></td><td><?php echo $food->category;?></td><td><?php echo $food->qty;?></td><td><?php echo number_format($food->cash);?></td><td><?php echo $food->customer;?></td><td><?php echo $food->firstname.' '.$food->lastname;?></td><td><?php echo $food->sale_time;?></td></tr>
				 		
					<?php
				}	
				?>
				<?php 
				}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 No Record Found!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>