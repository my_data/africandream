<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('expenditure');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong>Expenditure</strong></h5>
	</div>
	<div class="panel-body">
	<div class="jumbotron">
		<h6>Edit Expenditure</h6>
		<?php 
									$attrib=array('role'=>'form');
									echo form_open('expenditure/update',$attrib);?>
									
										<div class="form-group">
										<label for="">Description</label>
											<input type="text" name="description" class="form-control"  required="required"  title="Description" value="<?php echo $row[0]->des;?>"/>
										</div>
										<div class="form-group">
											<label for="">Cost</label>
											<input type="number" class="form-control"   name="cost" required="required" value="<?php echo $row[0]->cost;?>">
										</div>
										<input type="hidden" name="exp_id" required="required" value="<?php echo $row[0]->exp_id;?>" />
			<button type="button" class="btn btn-danger" id="concel">Back</button>
			<button type="submit" class="btn btn-success" name="update_exp">Update</button>
								</div>
		</form>
		</div>
	</div>
</div>