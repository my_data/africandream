<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('home');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong>Users Catalogue</strong></h5>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="alert alert-danger" style="text-align: center;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			 Change Password
	</div>
	<div class="jumbotron">
		
		<?php echo form_open('staff/update_password');?>
			<div class="form-group">
			<label for="">Old Password</label>
			<input type="password" class="form-control input-sm"  name="old_password" required="required"/>
			</div>
			<div class="form-group">
			<label for="">New Password</label>
			<input type="password" class="form-control input-sm"  name="new_password" required="required"/>
			</div>
			<button type="button" class="btn btn-danger btn-sm" id="concel">Back</button>
			<button type="submit" class="btn btn-success btn-sm" name="update_password">Update</button>
		</form>
		</div>
	</div>
</div>