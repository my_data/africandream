<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('staff/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>5) {
		window.location.href="<?php echo site_url('staff/search/')?>"+this.value;
	}	
});
//validation of username field
$("#addUser").validate({
	rules: {
		firstname: {
			required: true,
			// Using the normalizer to trim the value of the element
			// before validating it.
			//
			// The value of `this` inside the `normalizer` is the corresponding
			// DOMElement. In this example, `this` references the `username` element.
			normalizer: function(value) {
				return $.trim(value);
			}
		}
	}
});


});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<h5><?php echo $this->lang->line('users_cataloque',FALSE);?></h5>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<table class="table table-responsive table-bordered">
		<thead>
			<tr>
					<th>
					<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('staff/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2 input-sm" type="search" id="search" placeholder="Search" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary btn-sm" name="search_staff" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control input-sm">
						<option value="">Filter</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select>
					</th>
					<th>
					<div class="pull-right">
					<a class="btn btn-success btn-block" data-toggle="modal" href='#modal-id'><i class=" fa fa-plus"></i>&nbsp<i class=" fa fa-user"></i></a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color:#337ab7">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;"><?php echo $this->lang->line('new_user',FALSE);?></h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form','class'=>'form','id'=>'addUser');
									echo form_open('staff/save',$attrib);
									?>
									<div class="table-responsive">
										<table class="table table-striped table-bordered">
											<tbody>
												<tr>
													<td><div class="form-group">
											<label for=""><?php echo $this->lang->line('first_name',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="firstname" required="required" id="firstname">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('surname',FALSE);?></label>
											<input type="text" class="form-control input-sm"  name="lastname" required="required">
										</div></td>
												</tr>
												<tr><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('username',FALSE);?></label>
											<input type="text" class="form-control input-sm"   name="username" required="required">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('password',FALSE);?></label>
											<input type="password" class="form-control input-sm"   name="password" required="required">
										</div></td>
										</tr>
										<tr><td colspan="2"><?php echo $this->lang->line('permissions',FALSE);?></td></tr>
										<tr style="color: green;"><td colspan="2">

											<?php
											$counter=0;
											$modules=$this->module->get_all_modules();
											 foreach($modules as $row){
											 	$counter++;
											 ?>
											 <div class="form-group col-lg-3 col-sm-5 col-md-3">
								<label for="<?php echo $row->name;?>"><?php echo $row->name;?></label>
						<input type="checkbox" class="form-control"   name="module<?php echo $counter;?>" value="<?php echo $row->module_id;?>" />
										</div>
											<?php
											}?>
								<input type="hidden" name="total_modules" value="<?php echo $counter;?>">
									
										</td></tr>
										<tr><td colspan="2"><button type="submit" name="save_staff" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
											</tbody>
										</table>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div></th>
				</tr>
		</thead>
		
	</table>
		<table class="table table-striped table-bordered">
		<thead>
			<tr><th></th><th><?php echo $this->lang->line('first_name',FALSE);?></th><th><?php echo $this->lang->line('surname',FALSE);?></th><th><?php echo $this->lang->line('username',FALSE);?></th><th colspan="2"><?php echo $this->lang->line('action',FALSE);?></th></tr>
		</thead>
			<tbody>
			
				<?php
				if(isset($allstaff)){
				foreach ($allstaff as $row) {
					?>
				<tr>
					<td><input type="checkbox" name="<?php echo $row->employee_id;?>" value="<?php echo $row->employee_id?>"></td><td><?php echo $row->firstname;?></td><td><?php echo $row->lastname;?></td><td><?php echo $row->username;?></td><td colspan="2"><a href="<?php echo site_url('staff/update/'.$row->employee_id);?>"><i class="fa fa-edit" style="color:green"></i></a>&nbsp &nbsp
					<a href="<?php echo site_url('staff/delete/'.$row->employee_id);?>"><i class="fa fa-trash-o" style="color:red"></i></a></td>
				</tr>
					<?php
				}
				}
				else{
					?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->lang->line('error_sms',FALSE);?>
						
					</div>
					<?php
				}
				?>
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>
