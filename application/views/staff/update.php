<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('staff');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong><?php echo $this->lang->line('users_cataloque',FALSE);?></strong></h5>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<h6>Edit User</h6>
		<?php echo form_open('staff/update');?>
		<div class="table-responsive">
			<table class="table table-hover">
				<tbody>
					<tr>
						<td><div class="form-group">
			<label for=""><?php echo $this->lang->line('first_name',FALSE);?></label>
			<input type="text" class="form-control input-sm"  name="firstname" required="required" value="<?php echo $staff[0]->firstname;?>" />
			</div></td>
			<td><div class="form-group">
			<label for=""><?php echo $this->lang->line('surname',FALSE);?></label>
			<input type="text" class="form-control input-sm"  name="lastname" required="required" value="<?php echo $staff[0]->lastname;?>" />
			</div></td>
					</tr>
					<tr><td><div class="form-group">
			<label for=""><?php echo $this->lang->line('username',FALSE);?></label>
			<input type="text" class="form-control input-sm"   name="username" required="required" value="<?php echo $staff[0]->username;?>" />
			</div></td><td>	<div class="form-group">
		<label for=""><?php echo $this->lang->line('new_password',FALSE);?></label>
		<input type="password" class="form-control input-sm"   name="password"/>
		<input type="hidden"  name="employee_id" required="required" value="<?php echo $staff[0]->employee_id;?>" />
		</div></td>
			</tr>
			<tr><td colspan="2"><?php echo $this->lang->line('permissions',FALSE);?></td></tr>
										<tr style="color: green;"><td colspan="2">

											<?php
											$counter=0;
											$modules=$this->db->get('modules');
											 foreach($modules->result() as $row){
											 	$counter++;
											 ?>
											 <div class="form-group col-lg-3 col-sm-5 col-md-3">
								<label for="<?php echo $row->name;?>"><?php echo $row->name;?></label>
						<input type="checkbox" class="form-control"   name="module<?php echo $counter;?>" value="<?php echo $row->module_id;?>" />
										</div>
											<?php
											}?>
								<input type="hidden" name="total_modules" value="<?php echo $counter;?>">
									
										</td></tr>
			<tr><td></td><td><span class="input-group-btn">
					<button type="button" class="btn btn-danger" id="concel">Back</button>
				<button type="submit" class="btn btn-success" name="update_staff">Update</button>
				</span></td>
			</tr>
				</tbody>
			</table>
		</div>
		
			
		</form>
	</div>
</div>