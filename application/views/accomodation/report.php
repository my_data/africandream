<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
<?php require_once(APPPATH.'/views/print.php');?>
	<div class="panel-heading"></div>
	<div class="panel-body">
<div class="panel panel-default">
	<div class="panel-heading">
	<?php echo $this->lang->line('sold_rooms_report',FALSE);?>
	<div class="row">
			
			</div>
	</div>
	<div class="panel-body">
	<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success pull-right btn-sm"><i class="fa fa-print"></i></button>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 		<?php $this->load->view('breadcrumb');?>
			</div>
			<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_soldrooms',$attr);?>
		<div class="table">
			<table class="table table-sm">
				<thead>
					<tr>
						<th><div class="input-group">
						<?php echo $this->lang->line('today',FALSE);?><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="checkbox" name="today" id="today" class="form-control">
						</div>
						</th>
						<th>
							<div class="input-group">
							<?php echo $this->lang->line('from',FALSE);?><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
							</div>
						</th>
						<th>
							<div class="input-group">
							<?php echo $this->lang->line('to',FALSE);?><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="accomodation_report"><?php echo lang('generate_button');?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>
		<div id="printArea">
		<table class="table table-bordered table-hover" id="mytable2">
			<thead>
				<tr>
					<th><?php echo lang('room_number',FALSE);?></th><th><?php echo lang('category',FALSE);?></th><th><?php echo $this->lang->line('price',FALSE);?></th><th><?php echo $this->lang->line('customer_name',FALSE);?></th><th><?php echo $this->lang->line('saler',FALSE);?></th><th><?php echo $this->lang->line('time',FALSE);?></th><th><?php echo $this->lang->line('type',FALSE);?></th>
				</tr>
			</thead>
			<tbody>
			<?php
			$total_sale=0;
			$total_cash_sale=0;
			$total_credit_sale=0;
			$most_sold_room=0;
			$total_cash=0;
			$color="black";
	if(isset($data)){
		if ($data) {
		$sale_type="Unknown";
		foreach ($data as $v) {
			//calculate sum of total money
			$total_sale=$total_cash+$v->price;
			if ($v->status==1 || $v->status==0){
				//culculate the sum of total cash sales money
				$total_cash_sale=$total_cash_sale+$v->price;
				$color="green";
			 	 $sale_type="Cash Sale";
			 }
			 elseif ($v->status==2) {
			 	//culculate the sum of credit sales money
			 	$total_credit_sale=$total_credit_sale+$v->price;
			 	$color="red";
			 	$sale_type="Credit Sale";
			 }
		?>

				<tr style="color:<?php echo $color;?>">
					<td><?php echo $v->name?></td><td><?php echo $v->category?></td><td><?php echo number_format($v->price);?></td><td><?php echo $v->roomcustomer;?></td><td><?php echo $v->firstname.' '.$v->lastname;?></td><td><?php echo $v->day;?></td><td><?php echo $sale_type;?></td>
				</tr>
		<?php

	}
	?>
	<?php
	}}
	?>
		</tbody>
		<tfoot>
		<tr style="color: blue;">
	<td>
		<?php echo lang('total_cash_sale',FALSE);?>
	</td><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td><td><?php echo number_format($total_cash_sale);?></td>
	</tr>
		<tr style="color: red;">
	<td>
		<?php echo lang('credit_sales_money',FALSE);?>
	</td><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td><td><?php echo number_format($total_credit_sale);?></td>
	</tr>
	<tr style="color: green;">
	<td>
		<?php echo lang('gross_profit',FALSE);?>
	</td><td>--</td><td>--</td><td>--</td><td>--</td><td>--</td><td><?php echo number_format($total_cash_sale+$total_credit_sale);?></td>
	</tr>
	</tfoot>
		</table>
		</div>
	</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>