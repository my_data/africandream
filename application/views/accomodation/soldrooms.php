<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
	<div class="panel-heading"></div>
	<?php require_once(APPPATH.'/views/print.php');?>
	<div class="panel-body">
<div class="panel panel-default">
	<div class="panel-heading"><?php echo $this->lang->line('sold_rooms',FALSE);?>
	</div>
	<div class="panel-body">
	<div class="col-md-12 col-lg-12 col-sm-12">
			
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 		<?php $this->load->view('breadcrumb');?>
			</div>
			<div id="printArea">
		<table class="table table-bordered table-hover" id="mytable2">
			<thead>
				<tr>
					<th><?php echo $this->lang->line('room_number',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('fee',FALSE);?></th><th><?php echo $this->lang->line('cost_per_day',FALSE);?></th><th><?php echo $this->lang->line('customer_name',FALSE);?></th><th><?php echo $this->lang->line('saler',FALSE);?></th><th><?php echo $this->lang->line('time',FALSE);?></th><th><?php echo $this->lang->line('type',FALSE);?></th><th><?php echo $this->lang->line('action',FALSE);?></th>
				</tr>
			</thead>
			<tbody>
			<?php

	if($r=$this->accomodation_model->soldRooms()){
		 $sale_type = 'Unknown';
		 $color="black";
		 $leave_day='';
		foreach ($r as $v) {
			//Convert it into a timestamp.
			$start_day = strtotime($v->day);
			$leave_day = time();
			//Get the current timestamp.
			if ($v->checkout_time!='') {
				$leave_day =strtotime($v->checkout_time); 	
			}	
			
			//Calculate the difference.
			$difference = $leave_day - $start_day;	 
			//Convert seconds into days.
			$days = floor($difference /(60*60*24) );
			if ($days==0) {
					 $days=1;
					 }		 
			$cost = $v->price*$days;
			 if ($v->status==1) {
			 		$color="green";
			 	 $sale_type="Cash Sale";
			 }
			 elseif ($v->status==2) {
			 	$color="red";
			 	$sale_type="credit_sale";
			 }
		?>
				<tr style="color:<?php echo $color;?>">
					<td><?php echo $v->name?></td><td><?php echo $v->category?></td><td><?php echo number_format($v->fee);?></td><td><?php echo number_format($cost);;?></td><td><?php echo $v->roomcustomer;?></td><td><?php echo $v->firstname.' '.$v->lastname?></td><td><?php echo $v->day?></td><td><?php echo $sale_type;?></td>
					<td>
					<div class="dropdown">
					    <button class="btn btn-default dropdown-toggle btn-block" type="button" data-toggle="dropdown"><i class="caret"></i></button>
					    <ul class="dropdown-menu">
					      <li><a href="<?php echo site_url('accomodation/releaseRoom/'.$v->room_ID.'/'.$v->roomsale_id.'/'.$sale_type);?>" class="btn btn-success btn-sm"><?php echo lang('checkout',false);?></a></li>
					      <li><a href="<?php echo site_url('accomodation/cashSale/'.$v->room_ID.'/'.$v->roomsale_id);?>" class="btn btn-primary btn-sm">Cash</a></li>
					      <?php if($this->employee->checkpermision(8)){	
					 			?>
						<li><a data-id="<?php echo $v->roomsale_id;?>" data-target="#edit-sale<?php echo $v->roomsale_id;?>" class="btn btn-warning btn-sm" data-toggle="modal"><?php echo lang('change_button');?></a></li><?php } ?>
					    </ul>
					  </div>
					</td>
				</tr>
		<?php
	}
	?>
		</tbody>
		</table>
		</div>
	</div>
<?php foreach($r as $v){
	?>
		<div class="modal fade" id="edit-sale<?php echo $v->roomsale_id;?>">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color:#337ab7;">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" style="text-align: center;color:white;"><?php echo $this->lang->line('edit_sale_room',FALSE);?></h4>
						</div>
						<div class="modal-body">
							<?php 
							$attrib=array('role'=>'form');
							echo form_open('accomodation/editSale',$attrib);?>
							<center><h3>Customer: <?php echo $v->roomcustomer;?></h3></center>
							<div class="form-group">
							<input type="hidden" name="oldroomid" 
								value="<?php echo $v->room_ID;?>" required/>
							<label for="room">Room No</label>
								<?php
					 			$q="SELECT * FROM rooms WHERE status='EMPTY'";
					 				$result=$this->db->query($q);
					 			?>
					 			<select name="newroomid" style="color: green" class="form-control input-sm">
					 			<option value="">----------</option>
					 				<?php 

					 				if($result->num_rows()!=0){
					 					$res=$result->result();
					 					foreach ($res as $room) {
					 				?>
			    			<option value="<?php echo $room->room_id;?>"><?php echo $room->name.' / '.$room->category.' '.'/ Tsh'.$room->fee.' '.'/ '.$room->status;?> </option>
			    					<?php
			    					}}
			    					?>
			    					</select>
							
							</div>
							<div class="form-group">
							<label for="price"><?php echo $this->lang->line('discount',FALSE);?></label>
								<input type="number" name="discount" class="form-control" min="1" />
								<input type="hidden" name="sale_id" 
								value="<?php echo $v->roomsale_id;?>"/>
								<input type="hidden" name="oldprice" 
								value="<?php echo $v->fee;?>"/>
							</div>
							<div class="alert alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->lang->line('sms',FALSE);?>
							</div>
							<button type="submit" name="save_changes" class="btn btn-success btn-block btn-sm">Save Changes</button>
						</form>
						</div>
						
					</div>
				</div>
			</div>
				<?php
	}}?>
			
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>