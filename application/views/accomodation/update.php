<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('accomodation');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	h5{
		text-align: center;text-transform: uppercase;text-shadow:0.5px 0.5px 0.5px black;
	}
	.jumbotron{
		padding: 5%;
	}
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong><?php echo $this->lang->line('accomodation',FALSE);?></strong></h5>
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<div class="">
		<h6><?php echo $this->lang->line('edit_room',FALSE);?></h6>
		<?php 
								$attrib=array('role'=>'form');
									echo form_open('accomodation/update',$attrib);?>
									
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('room_number',FALSE);?></label>
											<input type="text" class="form-control"  name="roomname" required="required" value="<?php echo $row[0]->name;?>" />
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<input type="text" class="form-control"  name="category" required="required" value="<?php echo $row[0]->category;?>" />
										</div>
										<div class="form-group">
											<label for=""><?php echo $this->lang->line('price',FALSE);?></label>
											<input type="number" class="form-control"   name="fee" required="required" value="<?php echo $row[0]->fee;?>" />
										</div>
										<input type="hidden" name="roomid" required="required" value="<?php echo $row[0]->room_id;?>" />
			<button type="button" class="btn btn-danger" id="concel">Back</button>
			<button type="submit" class="btn btn-success" name="update">Update</button>
		</form>
		</div>
	</div>
</div>