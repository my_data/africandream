<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('accomodation/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>4) {
		window.location.href="<?php echo site_url('accomodation/search/')?>"+this.value;
	}	
});

});
</script>
<div class="panel panel-default">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body"><h5>Rooms Cataloque</h5>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2%;">
	 <?php $this->load->view('breadcrumb');?>
	</div>
	<table class="table table-bordered">
		<thead>
			<tr>
					<th>
					<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('accomodation/search',$attrib);
					 ?>	
			    		<div class="input-group">
			    		<input class="form-control mr-sm-2 input-sm" type="search" id="search" placeholder="Search" aria-label="Search" name="keyword" autofocus="autofocus" />
				      <span class="input-group-btn">
				        <button class="btn btn-secondary btn-sm" name="search_room" type="submit"><i class=" fa fa-search"></i></button>
				      </span>
 					</div>
 					</form>
					</th>
					<th><select name="sortby" id="sortby" class="form-control input-sm">
						<option value=""><?php echo $this->lang->line('filter',FALSE);?></option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select>
					</th>
					<th><?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('accomodation/my_sale',$attrib);?>
					 <button type="submit" name="my_sale" class="btn btn-warning btn-sm btn-block"><?php echo lang('my_sale',FALSE);?></button>
					 	</form>
					 </th>
					<th><?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('accomodation/viewSoldRooms',$attrib);?>
					 <button type="submit" name="soldRooms" class="btn btn-primary btn-sm btn-block"><?php echo $this->lang->line('view_sold_rooms',FALSE);?></button>
					 	</form>
					 </th>
					<th>
					<a class="btn btn-success btn-sm btn-block" data-toggle="modal" href='#modal-id-saleroom'><?php echo $this->lang->line('sale_room',FALSE);?></a>
						<div class="modal fade" id="modal-id-saleroom">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color: blue;"><?php echo $this->lang->line('welcome_to_sale_room',FALSE);?></h4>
									</div>
									<div class="modal-body">
												<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('accomodation/saleRoom',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td><?php echo $this->lang->line('room',FALSE);?></td>
					 			<td>
					 			<?php
					 			$q="SELECT * FROM rooms WHERE status='EMPTY'";
					 				$result=$this->db->query($q);
					 			?>
					 			<select name="room_id" style="color: green" class="form-control input-sm" required="required">
					 			<option value="">--Select Room--</option>
					 				<?php 

					 				if($result->num_rows()!=0){
					 					$res=$result->result();
					 					foreach ($res as $room) {
					 				?>
			    			<option value="<?php echo $room->room_id;?>"><?php echo $room->name.' / '.$room->category.' '.'/ Tsh'.$room->fee.' '.'/ '.$room->status;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    	
			    				<?php } ?>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('customer_name',FALSE);?></td>
					 			<td><input type="text" name="customer" class="form-control input-sm" required="required">
			    				</td>
					 			</tr>
					 			<?php
					 			if($this->employee->checkpermision(8)){	
					 			?>
					 			<tr>
					 			<td><?php echo $this->lang->line('discount',FALSE);?></td>
					 			<td><input type="number" name="discount" class="form-control input-sm" min="1" />
			    				</td>
					 			</tr>
					 				<?php
					 			}
					 			?>
					 			<tr><td><button type="submit" name="creditSale" class="btn btn-warning btn-block"><?php echo $this->lang->line('credit_sale',FALSE);?></button></td><td><button type="submit" name="saleRoom" class="btn btn-success btn-block">Cash Sale</button></td></tr>
					 		</tbody>
					 	</table>
 					</form>	
									</div>
								</div>
							</div>
						</div>
					</th>
				<!-- 	Booking -->
				<th>
					<a class="btn btn-primary btn-sm btn-block" data-toggle="modal" href='#modal-id-booking'><?php echo lang('booking',FALSE);?></a>
						<div class="modal fade" id="modal-id-booking">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
									<h4 class="modal-title" style="text-align: center;color: blue;"><?php echo $this->lang->line('welcome_to_room_booking',FALSE);?>
									<?php 
									$attrib=array('class'=>'form-inline');
									echo form_open('accomodation/showBooking',$attrib);?>
									<button type="submit" name="view-booking" class="btn btn-warning btn-sm pull-right"><?php echo lang('view_bookings');?></button></form></h4>
									</div>
									<div class="modal-body">
												<?php
					$attrib=array('class'=>'form-inline');
					 echo form_open('accomodation/booking',$attrib);
					 ?>	
					 <div class="table-responsive">
					 	<table class="table table-hover">
					 		<thead>
					 			<tr>
					 				<th></th>
					 			</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 			<td><?php echo $this->lang->line('room',FALSE);?></td>
					 			<td>
					 			<?php

					 				$result=$this->db->get('rooms');
					 			?>
					 			<select name="room_id" style="color: green" class="form-control input-sm" required="required">
					 			<option value="">--Select Room--</option>
					 				<?php 

					 				if($result->num_rows()!=0){
					 					$res=$result->result();
					 					foreach ($res as $room) {
					 				?>
			    			<option value="<?php echo $room->room_id;?>"><?php echo $room->name.' / '.$room->category.' '.'/ Tsh'.$room->fee.' '.'/ '.$room->status;?> </option>
			    					<?php
			    					}
			    					?>
			    					</select>
			    	
			    				<?php } ?>
			    				</td>
					 			</tr>
					 			<tr>
					 			<tr>
					 			<td><?php echo $this->lang->line('customer_name',FALSE);?></td>
					 			<td><input type="text" name="customer" class="form-control input-sm" required="required">
			    				</td>
					 			</tr>
					 			<tr>
					 			<td><?php echo lang('day',FALSE);?></td>
					 			<td><input type="date" name="day" class="form-control input-sm" />
			    				</td>
					 			</tr>
					 			<tr><td colspan="2"><button type="submit" name="book" class="btn btn-success btn-block btn-sm"><?php echo $this->lang->line('book',FALSE);?></button></td>
					 			</tr>
					 		</tbody>
					 	</table>
 					</form>	
									</div>
								</div>
							</div>
						</div>
					</th>
				<!-- 	Add new room -->
					<th><div class="pull-right">
					<a class="btn btn-warning btn-sm" data-toggle="modal" href='#modal-id'><?php echo $this->lang->line('add_room',FALSE);?></a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color:#337ab7;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;">ADD ROOM</h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('accomodation/save',$attrib);?>
									<div class="table-responsive">
										<table class="table table-responsive">
											<tbody>
												<tr>
													<td><div class="form-group">
											<label for=""><?php echo $this->lang->line('room_number',FALSE);?></label>
											<input type="text" class="form-control"  name="roomname" required="required">
										</div></td><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('category',FALSE);?></label>
											<select name="category" class="form-control" required="">
												<option value="">--------------------</option>
												<option>Standard</option>
												<option>Deluxe</option>
												<option>Executive Deluxe</option>
												<option>Suite</option>
												<option>Family Room</option>
											</select>
										</div></td>
												</tr>
												<tr><td><div class="form-group">
											<label for=""><?php echo $this->lang->line('retail_price',FALSE);?></label>
											<input type="number" class="form-control"   name="fee" required="required">
										</div></td></tr>
										<tr><td><button type="submit" name="save_room" class="btn btn-primary btn-block"><i class="fa fa-plus"></i></button></td></tr>
											</tbody>
										</table>
									</div>
									
										</form>
								</div>
								
							</div>
						</div>
					</div>
					</div></th>
				</tr>
		</thead>
		
	</table>
		<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr><th></th><th><?php echo $this->lang->line('room_number',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('price',FALSE);?></th><th><?php echo $this->lang->line('status',FALSE);?></th><th colspan="2"><?php echo $this->lang->line('action',FALSE);?></th></tr>
		</thead>
			<tbody>
				<?php
				foreach ($room_result as $row) {
					?>

				<tr>
					<td><input type="checkbox" name="<?php echo $row->name?>" value="<?php echo $row->room_id?>"></td><td><?php echo $row->name?></td><td><?php echo $row->category?></td><td><?php echo $row->fee?></td><td><?php echo $row->status?></td><td colspan="2"><a href="<?php echo site_url('accomodation/update/'.$row->room_id);?>"><i class="fa fa-edit" style="color:green"></i></a>&nbsp &nbsp
					<a href="<?php echo site_url('accomodation/delete/'.$row->room_id);?>"><i class="fa fa-trash-o" style="color:red"></i></a></td>
				</tr>
					<?php
				}

				?>
				
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</div>