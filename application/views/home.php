<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url();?>js/highcharts.js"></script>
<!-- end load library -->

<div class="panel panel-success">
    <div class="panel-heading">
         <span>Welcome to African Dream Conference Center</span>
    </div>
    <div class="panel-body">
<div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
     
<?php
    /* Mengambil query report*/
    if($Report){
    foreach($Report as $result){
        // for ($i=0; $i < count($report); $i++) { 
            # code...
        $bulan[] =  $result->name; //ambil bulan
        $value[] = (float)  $result->price; //ambil nilai
        }
    // }
    /* end mengambil query*/
     
?>
 
<!-- Load chart dengan menggunakan ID -->
<div id="drinks"></div>
<!-- END load chart -->
 
<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#drinks').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Graph Of Today My Drinks Sales',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 10,
                colorByPoint: true,
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>,
             crosshair: true
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Cash Tsh'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The Cash Sale for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Drinks Name',
            data: <?php echo json_encode($value);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
        </script>
    <?php }else{
        ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><strong>African Dream Management Information System,</strong></p><p><small>Ooops! There is No Your Drink sales Today</small></p>
        </div>
        <?php } ?>
        </div>
</div> 
<!-- Displayi food sales graph below -->
<div class="panel panel-default">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
<?php
    /* Mengambil query report*/
    if($food_graph){
    foreach($food_graph as $result){
        // for ($i=0; $i < count($report); $i++) { 
            # code...
        $bulan[] =  $result->name; //ambil bulan
        $value[] = (float)  $result->price; //ambil nilai
        }
    // }
    /* end mengambil query*/
     
?>
 
<!-- Load chart dengan menggunakan ID -->
<div id="food"></div>
<!-- END load chart -->
 
<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#food').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Graph Of Today My Foods Sales',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 20,  colorByPoint: true
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>,
             crosshair: true
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Cash Tsh'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The Cash Sale for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Food',
            data: <?php echo json_encode($value);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
        </script>
    <?php }else{
        ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><strong>African Dream Management Information System,</strong></p><p><small>Ooops! There is No Your Food sales Today</small></p>
        </div>
        <?php } ?>
        </div>
</div>
<!-- Displaying Accomodation/Rooms graph -->
<div class="panel panel-success">
    <div class="panel-heading">
       
    </div>
    <div class="panel-body">
<?php
    /* Mengambil query report*/
    if($room_graph){
    foreach($room_graph as $result){
        // for ($i=0; $i < count($report); $i++) { 
            # code...
        $bulan[] =  $result->room; //ambil bulan
        $value[] = (float)  $result->roomprice; //ambil nilai
        }
    // }
    /* end mengambil query*/
     
?>
 
<!-- Load chart dengan menggunakan ID -->
<div id="rooms"></div>
<!-- END load chart -->
 
<!-- Script untuk memanggil library Highcharts -->
<script type="text/javascript">
$(function () {
    $('#rooms').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: 'Graph Of Today My Rooms Sales',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: '',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 20,  colorByPoint: true
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  <?php echo json_encode($bulan);?>
        },
        exporting: { 
            enabled: false 
        },
        yAxis: {
            title: {
                text: 'Cash Tsh'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The Cash Sale for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Rooms',
            data: <?php echo json_encode($value);?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
        </script>
    <?php }else{
        ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><strong>African Dream Management Information System,</strong></p><p><small>Ooops! There is No Your Room sales Today</small></p>
        </div>
        <?php } ?>
        </div>
</div>
    </div>
</div> 