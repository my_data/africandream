<script type="text/javascript">
	$(document).ready(function(){
		$("#concel").click(function(){
			window.location.href="<?php echo site_url('other_income');?>";
		});

	});
</script>
<style type="text/css">
	h6{
		text-align: center;
	}
	
	input{
		text-align: center;
	}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h5><strong>Other Income</strong></h5>
	</div>
	<div class="panel-body">
	<div class="">
		<center><h5>Edit Income</h5></center>
		<hr>
		<?php 
									$attrib=array('role'=>'form');
									echo form_open('other_income/update',$attrib);?>
									
										<div class="form-group col-lg-8 col-md-8">
										<label for="">Description</label>
										<textarea name="description" id="input" class="form-control" rows="3" required="required" value=""><?php echo $row[0]->des;?></textarea>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<label for="">Money</label>
											<input type="number" class="form-control"   name="money" required="required" value="<?php echo $row[0]->fund;?>">
										</div>
										<input type="hidden" name="income_id" required="required" value="<?php echo $row[0]->income_id;?>" />
						<div class="form-group col-lg-6 col-md-6">	
						<span class="input-group-btn col-lg-6 col-md-6" style="margin-top:5%">
						<button type="button" class="btn btn-danger" id="concel">Back</button>
						<button type="submit" class="btn btn-success" name="update_income">Update</button>
						</span>
						</div>
			
								</div>
		</form>
		</div>
	</div>
</div>