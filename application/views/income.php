<script type="text/javascript">
$(document).ready(function() {
//filtering of drinks
$("#sortby").change(function(){
	window.location.href="<?php echo site_url('other_income/filter/')?>"+this.value;
});
//searching of drinks
$("#search").keyup(function(){
	var value=this.value;
	if (value.length>=2) {
		window.location.href="<?php echo site_url('other_income/search/')?>"+this.value;
	}	
});
});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="well well-sm">
			<h5 style="text-align: center;text-transform: uppercase;text-shadow: 2px;"><strong>OTHER INCOME</strong></h5>
	</div>
		<table class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th><div class="col-lg-4">
			  		<form class="form-inline">
			  		<div class="input-group">
			  		<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			    		<input class="form-control mr-sm-2" type="search" id="search" placeholder="Search" aria-label="Search"/>
			    	</div>
			  		</form>
					</div></th>
					<th><select name="sortby" id="sortby" class="form-control">
						<option value="">Filter</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="50">50</option>
						<option value="100">100</option>
						</select></th>
					<th><div class="pull-right">
					<a class="btn btn-primary btn-block" data-toggle="modal" href='#modal-id'><i class="fa fa-plus"></i></a>
					<div class="modal fade" id="modal-id">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="background-color: #337ab7;">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" style="text-align: center;color:white;">ADD NEW</h4>
								</div>
								<div class="modal-body">
									<?php 
									$attrib=array('role'=>'form');
									echo form_open('other_income/save',$attrib);?>
										<div class="form-group">
										<label for="">Description</label>
											<textarea name="description" class="form-control" rows="3" required="required" autofocus="autofocus"></textarea>
										</div>
								<div class="form-group">
								<select name="source" id="sortby" class="form-control" required="required">
									<option value="Null" selected="selected">Source</option>
									<option value="MAIN Hall">MAIN HALL</option>
									<option value="CONFERENCE P">CONFERENCE P</option>
									<option value="SWIMMING POOL">SWIMMING POOL</option>
									<option value="LAUNDRY">LAUNDRY</option>
									<option value="PROJECTOR">PROJECTOR</option>
									<option value="STATIONARY">STATIONARY</option>
									<option value="SMALL HALL">SMALL HALL</option>
									<option value="BOARD ROOM">BOARD ROOM</option>
									<option value="BANQUET DRINK">BANQUET DRINK</option>
									<option value="OTHERS">OTHERS</option>
								</select>
								</div>
								<div class="form-group">
											<label for="">Money</label>
											<input type="number" class="form-control"   name="money" required="required" placeholder="Tshs.">
										</div>
									
								</div>
								<div class="modal-footer">
										<button type="submit" name="save_income" class="btn btn-primary">Save</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					</div></th>
				</tr>
			</thead>
		</table>
		<table class="table table-hover table-bordered">
			<thead>
				<tr><td>Description</td><td>Money</td><td>Date</td><td>Employee</td><td>Source</td><td colspan="2">Action</td></tr>
			</thead>
			<tbody>
				<?php
				if(isset($income_result)){
				foreach ($income_result as $row) {
					$query="SELECT * FROM employees where employee_id='$row->emp_id'";
					$res=$this->db->query($query);
					$emp_name=$res->result();
					$emp_name=$emp_name[0]->firstname;
					?>

				<tr>
					<td><?php echo $row->des;?></td><td><?php echo $row->fund;?></td><td><?php echo $row->day;?></td><td><?php echo $emp_name;?></td><td><?php echo $row->source;?></td><td colspan="2"><a href="<?php echo site_url('other_income/update/'.$row->income_id);?>"><i class="fa fa-edit" style="color:green"></i></a>&nbsp &nbsp
					<a href="<?php echo site_url('other_income/delete/'.$row->income_id);?>"><i class="fa fa-trash-o" style="color:red"></i></a></td>
				</tr>
					<?php
				}
				}
				?>
				
			</tbody>
		</table>
	</div>
</div>