<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title;?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>login_template/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>login_template/css/main.css">
		<style type="text/css">
								#alert-box{
								position:absolute;
								top: 7%;
								left: 25%;
								background-color:red;
								}	
		</style>
<!--===============================================================================================-->
</head>
<body style="background-color: #708090 !important;">
<script type="text/javascript">
	$("#alert-box").slideDown();
</script>
	<?php if(isset($_SESSION['fail'])){
							?>
							<div class="row align-items-center">
								<div class="col-md-6 col-lg-6 col-md-offset-3" id="alert-box">
										<div class="alert">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<strong><center><?php  echo $this->session->userdata('fail');
										$this->session->unset_userdata('fail');
										?></center></strong>	
									</div>
									</div>
							</div>
		<?php
	} ?>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
	<?php 
		echo form_open('login', array('class' => 'login100-form validate-form p-l-55 p-r-55 p-t-178'));
	?>
					<span class="login100-form-title">
					<h2>AFRICAN DREAM</h2>
				
					</span>

					<div class="wrap-input100 validate-input m-b-16" data-validate="Please enter username">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Please enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>

					
					<div class="container-login100-form-btn">
						<button type="submit" name="login" class="login100-form-btn">
							Sign in
						</button>
					</div>
					<div class="text-right p-t-13 p-b-23">
						<span class="txt1">
							Developed By
						</span>

						<a href="#" class="txt2">
							 /agmacha@yahoo.com 
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>login_template/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>login_template/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>login_template/js/main.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#alert-box").slideUp(10000);
	});
								
	</script>
</body>
</html>