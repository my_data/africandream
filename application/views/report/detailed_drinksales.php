</script>
<div class="panel panel-primary">
	<div class="panel-heading">	
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/drinkSales',$attr);?>
		<div class="table">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>Today:
						<div class="form-group">
		<input type="checkbox" name="today" id="today" class="form-control input-sm">
		</div></th><th>From:<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
			</div></th><th>To:<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
			</div></th><th><div class="form-group">
				<input type="text" name="search" class="form-control input-sm" placeholder="Search">
			</div></th><th>
			<button type="submit" class="btn btn-primary btn-sm btn-block" name="show_detailed_drinks"><?php echo lang('generate_button',false);?></button></th>
					</tr>
				</thead>
			</table>
		</div>	
		</form>	
		<?php $from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
			<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success btn-sm"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body">
			<div class="table-responsive" id="printArea">
				 	<table border="1" class="table table-hover table-striped table-bordered">
				 	 <caption><center><h4 style="color: black;"><?php echo $this->lang->line('detailed_drink_report').' '.'FROM'.' ('.$from.') '.'TO'.' ('.$to.')';?></h4></center></caption>
				 		<thead>
				 			<tr>
				 				<th>ITEM NAME</th><th>CATEGORY</th><th>QUANTITY</th><th>SOLD DATE</th><th>CUSTOMER</th><th>CASH</th><th>SALER</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				if (isset($cash) && isset($sold_items)) {
					$item_capital=0;
					$capital=0;
					if($sold_items){
					$total_sale=$cash->row();
					foreach ($sold_items->result() as $item) {
						$r=get_item_stock($item->item_id);
						$item_capital=($r[0]->size)*$r[0]->cost;
						$capital=$capital+$item_capital;
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->amount;?></td><td><?php echo $item->sale_time;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo number_format($item->price);?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td></tr>	
					<?php
				}
				?>
				<!-- <tr style="color: red;">
				 	<th colspan="3">Drinks Capital:</th><th colspan="3"><?php //echo number_format($capital);?>/=</th>
				 </tr> -->
				<tr style="color: green;">
				 	<th colspan="3"><?php echo lang('gross_profit',false);?></th><th colspan="3"><?php echo number_format($total_sale->value_sum);?>/=</th>
				 </tr>
				 <!-- <tr style="color: green;">
				 	<th colspan="2">Net Profit:</th><th colspan="2"><h3><?php //echo number_format($total_sale->value_sum-$capital);?></h3></th>
				 </tr> -->
				<?php }else{ redirect('reports/drinkSales');}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr>
						<?php 
					}
				 ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>