<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_drinks_stock',$attr);?>
		<div class="table">
			<table class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>
						<?php echo $this->lang->line('from',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control">
							</div>
						</th>
						<th><?php echo $this->lang->line('to',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="show_stocking"><?php echo lang('generate_button',false);?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
		<?php $from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" id="printView" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body" >
			<div class="table-responsive" id="printArea">
				 <table border="1" class="table table-hover table-striped table-bordered table-sm" >
				 <caption style="color:black; padding-bottom:2%;"><center><h4><?php echo $this->lang->line('drinks_stocking').' '.'FROM'.' ('.$from.') '.'TO'.' ('.$to.')';?></h4></center></caption>
				 		<thead style="background-color: silver;">
				 			<tr>
				 				<th>S/N:</th><th><?php echo $this->lang->line('drink',FALSE);?></th>
				 				<th><?php echo $this->lang->line('category');?></th>
				 				<th><?php echo $this->lang->line('open_stock');?></th>
				 				<th><?php echo $this->lang->line('received',FALSE);?></th>
				 				<th><?php echo $this->lang->line('received_cost',FALSE);?></th>
				 				<th><?php echo $this->lang->line('item_cost',FALSE);?></th>
				 				<th><?php echo $this->lang->line('open_stock_capital',FALSE);?></th>	
				 				</th><th><?php echo $this->lang->line('unit_sold',FALSE);?></th>
				 				<th><?php echo $this->lang->line('sale_price',FALSE);?></th>
				 				<th><?php echo $this->lang->line('sales',FALSE);?></th>
				 				<th><?php echo $this->lang->line('cost_of_sales',FALSE);?></th>
				 				<th><?php echo $this->lang->line('profit_per_unit',FALSE);?></th>
				 				<th><?php echo $this->lang->line('spoil',FALSE);?></th>
				 				<th><?php echo $this->lang->line('cost_of_spoil',FALSE);?></th>
				 				<th><?php echo $this->lang->line('complementary',FALSE);?></th>
				 				<th><?php echo $this->lang->line('cost_of_comp',FALSE);?></th>
				 				<th><?php echo $this->lang->line('close_stock',FALSE);?></th>
				 				<th><?php echo $this->lang->line('close_stock_capital',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$received_cost=0;
				$total_received_cost=0;
				$cost_of_sales=0;
				$sum_of_cost_of_sales=0;
				$sum_of_profit_per_unit=0;
				$cost_of_goods_sold=0;
				$total_open_stock_cost=0;
				$total_close_stock_cost=0;
				$total_sales=0;
				$gross_profit=0;
				$total_capital=0;
				$item_cost=0;
				$item_capital=0;
				$total_cost_of_spoil=0;
				$total_cost_of_comp=0;
				if (isset($result)){
					$sn=1;
					if ($result) {
					foreach ($result as $r) {
						$comp='-';
						/* function to get the cost of drink*/
					foreach($this->report->drinks_item_cost($r->item_id) as $r5){
					/* function to get the sum of drinks sold*/
					foreach($this->report->drinks_sum_sold($r->item_id) as $r2){
					/* function to get the sum of drinks received today by individual*/
					foreach($this->report->drinks_sum_received($r->item_id) as $r3){
						/* function to get the sum of drinks spoiled*/
					foreach($this->report->drinks_sum_spoiled($r->item_id) as $r4){
						/* function to get the sum of drinks complementary*/
					foreach($this->report->drinks_sum_comp($r->item_id) as $r6){
					/* function to get the sum of absorbed drinks*/
					foreach($this->report->drinks_sum_absorbed($r->item_id) as $r7){
					$comp=($r6->comp)+($r7->absorbed);
					$open_stock=(($r->close_stock+$r2->qty_sold+$r4->spoiled+$comp)-($r3->qty_received));
					$item_capital=$r5->item_cost*$open_stock;
					$total_capital=$total_capital+$item_capital;
					$total_open_stock_cost=$total_capital;
					$total_close_stock_cost=$total_close_stock_cost+($r->close_stock*$r5->item_cost);
					$total_sales=$total_sales+($r2->qty_sold*$r->saleprice);
					$gross_profit=$gross_profit+($r2->qty_sold*$r->saleprice);
					$cost_of_sales=$r5->item_cost*$r2->qty_sold;
					$sum_of_cost_of_sales=$sum_of_cost_of_sales+$cost_of_sales;
					$profit_per_unit=($r2->qty_sold*$r->saleprice)-$cost_of_sales;
					$sum_of_profit_per_unit=$sum_of_profit_per_unit+$profit_per_unit;

					$cost_of_spoil=$r4->spoiled*$r5->item_cost;
					$total_cost_of_spoil=$total_cost_of_spoil+$cost_of_spoil;
					$cost_of_comp=$comp*$r5->item_cost;
					$total_cost_of_comp=$total_cost_of_comp+$cost_of_comp;

					$received_cost=$r3->qty_received*$r5->item_cost;
					$total_received_cost=$total_received_cost+$received_cost;
							/*
					check if drink sold an then include into total cost of item sold below
					*/
					if (!empty($r2->qty_sold)) {
							$cost_of_goods_sold=$cost_of_goods_sold+$item_capital;
						}	
						//Addition of absorbed drinks and complementary
								if (empty($comp)) {
									$comp=0;
								}
							/* function to check if there is no received drinks and put dash*/
							if (empty($r3->qty_received)) {
									$r3->qty_received=0;
							}
							/* function to check if there is no spoiled drinks and put dash*/
							if (empty($r4->spoiled)) {
									$r4->spoiled=0;
							}
							/* function to check if there is no sold drinks and put dash*/
							if (empty($r2->qty_sold)) {
									$r2->qty_sold=0;
							}
					?>	 			
				 			<tr>
				 			<td><?php echo $sn;?></td><td><?php echo $r->name;?></td>
				 			<td><?php echo $r->catgry;?></td><td><?php echo $open_stock;?></td>
				 			<td><?php echo $r3->qty_received;?></td>
				 			<td><?php echo number_format($received_cost);?>/=</td>
				 			<td><?php echo number_format($r5->item_cost);?>/=</td>
				 			<td><?php echo number_format($item_capital);?>/=</td>
				 			<td><?php echo $r2->qty_sold;?></td>
				 			<td><?php echo number_format($r->saleprice);?>/=</td>
				 			<td><?php echo number_format($r2->qty_sold*$r->saleprice);?>/=</td>
				 			<td><?php echo number_format($cost_of_sales);?>/=</td>
				 			<td><?php echo number_format($profit_per_unit);?>/=</td>
				 			<td><?php echo $r4->spoiled;?></td>
				 			<td><?php echo number_format($cost_of_spoil);?></td>
				 			<td><?php echo $comp;?></td><td><?php echo number_format($cost_of_comp);?></td>
				 			<td><?php echo $r->close_stock;?></td>
				 			<td><?php echo number_format($r->close_stock*$r5->item_cost);?>/=</td>
				 			</tr>
				 		
					<?php 
					$sn++;
					}}}}}}
				}	
				?>
				<?php 
				} 
				?>
				
				<tr><td colspan="20"></td></tr>
				<tr style="color:green;"><th colspan="4">Total</th><th></th>
				<th><?php echo number_format($total_received_cost);?>/=</th><th></th>
				<th><?php echo number_format($total_open_stock_cost);?>/=</th><th></th><th></th>
				<th><?php echo number_format($total_sales);?>/=</th>
				<th><?php echo number_format($sum_of_cost_of_sales);?>/=</th>
				<th><?php echo number_format($sum_of_profit_per_unit);?>/=</th><th></th>
				<th><?php echo number_format($total_cost_of_spoil);?>/=</th><th></th>
				<th><?php echo number_format($total_cost_of_comp);?>/=</th><th></th>
				<th><?php echo number_format($total_close_stock_cost);?>/=</th></tr>
				<tr style="color:green;"><th colspan="10"><?php echo lang('cost_of_goods_sold',false);?></th><th colspan="10"><?php echo number_format($sum_of_cost_of_sales);?>/=</th></tr>
				<tr style="color:green;"><th colspan="10"><?php echo lang('profit',false);?></th>
				<th colspan="10"><?php echo number_format($gross_profit-$sum_of_cost_of_sales);?>/=</th>
				</tr>
				<!--Calculation of Trading Account Below -->
				<tr><td colspan="20"></td></tr>
				<tr><td colspan="20" style="text-align: center;"><h4>TRADING ACCOUNT</h4></td></tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Total Sales</td><td></td><td></td><td></td><td></td><td></td><td><?php echo number_format($total_sales);?>/=</td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Open Stock</td><td></td><td></td><td></td>
<td><?php echo number_format($total_open_stock_cost);?>/=</td><td></td><td></td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Add Addition</td><td></td>
<td><?php echo number_format($total_received_cost);?>/=</td><td></td>
<td><?php echo number_format($total_received_cost+$total_open_stock_cost);?>/=</td><td></td><td></td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Less Spoilage & Comp</td><td></td>
<td><?php echo number_format($total_cost_of_spoil+$total_cost_of_comp);?>/=</td><td></td>
<td><?php echo number_format($total1=($total_received_cost+$total_open_stock_cost)-($total_cost_of_spoil+$total_cost_of_comp));?>/=</td><td></td><td></td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Goods Available For Sale</td><td></td><td></td><td></td><td><?php echo number_format($total1);?>/=</td><td></td><td></td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Less Clossing Stock</td><td></td>
<td><?php echo number_format($total_close_stock_cost);?>/=</td><td></td><td>
<?php echo number_format($total1-$total_close_stock_cost);?>/=</td><td></td><td></td>
				</tr>
				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Cost Of Goods Sold</td><td></td><td></td><td></td><td></td><td></td><td><?php echo number_format($total2=$total1-$total_close_stock_cost);?>/=</td>
				</tr>
<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3"></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>

				<tr>
<td></td><td></td><td></td><td colspan="3"></td><td colspan="3">Gross Profit=(Sales-Cost Of Goods Sold)</td><td></td><td></td><td></td><td></td><td>=</td><td><?php echo number_format($total_sales-$total2);?>/=</td>
				</tr>
				<?php
				}
				?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>