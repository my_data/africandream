<style type="text/css">
	.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
        margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
<div class="panel panel-default">
	<div class="panel-heading">	
	</div>
	<div class="panel-body">
    <div class="panel panel-success">
        <div class="panel-body">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <?php $this->load->view('breadcrumb');?>
    </div>
    <!-- Drob down menu for listing report options -->
    <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <center style="padding:3%;"><h3>REPORTS CONSOLE</h3></center>
        
        </div>
             <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">     
            <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-warning btn-sm" data-target="dropdownMenu4">
                Others<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu4">
            <li><a href="<?php echo site_url('reports/daily_summary');?>" class="list-group-item"><?php echo lang('daily_summary');?></a></li>
            </ul>
        </div>
        </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
            <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-primary btn-sm" data-target="dropdownMenu">
                Drinks<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
             <li><a href="<?php echo site_url('reports/get_drinks_stock');?>" class="list-group-item">Drinks Stocking</a></li>
              <li><a href="<?php echo site_url('reports/drinkSales/summary');?>" class="list-group-item">Summary</a></li>
                <li><a href="<?php echo site_url('reports/drinkSales/detailed');?>" class="list-group-item">Datailed</a></li>
                <li><a href="<?php echo site_url('reports/suspendedDrinkSales');?>" class="list-group-item">Credit Sales</a></li>
                <li><a href="<?php echo site_url('reports/get_complementary');?>" class="list-group-item">Complementary</a></li>
                <li><a href="<?php echo site_url('reports/get_spoil');?>" class="list-group-item">Spoiled  Drinks</a></li>
                <li><a href="<?php echo site_url('reports/get_receiving');?>" class="list-group-item">Receiving</a></li>
                <li><a href="<?php echo site_url('drinks_order/get_drink_orders');?>" class="list-group-item">Sales Orders</a></li>
            </ul>
        </div>
        </div>

        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
              <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-warning btn-sm" data-target="dropdownMenu2">
                Food<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu2">
              <li><a href="<?php echo site_url('reports/getSummaryFoodSales');?>" class="list-group-item">Summary</a></li>
                <li><a href="<?php echo site_url('reports/getDetailedFoodSales');?>" class="list-group-item">Datailed</a></li>
                <li><a href="<?php echo site_url('reports/foodCreditSales');?>" class="list-group-item">Credit Sales</a></li>
            </ul>
        </div>
        </div>

        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">     
        <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-primary btn-sm" data-target="dropdownMenu3">
                Employees<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu3">
              <li><a href="<?php echo site_url('reports/get_customers');?>" class="list-group-item">Summary</a></li>
            </ul>
        </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">     
            <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-warning btn-sm" data-target="dropdownMenu4">
                Expenditures<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu4">
              <li><a href="<?php echo site_url('reports/get_expenditures');?>" class="list-group-item">Summary</a></li>
            </ul>
        </div>
        </div>
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">     
            <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-primary btn-sm" data-target="dropdownMenu10">
                Other Income<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu10">
              <li><a href="<?php echo site_url('reports/get_otherincome');?>" class="list-group-item">All</a></li>
            </ul>
        </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">     
        <div class="dropdown">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-warning btn-sm" data-target="dropdownMenuu">Accomodation<span class="caret"></span>
            </a>
            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenuu">
            <li>
            <a href="<?php echo site_url('reports/soldrooms_summary');?>" class="list-group-item" hidden="true">Summary</a>
            </li>
            <li>
              <a href="<?php echo site_url('reports/get_soldrooms');?>" class="list-group-item">Detailed</a> </li>
            </ul>
        </div>
        </div>
        <!-- end of dropdown menu report for drinks -->
    </div>
        </div>
    </div>
	
</div>
