<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
	<div class="panel-heading">	
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_customers',$attr);?>
		<div class="table">
			<table class="table table-hover">
				<thead>
					<tr>
						<th><div class="form-group">
				Today:<input type="checkbox" name="today" id="today" class="form-control input-sm">
		</div></th><th><div class="input-group">
				From:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
			</div></th><th><div class="input-group">
				To:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
			</div></th><th>
			<button type="submit" class="btn btn-primary btn-sm" name="show_employees">Show</button></th>
					</tr>
				</thead>
				
			</table>
		</div>	
		</form>	
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
			<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body">
			<div class="table-responsive" id="printArea">
				 	<table border="1" class="table table-hover table-striped table-bordered" id="mytable2">
				 	<caption><center><font size="5px;" color="blue";> Employees Report </font></center></caption>
				 		<thead>
				 			<tr>
				 				<th>WAITER/WAITRESS</th><th>TOTAL SALES (CASH)</th>
				 		</thead>
				 		<tbody>
				<?php
				$max_foodsale=0;
				$foodsaler="None";
				$max_sale=0;
				$best_saler="None";
				if($customers=$this->report->get_customers_report()){
					 $food_salers=getBestFoodSaler();
					 foreach ($food_salers as $key) {
					 	//algorisim for finding the best food saler
					 	
					 	if ($max_foodsale<$key->food_sales) {
							$max_foodsale=$key->food_sales;
							$foodsaler=$key->firstname.' '.$key->lastname;
						}
					 }
					
					foreach ($customers->result() as $customer){
						//algorism to findout the best saler for both drinks and food
						$r=count_customer_sales($customer->saler_id);
						if ($max_sale<$r[0]->total_sales) {
							$max_sale=$r[0]->total_sales;
							$best_saler=$r[0]->firstname.' '.$r[0]->lastname;
						}
						//displaying results
					?>
				 		<tr><td><?php echo $customer->firstname.' '.$customer->lastname;?></td>
				 		<td><?php echo number_format($r[0]->total_sales);?></td>
				 		</tr>	
					<?php
				}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr>
						<?php 
					}
				 ?>
				 <tr style="color: green;"><td>Best Drinks Saler:</td><td><?php echo $best_saler;?></td></tr><tr style="color: green;"><td>Best Food Saler:</td><td><?php echo $foodsaler;?></td></tr>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>