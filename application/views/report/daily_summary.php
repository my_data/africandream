<div class="panel panel-default">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/daily_summary',$attr);?>
		<div class="table">
			<table class="table table-sm">
				<thead>
					<tr>
						<th>
						<?php echo $this->lang->line('from',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span>
							<input type="date" name="from" id="from" class="form-control">
							</div>
						</th>
						<th><?php echo $this->lang->line('to',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon input-sm"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-success btn-block btn-sm" name="show_summary"><?php echo lang('generate_button',false);?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
		<?php $from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" id="printView" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body" >
			<div class="table-responsive" id="printArea">
				 <table border="1" class="table table-hover table-striped table-bordered table-sm" >
				 <caption style="color:black; padding-bottom:2%;"><center><h4><?php echo $this->lang->line('daily_summary').' '.'From'.' ('.$from.') '.'To'.' ('.$to.')';?></h4></center></caption>
				 		<thead style="background-color: silver;">
				 			<tr>
				 				<th>S/N:</th><th><?php echo $this->lang->line('source',FALSE);?></th>
				 				<th><?php echo $this->lang->line('CASH');?></th>
				 				<th><?php echo $this->lang->line('ADV_CASH');?></th>
				 				<th><?php echo $this->lang->line('in_out',FALSE);?></th>
				 				<th><?php echo $this->lang->line('debt_recovered',FALSE);?></th>	
				 				<th><?php echo $this->lang->line('credit',FALSE);?></th>
				 				<th><?php echo $this->lang->line('total',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$gross_profit=0;
				$total_expend=0;
				$total_cash=0;
				$total_credit=0;
				if (isset($income_result)){

					$sn=1;
					if ($income_result) {
					foreach ($income_result as $r) {

						$comp='-';
						/* function to get the sum of drinks capital*/
				foreach($this->report->sum_sources($r->source) as $r5){
						$total_cash=$total_cash+$r5->fund;
						$gross_profit=$gross_profit+$r5->fund;	
					?>	 			
				 			<tr>
				 			<td><?php echo $sn;?></td>
				 			<td><?php echo $r->source;?></td>
				 			<td><?php echo number_format($r5->fund);?>/=</td>
				 			<td></td><td></td><td></td><td></td>
				 			<td><?php //echo number_format($r5->fund);?></td>
				 			</tr>
				 		
					<?php
					$sn++;
					}
				}	
				}
				?>
				<tr>
					<td><?php echo $sn;?></td>
					<td>RESTAURANT</td>
					<td><?php echo number_format($this->report->restaurant_cash_money());?>/=</td>
					<td></td><td></td><td></td>
					<td><?php echo number_format($this->report->restaurant_credit_money());?>/=</td>
					<td><?php //echo number_format($this->report->restaurant_profit());?></td>
				</tr>
				<tr>
					<td><?php echo $sn;?></td>
					<td>ACCOMODATION</td>
					<td><?php echo number_format($this->report->accomodation_cash_money());?>/=</td>
					<td></td>
					<td></td><td></td>
					<td><?php echo number_format($this->report->accomodation_credit_money());?>/=</td>
					<td><?php //echo number_format($this->report->accomodation_profit());?></td>
				</tr>
				<tr>
					<td><?php echo $sn;?></td>
					<td>BAR</td>
					<td><?php echo number_format($this->report->bar_cash_money());?>/=</td>
					<td></td><td></td><td></td>
					<td><?php echo number_format($this->report->bar_credit_money());?>/=</td>
					<td><?php //echo number_format($this->report->bar_profit());?></td>
				</tr>
				<tr style="color: blue;">
					<td><?php echo $sn="";?></td>
					<td>TOTAL</td>
					<td><?php echo number_format($total_cash+$this->report->restaurant_cash_money()+$this->report->accomodation_cash_money()+$this->report->bar_cash_money());?>/=
					</td>
					<td></td><td></td><td></td>
					<td><?php echo number_format($this->report->restaurant_credit_money()+$this->report->accomodation_credit_money()+$this->report->bar_credit_money());?>/=
					</td>
					<td><?php //echo number_format($gross_profit+$this->report->accomodation_profit()+$this->report->restaurant_profit()+$this->report->bar_profit());?>
					</td>
				</tr>
					<!-- 	Display break line -->
					<tr>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<!-- 	Displaying Bank withdraw -->
					<tr>
						<td><?php echo $sn="";?></td>
						<td>Draw From Bank</td><td></td>
						<td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td><?php echo $sn="";?></td>
						<td>Balance bd</td><td></td><td></td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td><?php echo $sn="";?></td>
						<td>Total Cashdebt Rec.Adv</td><td></td>
						<td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr style="color:green;">
						<td><?php echo $sn="";?></td>
						<td>TOTAL</td><td></td><td></td>
						<td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
				<!-- 	Displaying expenditures sources -->
					<tr style="color:blue;">
						<td></td><td></td><td></td><td>EXPENDITURES</td><td></td><td></td><td></td><td></td>
					</tr>
					<?php
					$counter2=0;
					 foreach ($expend_result as $rs){
					 	$counter2++;
							/* function to get the sum of expenditure capital*/
						$total_expend=$total_expend+$this->report->sum_expendsources($rs->source);
						?>
						<tr>
							<td><?php echo $counter2;?></td>
							<td><?php echo $rs->source;?></td><td></td><td></td><td></td><td></td><td></td>
							<td><?php echo number_format($this->report->sum_expendsources($rs->source));?>/=</td>
						</tr>
						<?php
					}?>
				<?php
				}
				?>
				<tr style="color: blue;">
					<td></td>
					<td>TOTAL</td><td></td><td></td><td></td><td></td><td></td>
					<td><?php echo number_format($total_expend);?>/=</td>
				</tr>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>