<div class="panel panel-primary">
	<div class="panel-heading">
	</div>
	<?php $from=strtotime($this->session->userdata('from'));
				$to=strtotime($this->session->userdata('to'));
				$from=date('d/m/Y',$from);
				$to=date('d/m/Y',$to);
		?>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/drinkSales/summary',$attr);?>
		<div class="table">
			<table class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>
						<?php echo $this->lang->line('today',FALSE);?>
						<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="checkbox" name="today" id="today" class="form-control">
						</div>
						</th>
						<th><?php echo $this->lang->line('from',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
							</div>
						</th>
						<th><?php echo $this->lang->line('to',FALSE);?>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
							</div>
						</th>
						<th>
						<select name="employee_id" class="form-control">
								<option value="">-- Select Employee --</option>
								<?php 
								$employees=$this->db->get('employees');
								if ($employees->num_rows()>0) {
									foreach ($employees->result() as $employees) {
								?>
								<option value="<?php echo $employees->employee_id?>"><?php echo $employees->firstname.' '.$employees->lastname;?></option>
								<?php	
								}}
								?>
							</select>
							</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="show_summary_drinks"><?php echo lang('generate_button',false);?>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
		<div class="panel panel-default">
			<?php require_once(APPPATH.'/views/print.php');?>
				<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" onclick="printData('printArea')" id="printView" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body" >
			<div class="table-responsive" id="printArea">
				 <table border="1" class="table table-hover table-striped table-bordered table-sm" >
				 <caption><center><font size="5px;" color="blue";><?php echo $this->lang->line('summary_drink_report').' '.'FROM'.' ('.$from.') '.'TO'.' ('.$to.')';?> </font></center></caption>
				 		<thead>
				 			<tr>
				 				<th><?php echo $this->lang->line('drink',FALSE);?></th><th><?php echo $this->lang->line('category',FALSE);?></th><th><?php echo $this->lang->line('sold_quantity',FALSE);?></th><th><?php echo $this->lang->line('cash',FALSE);?></th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php
				$max_qty=0;
				$item_capital=0;
				$capital=0;
				$net=0;
				$max_name="None";
				if (isset($cash) && isset($sold_item_name)){
					if ($sold_item_name) {
					$total_sale=$cash->row();
					foreach ($sold_item_name->result() as $item) {
					$res=get_sold_item_detail($item->item_id,$this->session->userdata('from'),$this->session->userdata('to'));
					 foreach ($res as $r) {
					 	$item_capital=($r->size+$r->qty)*$r->capital;
					 		$capital=$capital+$item_capital;
					 		//finding most sold drink algorism
					 		if($max_qty<$r->qty){
					 			$max_qty=$r->qty;
					 			$max_name=$r->name;
					 		}
					?>	 			
				 			<tr><td><?php echo $r->name;?></td><td><?php echo $r->category;?></td><td><?php echo $r->qty;?></td><td><?php echo number_format($r->cash);?>/=</td></tr>
				 		
					<?php
				}}
				
				?>
				<tr><td>--</td><td>--</td><td>--</td><td>--</td></tr>
				<tr style="color: green;">
				 	<th><i class="fa fa-spinner fa-spin"></i>&nbsp Most sold Drink:</th><th><?php echo $max_name;?></th><th><?php echo $max_qty;?></th>
				 </tr>
				<tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('drinks_capital',FALSE);?>
				 	</th><th colspan="2"><?php echo number_format($capital);?>/=</th>
				 </tr> 
				 <tr style="color: red;">
				 <th colspan="2"><?php echo $this->lang->line('total_complementary_drinks',FALSE);?></th><th colspan="2"><?php echo get_total_complementary_drinks();?></th>
				 </tr>
				 <tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('total_spoil_drinks',FALSE);?>
				 	</th><th colspan="2"><?php echo get_total_spoil_drinks();?></th>
				 </tr>
				 <tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('drinks_absorbed_to_conference',FALSE);?>
				 	</th><th colspan="2"><?php echo get_absorbed_conference();?></th>
				 </tr>
				 <tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('drinks_absorbed_to_restaurant',FALSE);?></th><th colspan="2"><?php echo get_absorbed_restaurant();?></th>
				 </tr>
				 <tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('drinks_absorbed_to_rooms',FALSE);?></th><th colspan="2"><?php echo get_absorbed_rooms();?></th>
				 </tr>
				 <tr style="color: red;">
				 	<th colspan="2"><?php echo $this->lang->line('total_credit_sales',FALSE);?>
				 	</th><th colspan="2"><?php echo number_format(get_total_suspended_sales());?>/=</th>
				 </tr>
				 <tr style="color: green;">
				 	<th colspan="2"><?php echo $this->lang->line('total_cash_sales',FALSE);?></th><th colspan="2"><?php echo number_format($total_sale->value_sum);?>/=</th>
				 </tr>
				 <tr style="color: green;">
				 	<th colspan="2"><?php echo $this->lang->line('gross_profit',FALSE);?>
				 	</th><th colspan="2"><?php echo $net=number_format(get_total_suspended_sales()+$total_sale->value_sum);?>/=</th>
				 </tr>
				 <tr style="color: blue;">
				 	<th colspan="2"><?php echo lang('net_profit');?></th><th colspan="2"><h4><?php echo number_format($net-$capital);?>/=</h4></th>
				 </tr>
				<?php 
				}
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 NO Record Found!
						</div></td></tr><?php
					
					} ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
</div>