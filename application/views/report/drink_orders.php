<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>

<div class="panel panel-primary">
			<div class="panel-heading"></div>
			<div class="panel-body">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
			
				<h3 style="text-align: center;">List of Drinks Orders</h3>
				 		 			
			<div class="table-responsive">
				 	<table class="table table-hover table-striped table-bordered" id="mytable1">
				 		<thead>
				 			<tr>
				 				<th>NAME</th><th>CATEGORY</th><th>QUANTITY</th><th>DATE</th><th>Customer</th><th>PRICE</th><th>SOLD BY</th><th>STATUS</th>
				 			</tr>
				 		</thead>
				 		<tbody>
				<?php

				$result_obj=$this->db->query("SELECT * FROM drink_order INNER JOIN drinks ON drink_order.drink_id=drinks.item_id INNER JOIN employees ON employee_id=saler_id");

				if ($result_obj) {
					if ($result_obj->num_rows()<1){
							?>
				 		<tr><td colspan=""><div class="alert alert-danger" style="text-align: center;">
				 			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				 			No Records Found!
				 		</div></td></tr>
				
				 <?php
						}else{
					foreach ($result_obj->result() as $item) {
						if ($item->status==1) {
							$status="Ready";
						}
						if ($item->status==0) {
							$status="Note Ready";
						}
					?>
				 		<tr><td><?php echo $item->name;?></td><td><?php echo $item->category;?></td><td><?php echo $item->qty;?></td><td><?php echo $item->day;?></td>
				 			<td><?php echo $item->customer;?></td><td><?php echo number_format($item->price);?></td><td><?php echo $item->firstname.' '.$item->lastname;?></td><td><?php echo $status;?></td></tr>
				 		
					<?php
				}
			}
				?>
				<?php }
				?>
				</tbody>
				</table>
		</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable1").DataTable();
			});
		</script>
</div>