<div class="panel panel-primary">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title">Expenditures</h3>
		</div>
		<div class="panel-body">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_expenditures',$attr);?>
		<div class="table">
			<table class="table">
				<thead>
					<tr>
						<th><div class="input-group">
						Today:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="checkbox" name="today" id="today" class="form-control input-sm">
						</div>
						</th>
						<th>
							<div class="input-group">
							From:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="from" id="from" class="form-control input-sm">
							</div>
						</th>
						<th>
							<div class="input-group">
							To:<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="date" name="to" id="to" class="form-control input-sm">
							</div>
						</th>
						<th>
					<button type="submit" class="btn btn-primary btn-block btn-sm" name="show_expenditures"><i class="fa fa-eye"></i>
						</button>
						</th>
					</tr>
				</thead>	
			</table>
		</div>
		</form>	
				<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>Description</th><th>Time</th><th>Employee</th><th>Cost</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				if (isset($result)) {
					foreach ($result->result() as $v) {
						?>
					<tr>
						<td><?php echo $v->des; ?></td><td><?php echo $v->day; ?></td><td><?php echo $v->firstname.' '.$v->lastname; ?></td><td><?php echo $v->cost; ?></td>
					</tr>
						<?php
					}
					?>
					<tr style="color: red;"><td>Total Cost Tsh:</td><td><?php echo $v->totalcost; ?></td></tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
		</div>
	</div>
		
	</div>
</div>