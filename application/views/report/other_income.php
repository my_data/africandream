<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>datatable/datatables.min.css"/>
<div class="panel panel-primary">
	<div class="panel-heading">	
	</div>
	<div class="panel-body">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	 <?php $this->load->view('breadcrumb');?>
	</div>
		<?php 
		$attr=array('class'=>'form-inline','role'=>'form');
		echo form_open('reports/get_customers',$attr);?>
		<div class="table">
			<table class="table table-hover">
				<thead>
			
				</thead>
				
			</table>
		</div>	
		</form>	
		<div class="panel panel-default">
			
			<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<button type="button" id="print" class="btn btn-success btn-sm pull-right"><i class="fa fa-print"></i></button>
			</div>
			</div>
			<div class="panel-body">
			<div class="table-responsive" id="printArea">
				 	<table border="1" class="table table-hover table-striped table-bordered" id="mytable2">
				 	<caption><center><font size="5px;" color="blue";> Other Income Report </font></center></caption>
				 		<thead>
				 			<tr>
				 				<th>Description</th><th>Money (CASH)</th><th>Date</th><th>Employee</th>
				 		</thead>
				 		<tbody>
				<?php
				$res=$this->db->query("select * from income inner join employees on emp_id=employee_id");
				if($res->num_rows()>0){
					 foreach ($res->result() as $v) {
					 	
					?>
				 		<tr><td><?php echo $v->des;?></td><td><?php echo $v->fund;?></td><td><?php echo $v->day;?></td><td><?php echo $v->firstname.' '.$v->lastname;?></td>
				 		</tr>	
					<?php }
					?>
					<?php
				}
				else{
						?><tr><td><div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							 Select Record!
						</div></td></tr>
						<?php 
					}
				 ?>
				</tbody>
				</table>
				 </div>
			</div>	
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url();?>datatable/datatables.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#mytable2").DataTable();
			});
		</script>
</div>