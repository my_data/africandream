<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $title;?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<?php echo base_url();?>" />
<!-- css links -->
<link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>css/styles.css" rel="stylesheet" type="text/css" />
<!-- javascripts links -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/print.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<style type="text/css">			
</style>
</head>
<body id="whole-body">
<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="header">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<h2 id="head-title"><?php echo $this->lang->line('header_title');?></h2>
				</div>
						<?php if(isset($_SESSION['success'])){
							?>
							<style type="text/css">
								#alert-box{
								position:absolute;
								top: 70%;
								}	
							</style>
							<script type="text/javascript">
								$("#alert-box").slideDown();
							</script>
							<div class="row align-items-center" >
								<div class="col-md-6 col-md-offset-3" id="alert-box">
										<div class="alert  alert-success" style="background-color: green;">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<strong><center style="color:white;"><?php  echo $this->session->userdata('success');
										$this->session->unset_userdata('success');
										?></center></strong>	
									</div>
									</div>
								</div>
							<?php
						} ?>

						<?php if(isset($_SESSION['fail'])){
							?>
							<style type="text/css">
								#alert-box{
								position:absolute;
								top: 80%;
								}	
							</style>
							<script type="text/javascript">
								$("#alert-box").slideDown();
							</script>
							<div class="row align-items-center" >
								<div class="col-md-6 col-md-offset-3" id="alert-box">
										<div class="alert " style="background-color: orange;">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<strong><center><?php  echo $this->session->userdata('fail');
										$this->session->unset_userdata('fail');
										?></center></strong>	
									</div>
									</div>
								</div>
							<?php
						} ?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right" id="loginAs">
						<div class="dropdown">
					    <button style="border-radius: 50%;" class="btn btn-default dropdown-toggle btn-block" type="button" data-toggle="dropdown">
					    &nbsp<?php echo $this->lang->line('login_as',FALSE);?>&nbsp<?php echo $this->session->userdata('username');?><i class="caret"></i></button>
					    <ul class="dropdown-menu">
					      <li><a href="<?php echo site_url('staff/update_password')?>">
					      <?php echo $this->lang->line('change_password',FALSE);?></a></li>
					      <li><a href="<?php echo site_url('home/Logout')?>"><?php echo $this->lang->line('logout',FALSE);?></a></li>
					    </ul>
					  </div>
					</div>
				</div>
				</div>
			</div>
			<div class="row">

					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 pull-left" id="menu">
						
						<ul class="nav nav-pills nav-stacked" >
						<a href="<?php echo site_url('home')?>" style="color: white; text-decoration: none;"><h5><i class="fa fa-dashboard"></i>&nbsp<?php echo $this->lang->line('dashboard',FALSE);?></h5></a>
						<?php 
						$counter=1;
						foreach($allowed_modules->result() as $module){
							/*selecting number of drinks order and the set number of orders*/
							$total_order='';
							if ($module->module_id==12) {
							$qry="SELECT * FROM drink_order WHERE status=0";
							$r=$this->db->query($qry);
							$number_order=$r->num_rows();
							$total_order='<span class="badge">'.$number_order.'</span>';
							}
							/*selecting number of food order and the set number of orders*/
							if ($module->module_id==13) {
							$qry="SELECT * FROM food_order WHERE status=0";
							$r=$this->db->query($qry);
							$number_order=$r->num_rows();
							$total_order='<span class="badge">'.$number_order.'</span>';
							}//end of selection
							?>
							<li style="margin-top: 5%;">
							<a href="<?php echo site_url('home/get_module/'.$module->module_id);?>">
							<i class="fa fa-folder"></i>
							&nbsp<?php echo $module->name;?> &nbsp 
							<i class="arrow fa fa-angle-right"></i><?php echo $total_order;?></a>
							</li>
							<?php
							$counter++;
							}?>
							
						</ul>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right" id="content">
						<?php $this->load->view($page);?>
					</div>
			</div>
</div>		
</body>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#alert-box").slideUp(5000);
	});
								
	</script>
</html>