<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Food extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getall(){
 		$this->db->trans_begin();
 		$query="SELECT * FROM food ORDER BY foodname ASC";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM food ORDER BY foodname ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveFood($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	
 	function search($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function saleFood($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
 	function get_food_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	function suspenditem_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	

 	function update_food($query)
	{
		$this->db->trans_begin();
		$this->db->query($query);
 		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function remove_food($id=0)
	{
		$query="DELETE FROM food WHERE food_id='$id'";
		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;		
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}

	function get_suspended_sales(){
		$saler=(int)$this->session->userdata('person_id');
		$query="SELECT * FROM foodSales inner join employees on foodSales.saler_id=employees.employee_id  WHERE status=0 AND saler_id=$saler";

		if($result=$this->db->query($query)){
			return $result;
		}
		else{
			return false;
		}
	}

	function remove_suspended($query){
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
 			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	function suspended_particular($query){
		if($result=$this->db->query($query)){
			return $result;
		}
		else{
			return false;
		}
	}


	function delete_suspended($id){
		$query="DELETE FROM foodSales WHERE sale_id='$id' AND status=0";
		$this->db->query($query);
		if($this->db->affected_rows()==1){
			return true;
		}
		else{
			return false;
		}
	}

	function get_order_particular($qry){
		$this->db->trans_begin();
		$r=$this->db->query($qry);
		if ($r->num_rows()>0) {
			$this->db->trans_commit();
			return $r->result_array();
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('food_orders'));
		}
	}
	function getPrice($food_id){
		$qry="SELECT price FROM food WHERE food_id=$food_id";
		$this->db->trans_begin();
		$r=$this->db->query($qry);
		if ($r->num_rows()>0) {
			$this->db->trans_commit();
			$r=$r->result();
			return $r[0]->price;
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('food_orders'));
		}
	}

	function updateFoodSale($query){
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
 			redirect(site_url('food_orders'));
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('food_orders'));
		}
	}

	function foodGraph(){
		$saler_id=$this->session->userdata('person_id');
		$today=date("Y-m-d",time());
		$query=$this->db->query("SELECT  name,price FROM foodSales WHERE status=1 AND saler_id=$saler_id AND sale_time='$today'");
		if ($query->num_rows()>0) {
			foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
		}
		else{
			return false;
		}
	}
}