<?php
class Module extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }
	
	function get_all_modules()
	{
		if ($result=$this->db->get('modules')) {
			return $result->result();
		}
		else{
			return false;
		}
		
		
	}
	
	function get_module_desc($module_id)
	{
		$query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
		if ($query->num_rows() ==1)
		{
			$row = $query->row();
			return lang($row->desc_lang_key);
		}
	
		return lang('error_unknown');	
	}
	
	function getmodule($module_id)
	{
		$query_string="SELECT * FROM modules WHERE module_id='$module_id'";
		$result=$this->db->query($query_string);
		return $result;		
	}
	
	function get_allowed_modules($employee_id)
	{
		$this->db->from('modules');
		$this->db->join('permissions','permissions.module_id=modules.module_id');
		$this->db->where("permissions.employee_id",$employee_id);
		$this->db->order_by("name", "asc");
		return $this->db->get();		
	}
}
?>
