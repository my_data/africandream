<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Exp extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getall(){
 		$this->db->trans_begin();
 		if($results=$this->db->get('expenditure')){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter_exp($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM expenditure ORDER BY day ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveData($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}


 	function saveComplementary($query){
 		$this->db->trans_begin();
 		$this->db->query($query);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
 	function search_exp($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}


 	function get_exp_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
		}	
	}
 	function update_exp($query)
	{
		$this->db->trans_begin();
		$this->db->query($query);
 		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function remove_exp($id=0)
	{
		$query="DELETE FROM expenditure WHERE exp_id='$id'";
		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;		
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
 }