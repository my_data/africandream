<?php
class Employee extends CI_Model{
	/*
	Determines if a given person_id is an employee
	*/
	
	/*
	Attempts to login employee and set session. Returns boolean based on outcome.
	*/
	function get_id($query)
	{
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if($result->num_rows()==1){
			$this->db->trans_commit();
			return $result->result();
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}
	function login($username, $password)
	{
		$query = $this->db->get_where('employees', array('username' => $username,'password'=>md5($password)), 1);
		if ($query->num_rows() ==1)
		{
			$row=$query->row();
			$this->session->set_userdata('person_id', $row->employee_id);
			$this->session->set_userdata('username', $row->username);
			return true;
		}
		else{
			return false;
		}
	}
	
	/*
	Logs out a user by destorying all session data and redirect to login
	*/
	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	
	/*
	Determins if a employee is logged in
	*/
	function is_logged_in()
	{
		return $this->session->userdata('person_id')!=false;
	}

	function get_all_staff()
	{
		$this->db->trans_begin();
 		if($return=$this->db->get('employees')){
 			$this->db->trans_commit();
 			return $return->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
	function delete_staff($employee_id)
	{
		$query="DELETE FROM employees WHERE employee_id=$employee_id";
		$this->db->query($query);
		$this->db->trans_begin();
 		if($this->db->affected_rows()>0){
 			$qry="DELETE FROM permissions WHERE employee_id=$employee_id";
 			$this->db->query($qry);
 			if ($this->db->affected_rows()>0) {
 				$this->db->trans_commit();
 				return 1;
 			}	
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}
	
function get_staff_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
		}	
	}

	function update($query)
	{
		$this->db->trans_begin();
		if($this->db->query($query)){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
		}	
	}
	function update_pass($query)
	{
		$this->db->trans_begin();
		if($this->db->query($query)){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
		}	
	}


	function filter($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM employees ORDER BY firstname ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveStaff($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
 	function add_permissions($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function search_employee($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
	
	function get_name($query){
 		$this->db->trans_begin();
 		if($result=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function removePermissions($emp_id){
 		$qry="DELETE FROM permissions WHERE employee_id=$emp_id";
 		$this->db->trans_begin();
 		$this->db->query($qry);
 		if($this->db->affected_rows()>0){
 			$this->db->trans_commit();
 			return true;
 		}
 		else{
 			$this->db->trans_rollback();
 			return false;
 		}
 	}

 	function checkpermision($module_id){
 		$id=$this->session->userdata('person_id');
		$qr="SELECT module_id FROM permissions WHERE employee_id=$id AND module_id=$module_id";
		$r=$this->db->query($qr);
		if ($r->num_rows()==1) {
			return true;
 		}
 		else{
 			return false;
 		}
 	}
}
?>
