<?php
class Report extends CI_Model{
	/*
	Determines if a given person_id is an employee
	*/
	
	/*
	Attempts to login employee and set session. Returns boolean based on outcome.
	*/
	function sales($query)
	{
		$this->db->trans_begin();
		$result=$this->db->query($query);
		if($result->num_rows()>0){
			$this->db->trans_commit();
			return $result;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}
	function summary_drinksales_new($from=0,$to=0){
		$item_sold_query="SELECT DISTINCT name,category,item_id FROM drinkSales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
		$total_cash_query="SELECT SUM(price) AS total_cash FROM drinkSales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
		$this->db->trans_begin();
		$item_sold=$this->db->query($item_sold_query);
		$total_cash=$this->db->query($total_cash_query);
		if ($item_sold->num_rows()>0 AND $total_cash->num_rows()>0) {
			$this->db->trans_commit();
			$this->session->set_userdata('total_cash',$total_cash);
			$this->session->set_userdata('item_sold',$item_sold);
			return true;
		}
		else{
 			$this->db->trans_rollback();
			return false;
		}	
	}

	function get_expenditures(){
		$from=date("Y-m-d",time());
		$to=date("Y-m-d",time());
		if(isset($_SESSION['from'])){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		}
		$q="SELECT *,SUM(expenditure.cost) as totalcost FROM expenditure INNER JOIN employees ON emp_id=employee_id where day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result;
		}
		else{
			return false;
		}
	}

	function get_complementary(){
		$from=date("Y-m-d",time());
		$to=date("Y-m-d",time());
		if(isset($_SESSION['from'])){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		}
		$q="SELECT *,SUM(complementary.quantity) as qty FROM complementary INNER JOIN drinks ON complementary.drink_id=drinks.item_id INNER JOIN employees ON employees.employee_id=complementary.saler_id where day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result;
		}
		else{
			return false;
		}
	}
function get_spoil(){
		$from=date("Y-m-d",time());
		$to=date("Y-m-d",time());
		if(isset($_SESSION['from'])){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		}
		
		$q="SELECT *,SUM(spoil.qty) as total_qty FROM spoil INNER JOIN drinks ON spoil.item_id=drinks.item_id INNER JOIN employees ON employees.employee_id=spoil.saler_id where day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result;
		}
		else{
			return false;
		}
	}
	function get_customers_report(){
		
		$from=date("Y-m-d",time());
		$to=date("Y-m-d",time());
		if(isset($_SESSION['from'])){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		}
		$q="SELECT DISTINCT drinkSales.saler_id,employees.firstname,employees.lastname FROM drinkSales  INNER JOIN employees ON employees.employee_id=drinkSales.saler_id WHERE drinkSales.sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result;
		}
		else{
			return false;
		}
	}

	function drinks_stocking(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT drinks.item_id,drinks.name,category as catgry,drinks.cost,drinks.size as close_stock,drinks.price as saleprice FROM drinks";
		$result=$this->db->query($q);
		if ($result->num_rows()>0) {
			return $result->result();
		}
		else{
			return false;
		}
	}
	function drinks_sum_sold($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(drinkSales.amount) as qty_sold FROM drinkSales WHERE drinkSales.item_id=$item_id AND drinkSales.sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}

	function drinks_sum_received($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(drink_receives.qty) as qty_received FROM drink_receives WHERE drink_id=$item_id AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	function drinks_sum_spoiled($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(spoil.qty) as spoiled FROM spoil WHERE spoil.item_id=$item_id AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}

	function drinks_sum_comp($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(quantity) as comp FROM complementary WHERE drink_id=$item_id AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	function drinks_sum_absorbed($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(qty) as absorbed FROM absorbed_drinks WHERE drink_id=$item_id AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	function drinks_item_cost($item_id){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT drinks.cost as item_cost FROM drinks WHERE item_id=$item_id";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}

	function daily_summary_incomesources(){
		$q="SELECT DISTINCT source FROM income ORDER BY source ASC";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
	function daily_summary_expendsources(){
		$q="SELECT DISTINCT source FROM expenditure ORDER BY source ASC";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
function sum_sources($source){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(fund) as fund FROM income WHERE source='$source' AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			return $result->result();
		}
		else{
			return false;
		}
	}
function sum_expendsources($source){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(cost) as fund FROM expenditure WHERE source='$source' AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return false;
		}
	}
function restaurant_cash_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM foodSales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
	}
function restaurant_credit_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM foodSales WHERE status!=1 AND sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
	}
function accomodation_cash_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM soldroom WHERE status!=2 AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
	}
function accomodation_credit_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM soldroom WHERE status=2 AND day BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
	}
function bar_cash_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM drinkSales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
}
function bar_credit_money(){
		$from=$this->session->userdata('from');
		$to=$this->session->userdata('to');
		$q="SELECT sum(price) as fund FROM drinkSales WHERE status=0 AND sale_time BETWEEN '$from' AND '$to'";
		$result=$this->db->query($q);
		if ($result->num_rows()>0){
			$r=$result->result();
			return $r[0]->fund;
		}
		else{
			return '0.00';
		}
}
}//end of model