<?php
 defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * 
 */
 class Drink extends CI_Model{
 	
 	public function __construct()
 	{
 		
 	}

 	function getall(){
 		$this->db->trans_begin();
 		$query="SELECT * FROM drinks ORDER BY name ASC";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}
 	function filter($no){
 		$no=(int)$no;
 		$this->db->trans_begin();
 		$query="SELECT * FROM drinks ORDER BY name ASC LIMIT $no";
 		if($results=$this->db->query($query)){
 			$this->db->trans_commit();
 			return $results->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}

 	}

 	function saveitem($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function saleitem($query){
 		$this->db->trans_begin();
 		if($this->db->query($query)){
 			$this->db->trans_commit();
 			return 1;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function search($query){
 		$this->db->trans_begin();
 		$result=$this->db->query($query);
 		if($result->num_rows()>0){
 			$this->db->trans_commit();
 			return $result->result();
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}
 	function search_sale($query){
 		$this->db->trans_begin();
 		$result=$this->db->query($query);
 		if($result->num_rows()>0){
 			$this->db->trans_commit();
 			return $result;
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
 	}

 	function get_item_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success->result();
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	function suspenditem_particular($query)
	{
		$this->db->trans_begin();
		if($success = $this->db->query($query)){
			$this->db->trans_commit();
			return $success;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}
	
	function update_original_item_size($query)
	{
		$this->db->trans_begin();
		if($r=$this->db->query($query)){
			if ($this->db->affected_rows()>0) {
				$this->db->trans_commit();
				return $r;
			}
			else{
			$this->db->trans_rollback();
			return false;
		}	
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

 	function update_item($query)
	{
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	function remove_item($id=0)
	{
		$query="DELETE FROM drinks WHERE item_id='$id'";
		$this->db->trans_begin();
 		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;	
 		}
 		else{
 			$this->db->trans_rollback();
 			return FALSE;
 		}
	}

	function get_suspended_sales_drink($saler_id){
		$query="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE status=0 AND saler_id=$saler_id";
		$result=$this->db->query($query);
		if($result->num_rows()>0){
			return $result;
		}
		else{
			return false;
		}
	}
	
	function remove_suspended($query){
		$this->db->trans_begin();
		$this->db->query($query);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			return 1;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	function suspended_particular($query){
		if($result=$this->db->query($query)){
			return $result;
		}
		else{
			return false;
		}
	}


	function delete_suspended_drink($id){
		$this->db->trans_begin();
		$query="DELETE FROM drinkSales WHERE sale_id='$id' AND status=0";
		$this->db->query($query);
		if($this->db->affected_rows()==1){
			$this->db->trans_commit();
			return true;
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	function update_sale($item_id,$new_sale_qty,$customer,$old_sale_qty,$sale_id){
		//Get item stock and then return the previous sale quantity before add new
		$this->db->trans_begin();
		$stock_obj=$this->db->query("SELECT * FROM drinks WHERE item_id=$item_id LIMIT 1");
		$stock_ob=$stock_obj->result();
		$stock=$stock_ob[0]->size;
		$retail_price=$stock_ob[0]->price;
		$new_price=(float)($retail_price*(int)$new_sale_qty);
		$stock=(int)$stock+$old_sale_qty;
		$new_stock=(int)$stock-$new_sale_qty;
		if ($new_stock<0) {
			redirect('counter');
		}
		$query_string1="UPDATE drinkSales SET amount=$new_sale_qty,customer='$customer',price=$new_price WHERE sale_id=$sale_id";
		$this->db->query($query_string1);
		if($this->db->affected_rows()>0){
			$this->db->trans_commit();
			$query_string="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
			$this->db->query($query_string);
			if($this->db->affected_rows()>0){
					
				$this->db->trans_commit();
				return 1;
			}
			else{
				$this->db->trans_rollback();
				return false;
			}
		}
		else{
				$this->db->trans_rollback();
				return false;
			}
	}

	function received_drink($qry){
		$this->db->trans_begin();
		$this->db->query($qry);
		if ($this->db->affected_rows()>0) {
			$this->db->trans_commit();
			redirect(site_url('receiving'));
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('receiving'));
		}
	}

	function get_order_particular($qry){
		$this->db->trans_begin();
		$r=$this->db->query($qry);
		if ($r->num_rows()>0) {
			$this->db->trans_commit();
			return $r->result_array();
		}
		else{
			$this->db->trans_rollback();
			redirect(site_url('drinks_order'));
		}
	}

	function drinksReport(){
		$saler_id=$this->session->userdata('person_id');
		$today=date("Y-m-d",time());
		$query=$this->db->query("SELECT  name,price FROM drinkSales WHERE status=1 AND saler_id=$saler_id AND sale_time='$today'");
		if ($query->num_rows()>0) {
			foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
		}
		else{
			return false;
		}
	}
}