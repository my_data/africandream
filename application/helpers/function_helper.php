<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 function get_sold_item_detail($id,$from,$to){
 	$ci=& get_instance();
 	$query="SELECT SUM(drinkSales.amount) AS qty,SUM(drinkSales.price) AS cash,drinks.name,drinks.category,drinks.cost as capital,drinks.size FROM drinkSales INNER JOIN drinks ON drinkSales.item_id=drinks.item_id WHERE drinkSales.sale_time BETWEEN '$from' AND '$to' AND drinkSales.item_id=$id AND drinkSales.status=1";
	$result=$ci->db->query($query);
	return $result->result();
 }

 function get_item_stock($item_id){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT drinks.size,cost, FROM  drinks  WHERE item_id=$item_id";
	$result=$ci->db->query($query);
	return $result->result();
 }
function get_employee_name($names_obj){
 	$ci=& get_instance();
 	$result=array();
 	foreach ($names_obj->result() as $name) {
 		$query="SELECT * FROM drinkSales INNER JOIN employees ON  employees.employee_id=drinkSales.saler_id WHERE drinkSales.saler_id='$name->saler_id'";
	    $result=$ci->db->query($query);
 	}
 	
	return $result;
 }

function get_total_expediture_cash(){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT SUM(expenditure.cost) AS cost FROM expenditure WHERE day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	$r=$result->result();
	$cost=$r[0]->cost;
	if (empty($cost)) {
		$cost=0;
	}
	return $cost;
}

function get_total_complementary_drinks(){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT SUM(quantity) AS qty FROM complementary WHERE day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	$r=$result->result();
	$sum=$r[0]->qty;
	if (empty($sum)) {
		$sum=0;
	}
	return $sum;
}

function get_total_spoil_drinks(){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT SUM(qty) AS qty FROM spoil WHERE day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	$r=$result->result();
	$sum=$r[0]->qty;
	if (empty($sum)) {
		$sum=0;
	}
	return $sum;
}

function get_total_otherincome_cash(){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT SUM(fund) AS cash FROM income WHERE day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	$r=$result->result();
	return $r[0]->cash;
}

function count_customer_sales($saler){
	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT employees.firstname,employees.lastname,SUM(drinkSales.price) AS total_sales FROM drinkSales inner join employees on saler_id=employee_id WHERE drinkSales.sale_time BETWEEN '$from' AND '$to'AND saler_id=$saler";
	$result=$ci->db->query($query);
	$r=$result->result();
	return $r;
}

function get_total_suspended_sales(){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT sum(price) as credit_cash FROM drinkSales WHERE status=0 AND sale_time BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		$result=$result->result();
		return $result[0]->credit_cash;
	}
	else{
		return 0.00;
	}
 }

 function get_absorbed_conference(){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT * FROM absorbed_drinks WHERE location='conference' AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->num_rows();
 }

  function get_absorbed_restaurant(){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT * FROM absorbed_drinks WHERE location='restaurant' AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->num_rows();
 }

  function get_absorbed_rooms(){
 	$ci=& get_instance();
 	$from=$ci->session->userdata('from');
 	$to=$ci->session->userdata('to');
 	$query="SELECT * FROM absorbed_drinks WHERE location='room' AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	return $result->num_rows();
 }
