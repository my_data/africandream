<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function get_sold_room_particular($id,$from,$to){
 	$id=(int)$id;
 	$ci=& get_instance();
 	$query="SELECT SUM(soldroom.price) AS roomprofit,soldroom.status FROM soldroom WHERE soldroom.day BETWEEN '$from' AND '$to' AND soldroom.room_ID=$id";
	$result=$ci->db->query($query);
	return $result->result();
 }
 function get_room_totalCredit($id,$from,$to){
 	$id=(int)$id;
 	$ci=& get_instance();
 	$query="SELECT SUM(soldroom.price) AS totalCredit FROM soldroom WHERE status=2 AND room_ID=$id AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		# code...
		$r=$result->result();
		return $r[0]->totalCredit;
	}
	else{
		return 0;
	}
			
 }
 function get_room_totalCash($id,$from,$to){
 	$id=(int)$id;
 	$ci=& get_instance();
 	$query="SELECT SUM(soldroom.price) AS totalCash FROM soldroom WHERE status=1 Or status=0 AND room_ID=$id AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		# code...
		$r=$result->result();
		return $r[0]->totalCash;
	}
	else{
		return 0;
	}
			
 }
  function get_room_grossProfit($id,$from,$to){
 	$id=(int)$id;
 	$ci=& get_instance();
 	$query="SELECT SUM(soldroom.price) AS grossProfit FROM soldroom WHERE room_ID=$id AND day BETWEEN '$from' AND '$to'";
	$result=$ci->db->query($query);
	if ($result->num_rows()>0) {
		# code...
		$r=$result->result();
		return $r[0]->grossProfit;
	}
	else{
		return 0;
	}
 }


