<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Food_orders extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		//$this->load->helper('function');
		$this->load->helper('food');
	}
	public function index(){
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/food_orders','title'=>'FOOD ORDERS','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}

	function get_food_orders(){
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/food_orders','title'=>'FOOD ORDERS','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}

}