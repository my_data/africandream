<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	protected $data;
	function __construct(){
		parent::__construct();
		$this->data=array('title'=>'AFRICAN DREAMS');
	}
	
	public function index()
	{

		if (isset($_POST['login'])) {
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			if ($this->form_validation->run()==TRUE) {
				if ($this->authenticate($this->input->post('username'),$this->input->post('password'))) {
					redirect('home');
					}
				else{
					$this->session->set_userdata('fail','Incorrect Username or Password');

					redirect('login');
				}
			}
			elseif($this->form_validation->run()==FALSE){
				
				redirect('login');
			}
			
		}

		else{
			$this->load->view('login',$this->data);
		}
		
		
	}

	private function authenticate($username=0,$password=0){
		$this->load->model('employee');
		if ($this->employee->login($username,$password)) {
			return true;
		}
		else{
			return false;
		}
	}
}
