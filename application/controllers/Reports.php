<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller{
	function __construct()
	{

		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('accomodation_model');	
		$this->load->model('module');
		$this->load->model('report');
		$this->load->model('drink');
		$this->load->model('food');
		$this->load->helper('function');
		$this->load->helper('food');	
		$this->load->library('form_validation');
	}
		
	function index()
	{
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}

	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/dashboard';
	$data['title']="Reports";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
public function drinkSales(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
if (isset($_POST['show_summary_drinks'])) {
	if (empty($_POST['from'])  AND empty($_POST['today'])) {
		redirect('reports/drinkSales/summary');
	}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold="SELECT DISTINCT item_id FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
	if (isset($_POST['today'])) {
		$today=date("Y-m-d",time());
		$this->session->set_userdata('from',$today);
		$this->session->set_userdata('to',$today);
		$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE status=1 AND sale_time='$today'";
		$item_sold="SELECT  DISTINCT item_id FROM drinkSales WHERE sale_time='$today'";
		}
	///selection of employee sales
	if (!empty($_POST['employee_id'])) {
			$employee_id=$_POST['employee_id'];
	$item_sold="SELECT DISTINCT item_id FROM drinkSales WHERE saler_id=$employee_id AND sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE saler_id=$employee_id AND status=1 AND sale_time BETWEEN '$from' AND '$to'";
	}//end
	$total_cash=$this->report->sales($total_cash);
	$sold_item_name=$this->report->sales($item_sold);
	$this->session->set_userdata('sold_item_names',$sold_item_name);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_drinksales';$data['title']="Summary sales";$data['cash']=$total_cash;
	$data['sold_item_name']=$sold_item_name;$this->load->vars($data);
	$this->load->view("template",$data);
	}
	elseif(isset($_POST['show_detailed_drinks'])){
		if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/drinkSales/detailed');
		}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
	$total_cash_qry="SELECT SUM(price) AS value_sum FROM drinkSales WHERE sale_time BETWEEN '$from' AND '$to'";
	if (!empty($_POST['search'])) {
		$search=$_POST['search'];
		$this->checkif_exist($search);
		$$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE status=1 AND sale_time BETWEEN '$from' AND '$to' AND saler_id LIKE '%$search%' OR name LIKE  '%$search%'";
		}
	if (isset($_POST['today'])) {
		$today=date("Y-m-d",time());
		$this->session->set_userdata('from',$today);
		$this->session->set_userdata('to',$today);
	$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE status=1 AND sale_time BETWEEN '$today' AND '$today'";
	$total_cash_qry="SELECT SUM(price) AS value_sum FROM drinkSales WHERE status=1 AND sale_time BETWEEN '$today' AND '$today'";
	if (!empty($_POST['search'])){
		$search=$_POST['search'];
		$this->checkif_exist($search);
		$item_sold_qry="SELECT * FROM drinkSales INNER JOIN employees ON drinkSales.saler_id=employees.employee_id WHERE saler_id LIKE '%$search%' OR name LIKE  '%$search%' AND status=1 AND sale_time ='$today'";
		}
	}
	if(!$sold_items=$this->report->sales($item_sold_qry)){
		$this->check_report_type();
	}
	$total_cash=$this->report->sales($total_cash_qry);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_drinksales';$data['title']="Detailed Report sales";$data['cash']=$total_cash;
	$data['sold_items']=$sold_items;$this->load->vars($data);
	$this->load->view("template",$data);
	}
	else{
		$this->check_report_type();
	}
}

public function getSummaryFoodSales(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
if (isset($_POST['show_summary_foods'])) {
	if (empty($_POST['from'])  AND empty($_POST['today'])) {
		redirect('reports/getSummaryFoodSales');
	}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	/* 
	status 1 means cash sale While 0 means credit sale
	*/
	$item_sold="SELECT DISTINCT item_id FROM foodSales WHERE sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE sale_time BETWEEN '$from' AND '$to'";
	$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE status=0 AND sale_time BETWEEN '$from' AND '$to'";
	if (isset($_POST['today'])) {
		$today=date("Y-m-d",time());
		$this->session->set_userdata('from',$today);
		$this->session->set_userdata('to',$today);
		$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE status=1 AND sale_time='$today'";
		$item_sold="SELECT  DISTINCT item_id FROM foodSales WHERE status=1 AND sale_time='$today'";
		$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE status=0 AND  sale_time='$today'";
		}
	///selection of employee sales
		if (!empty($_POST['employee_id'])) {
			$employee_id=$_POST['employee_id'];
	$item_sold="SELECT DISTINCT item_id FROM foodSales WHERE foodSales.saler_id=$employee_id AND status=1 AND sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE foodSales.saler_id=$employee_id AND status=1 AND sale_time BETWEEN '$from' AND '$to'";
	$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE foodSales.saler_id=$employee_id AND status=0 AND sale_time BETWEEN '$from' AND '$to'";
	}///end
	$total_credit_sale=$this->report->sales($total_credit_sale);
	$total_cash=$this->report->sales($total_cash);
	$sold_items_id=$this->report->sales($item_sold);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='food_report/summary_foodsales';$data['title']="Summary sales";$data['cash']=$total_cash;
	$data['sold_items_id']=$sold_items_id;$data['total_credit_sale']=$total_credit_sale;
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
	else{
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='food_report/summary_foodsales';$data['title']="Summary sales";
		$this->load->view("template",$data);
	}
}

	public function getDetailedFoodSales(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
if (isset($_POST['show_detailed_foods'])) {
	if (empty($_POST['from'])  AND empty($_POST['today'])) {
		redirect('reports/getDetailedFoodSales');
	}
	$from=$this->input->post('from');
	$to=$this->input->post('to');
	$this->session->set_userdata('from',$from);
	$this->session->set_userdata('to',$to);
	$item_sold="SELECT foodSales.amount AS qty,foodSales.price AS cash,food.foodname,food.category,employees.firstname,employees.lastname,foodSales.sale_time,foodSales.customer FROM foodSales inner join food on food.food_id=foodSales.item_id inner join employees on employees.employee_id=foodSales.saler_id WHERE foodSales.sale_time BETWEEN '$from' AND '$to'";
	$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE sale_time BETWEEN '$from' AND '$to'";
	$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE status=0 AND sale_time BETWEEN '$from' AND '$to'";
	if (isset($_POST['today'])) {
		$today=date("Y-m-d",time());
		$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE status=1 AND sale_time='$today'";
	$item_sold="SELECT foodSales.amount AS qty,foodSales.price AS cash,food.foodname,food.category,employees.firstname,employees.lastname,foodSales.sale_time,foodSales.customer FROM foodSales join food on food.food_id=foodSales.item_id  join employees on employees.employee_id=foodSales.saler_id WHERE foodSales.status=1 AND foodSales.sale_time='$today'";

		$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE sale_time='$today' AND status=0";
		}
		///selection of employee sales
		if (!empty($_POST['employee_id'])) {
			$employee_id=$_POST['employee_id'];
		$today=date("Y-m-d",time());
	$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE foodSales.saler_id=$employee_id AND status=1 AND sale_time BETWEEN '$from' AND '$to'";
	$item_sold="SELECT foodSales.amount AS qty,foodSales.price AS cash,food.foodname,food.category,employees.firstname,employees.lastname,foodSales.sale_time,foodSales.customer FROM foodSales join food on food.food_id=foodSales.item_id  join employees on employees.employee_id=foodSales.saler_id WHERE foodSales.saler_id=$employee_id AND foodSales.status=1 AND foodSales.sale_time BETWEEN '$from' AND '$to'";

		$total_credit_sale="SELECT SUM(price) AS credit_sale FROM foodSales WHERE foodSales.saler_id=$employee_id AND status=0 AND sale_time BETWEEN '$from' AND '$to'";
		}///end
	$total_credit_sale=$this->report->sales($total_credit_sale);
	$total_cash=$this->report->sales($total_cash);
	$sold_items_id=$this->report->sales($item_sold);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='food_report/detailed_foodsales';$data['title']="Detailed Food sales";$data['cash']=$total_cash;
	$data['sold_items_id']=$sold_items_id;$data['total_credit_sale']=$total_credit_sale;
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
	else{
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='food_report/detailed_foodsales';$data['title']="Detailed Food sales";
		$this->load->view("template",$data);
	}
}
	function check_report_type(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
	$report_type=$this->uri->segment(2);
	$type=$this->uri->segment(3);
	if ($report_type=='drinkSales') {
	switch ($type) {
	case 'summary':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_drinksales';
	$data['title']="Summary sales";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	break;
	case 'detailed':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_drinksales';
	$data['title']="Detailed Report";	
	$this->load->vars($data);
	$this->load->view("template",$data);	
	break;
	default:
	redirect('reports');
	break;
	}
	}
	if ($report_type=='foodSales') {
	switch ($type) {
	case 'summary':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/summary_foodksales';
	$data['title']="Summary sales";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	break;
	case 'detailed':
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/detailed_foodsales';
	$data['title']="Detailed Report";	
	$this->load->vars($data);
	$this->load->view("template",$data);	
	break;
	default:
	$this->index();
	break;
	}
	}
	}

	function detailed_sale_report($from,$to,$search=0){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$items_sold="SELECT * FROM sales WHERE status=1 AND sale_time BETWEEN '$from' AND '$to'";
		if($sold_items=$this->report->sales($items_sold)){
			return $sold_items;
		}
		else {
			return false;
		}
	}

	function checkif_exist($search){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$query="SELECT * FROM drinkSales WHERE saler_id LIKE '%$search%' OR name LIKE  '%$search%'";
		if ($r=$this->report->sales($query)) {
			if ($r->num_rows()<1) {
				redirect('reports/sales/detailed');
			}
			else{
				return false;
			}
		}
		else{
				return false;
			}
	}

	function get_expenditures(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_expenditures'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_expenditures');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_expenditures()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/expenditure';$data['title']="Expenditures";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports');
		}
		}
	else{
		if($result=$this->report->get_expenditures()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/expenditure';$data['title']="Expenditures";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports');
		}
	}
	}

	function get_complementary(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_complementary'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_complementary');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_complementary()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/complementary';$data['title']="Complementary";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_complementary');
		}
		}
	else{
		if($result=$this->report->get_complementary()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/complementary';$data['title']="Complementaries";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_complementary');
		}
	}
	}

	function get_spoil(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_spoil'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_spoil');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if($result=$this->report->get_spoil()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/spoil';$data['title']="Spoil";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_spoil');
		}
		}
	else{
		if($result=$this->report->get_spoil()){
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/spoil';$data['title']="Spoil";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_spoil');
		}
	}
	}

	function get_customers(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_employees'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_customers');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if (isset($_POST['today'])) {
		$this->session->set_userdata('from',date("Y-m-d",time()));
		$this->session->set_userdata('to',date("Y-m-d",time()));
		}
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Employees Report";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
		
	else{
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Employees Report";
		$this->load->vars($data);
		$this->load->view("template",$data);	
	}
	}

	function suspendedDrinkSales(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_drinkSales'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_customers');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/employees';$data['title']="Credit Sales";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
		
	else{
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/suspended_sales';$data['title']="Spoil";
		$this->load->vars($data);
		$this->load->view("template",$data);	
	}
	}

	function unsuspend(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		$sale_time=date('Y-m-d',time());
		$query="UPDATE drinkSales SET status=1,sale_time='$sale_time' WHERE sale_id='$id'";
		$suspended_sale_qry="SELECT * FROM drinkSales WHERE sale_id='$id'";
		$suspended_sale_r=$this->drink->suspended_particular($suspended_sale_qry);
		$suspended_sale_r=$suspended_sale_r->result();
		$drink_id=(int)$suspended_sale_r[0]->item_id;
		$amount=(int)$suspended_sale_r[0]->amount;
		if ($this->drink->remove_suspended($query)){
		$item_particula="SELECT size FROM drinks WHERE item_id=$drink_id";
		if($item_particula=$this->drink->suspenditem_particular($item_particula)){
			$item_particula=$item_particula->result();
			$stock=(int)$item_particula[0]->size;
			$new_stock=(int)$stock-$amount;
			$query="UPDATE drinks SET size='$new_stock' WHERE item_id=$drink_id";
			if($this->drink->update_original_item_size($query)){
			redirect('reports/suspendedDrinkSales');
			}
			else{
				redirect('reports/suspendedDrinkSales');
			}}
		}}

	function get_receiving(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
			$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/receiving';$data['title']="Receiving";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}

	function foodCreditSales(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if($this->food->get_suspended_sales()==false){
			$this->index();
		}
		$suspended=$this->food->get_suspended_sales();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'food_report/credit_sales','title'=>'Credit Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

	}

	function get_otherincome(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if($this->food->get_suspended_sales()==false){
			$this->index();
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'report/other_income','title'=>'Other Incomes','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);

	}
	function get_soldrooms(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['accomodation_report'])) {
			if (empty($_POST['from'])  AND empty($_POST['today'])) {
			redirect('reports/get_soldrooms');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if (isset($_POST['today'])) {
		$this->session->set_userdata('from',date("Y-m-d",time()));
		$this->session->set_userdata('to',date("Y-m-d",time()));
		}
		$data['data']=$this->accomodation_model->get_roomsales();
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='accomodation/report';$data['title']="Rooms Report";
		$this->load->vars($data);
		$this->load->view("template",$data);
		if($this->accomodation_model->get_roomsales()==false){
			redirect('reports/get_soldrooms');
		}
	}
	else{
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/report','title'=>'Accomodation','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}	

	}
	function soldrooms_summary(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_rooms'])) {
		if (empty($_POST['from'])  AND empty($_POST['today'])) {
			$this->session->set_userdata('fail','Please Select Time Range');
			redirect('reports/soldrooms_summary');
		}
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$this->session->set_userdata('from',$from);
		$this->session->set_userdata('to',$to);
		if (isset($_POST['today'])) {
		$this->session->set_userdata('from',date("Y-m-d",time()));
		$this->session->set_userdata('to',date("Y-m-d",time()));
		}
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='accomodation/soldrooms_summary';$data['title']="Rooms Report";
		$data['all_rooms']=$this->accomodation_model->all_rooms();
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
		
	else{
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['all_rooms']=$this->accomodation_model->all_rooms();
		$data['page']='accomodation/soldrooms_summary';$data['title']="Report Rooms";
		$this->load->vars($data);
		$this->load->view("template",$data);	
	}
	}

		function get_drinks_stock(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_stocking'])) {
			if (empty($_POST['from']) && empty($_POST['to'])) {
			$this->session->set_userdata('fail','Please Select Time Range');
			redirect('reports/get_drinks_stock');
		}
		$this->session->set_userdata('from',$_POST['from']);
		$this->session->set_userdata('to',$_POST['to']);
		if($result=$this->report->drinks_stocking()){
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/drinks_stocking';$data['title']="Drinks Stocking";
		$data['result']=$result;$this->load->vars($data);
		$this->load->view("template",$data);
		}
		else{
			redirect('reports/get_drinks_stock');
		}
		}
	else{
		
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/drinks_stocking';$data['title']="Drinks Stocking";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
	}

	function daily_summary(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['show_summary'])) {
			if (empty($_POST['from']) && empty($_POST['to'])) {
			$this->session->set_userdata('fail','Please Select Time Range');
			redirect('reports/daily_summary');
		}
		$this->session->set_userdata('from',$_POST['from']);
		$this->session->set_userdata('to',$_POST['to']);
		if($income_result=$this->report->daily_summary_incomesources()){
			if ($expend_result=$this->report->daily_summary_expendsources()) {
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/daily_summary';$data['title']="Daily Summary";
		$data['income_result']=$income_result;$data['expend_result']=$expend_result;
		$this->load->vars($data);
		$this->load->view("template",$data);
		}}
		else{
			redirect('reports/daily_summary');
		}
		}
	else{
		
		$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data['page']='report/daily_summary';$data['title']="Daily Summary";
		$this->load->vars($data);
		$this->load->view("template",$data);
	}
	}
	
}//end of controller
