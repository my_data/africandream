<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AllocateRoom extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('accomodation_model');
	}

	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$result=$this->accomodation_model->getRooms();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
	}

	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_room'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('roomname','Room Name','required');
			$this->form_validation->set_rules('category','Room category','required');
			$this->form_validation->set_rules('fee','Fee','required');
			if ($this->form_validation->run()==TRUE) {
				$roomname=$this->input->post('roomname');
				$category=$this->input->post('category');
				$fee=$this->input->post('fee');
				$query="INSERT INTO rooms VALUES('','$roomname','$category','$fee','EMPTY')";
				if($this->accomodation_model->saveRoom($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('allocateRoom');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$result=$this->accomodation_model->getRooms();
			$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
			$this->load->view('template',$data);
			}
		}
		else {
			$result=$this->accomodation_model->getRooms();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_room'])) {
			$search=$this->input->post('keyword');
		}
		$query="SELECT * FROM rooms WHERE name LIKE '%$search%' OR category LIKE '%$search%'";
		$result=$this->accomodation_model->search($query);
		if (!$result) {
			redirect('accomodation');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
	}

	public function filter(){
		$no=$this->uri->segment(3);
		$result=$this->accomodation_model->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);


	}
	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('roomname','name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('fee','fee','required');
			$this->form_validation->set_rules('roomid','id','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('roomname');
				$category=$this->input->post('category');
				$fee=$this->input->post('fee');
				$id=$this->input->post('roomid');
			$query="UPDATE rooms SET name='$name',category='$category',fee='$fee' WHERE room_id='$id'";
				if($this->accomodation_model->update($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect(site_url('accomodation'));
				}
			}
			else{
				redirect(site_url('accomodation'));
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM rooms WHERE room_id='$id' LIMIT 1";
		$result=$this->accomodation_model->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/update','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

		public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->accomodation_model->delete_room($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('accomodation');

		}

	}

}