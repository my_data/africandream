<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
	}

	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$staff=$this->employee->get_all_staff();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'staff/dashboard','title'=>'STAFF','allowed_modules'=>$allowed_modules,'allstaff'=>$staff);
		$this->load->view('template',$data);
	}

	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_staff'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('firstname','First Name','required');
			$this->form_validation->set_rules('lastname','Last Name','required');
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('total_modules','Total Modules','required');
			if ($this->form_validation->run()==TRUE) {
				$firstname=$this->input->post('firstname');
				$surname=$this->input->post('lastname');
				$username=$this->input->post('username');
				$password=md5($this->input->post('password'));
				$total_modules=$this->input->post('total_modules');
			$query="INSERT INTO employees VALUES('$firstname','$surname','$username','$password','')";
				if($this->employee->saveStaff($query)){
			$idquery="SELECT * FROM employees WHERE password='$password' AND username='$username' AND firstname='$firstname' AND lastname='$surname'";
					if (!$this->employee->get_id($idquery)) {
						$this->session->set_userdata('success','User Successfull Added');
						redirect('staff');
					}
					$employee_id_res=$this->employee->get_id($idquery);
					$employee_id=$employee_id_res[0]->employee_id;
					while ($total_modules>0) {
						$module_id=$this->input->post('module'.$total_modules);
						if ($module_id !='') {
							
         	$query1="INSERT INTO permissions VALUES('','$module_id','$employee_id')";
         	$this->employee->add_permissions($query1);
						}
						$total_modules--;
					}
						redirect('staff');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				redirect('staff');
			}
		}
		else {
		redirect('staff');
		}
	}

	public function search(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$search=$this->uri->segment(3);
		if (isset($_POST['search_staff'])) {
			$search=$this->input->post('keyword');
			if (empty($search)) {
				redirect('staff');
			}
		}
		$query="SELECT * FROM employees WHERE firstname LIKE '$search%' OR lastname LIKE '$search%' OR username LIKE '$search%'";
		$staff=$this->employee->search_employee($query);
		if (!$staff) {
			redirect('staff');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'staff/dashboard','title'=>'STAFF','allowed_modules'=>$allowed_modules,'allstaff'=>$staff);
		$this->load->view('template',$data);
	}

	public function filter(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$no=$this->uri->segment(3);
		$staff=$this->employee->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'staff/dashboard','title'=>'STAFF','allowed_modules'=>$allowed_modules,'allstaff'=>$staff);
		$this->load->view('template',$data);


	}

	public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->employee->delete_staff($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('staff');

		}

	}

	
	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_staff'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('firstname','First Name','required');
			$this->form_validation->set_rules('lastname','Last Name','required');
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('employee_id','id','required');
			$this->form_validation->set_rules('total_modules','Total Modules','required');
			if ($this->form_validation->run()==TRUE) {
				$firstname=$this->input->post('firstname');
				$lastname=$this->input->post('lastname');
				$username=$this->input->post('username');
				$password=$this->input->post('password');
				$employee_id=(int)$this->input->post('employee_id');
				$total_modules=$this->input->post('total_modules');
		$query="UPDATE employees SET firstname='$firstname',lastname='$lastname',username='$username' WHERE employee_id=$employee_id";
		//function to delete all permisions b4 set new
			
				if($this->employee->update($query)){
					if (!empty($password)) {
					$password=md5($password);
					$query="UPDATE employees SET password='$password' WHERE employee_id=$employee_id";
					$this->employee->update($query);
					}
				$this->employee->removePermissions($employee_id);
					while ($total_modules>0) {
				$module_id=(int)$this->input->post('module'.$total_modules);
				if (!empty($module_id)) {				
         		$query1="INSERT INTO permissions VALUES('',$module_id,$employee_id)";
         		$this->employee->add_permissions($query1);
						}
						$total_modules--;	
					}
				}
				redirect('staff');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('staff');
			}
			
		}
	else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM employees WHERE employee_id='$id' LIMIT 1";
		$staff=$this->employee->get_staff_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'staff/update','title'=>'STAFF','allowed_modules'=>$allowed_modules,'staff'=>$staff);
		$this->load->view('template',$data);
		}
	}

	function update_password(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_password'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('old_password','First Name','required');
			$this->form_validation->set_rules('new_password','Last Name','required');
			if ($this->form_validation->run()==TRUE) {
				$oldpass=md5($this->input->post('old_password'));
				$newpass=md5($this->input->post('new_password'));
				$id=$this->session->userdata('person_id');
				$query="UPDATE employees SET password='$newpass' WHERE password='$oldpass' AND employee_id=$id";
				if($this->employee->update_pass($query)){
					$this->session->set_userdata('success','You have Successfull changed your Password, Please Login with New Password');
					$this->session->unset_userdata('person_id');
					redirect('login');
				}
			}
			else{
				redirect(base_url());
			}
		}
		else{
			$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$data=array('page'=>'staff/change_password','title'=>'STAFF','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
		}
	}
}