<?php
//require_once ("secure_area.php");
$data;
class Home extends CI_Controller{
	function __construct()
	{

		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->model('food');	
		$this->load->model('accomodation_model');	
	}
		
	function index()
	{
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}

	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='home';
	$data['Report']=$this->drink->drinksReport();
	$data['food_graph']=$this->food->foodGraph();
	$data['room_graph']=$this->accomodation_model->roomGraph();
	$data['title']="Dashboard";	
	$this->load->vars($data);
	$this->load->view("template",$data);
	}
	public function get_module(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$module_id=$this->uri->segment(3);
		$module=$this->module->getmodule($module_id);
		$module_name=$module->result();
		$modulename=$module_name[0]->name;
		redirect($modulename);
	}
	function logout()
	{

		$this->employee->logout();
	}
}
?>