<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('food');
	}
	
	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$result=$this->food->getall();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/dashboard','title'=>'Restaurant','allowed_modules'=>$allowed_modules,'food_result'=>$result);
		$this->load->view('template',$data);
	}
	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_food'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('foodname','Name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('price','price','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('foodname');
				$category=$this->input->post('category');
				$price=$this->input->post('price');
				$query="INSERT INTO food VALUES('','$name','$category',$price)";
				if($this->food->saveFood($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('restaurant');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('fail','Fill fields');
				redirect('restaurant');
			}
		}
		else {
			redirect('restaurant');
		}
	}
	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_food'])) {
			$search=$this->input->post('keyword');
		}
		$query="SELECT * FROM food WHERE foodname LIKE '$search%' OR category LIKE '$search%'";
		$result=$this->food->search($query);
		if (!$result) {
			redirect('Restaurant');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/dashboard','title'=>'Foods','allowed_modules'=>$allowed_modules,'food_result'=>$result);
		$this->load->view('template',$data);

	}

	public function filter(){
		$no=$this->uri->segment(3);
		$result=$this->food->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/dashboard','title'=>'Foods','allowed_modules'=>$allowed_modules,'food_result'=>$result);
		$this->load->view('template',$data);
	}

	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_food'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('foodname','name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('food_id','id','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('foodname');
				$category=$this->input->post('category');
				$price=$this->input->post('price');
				$id=$this->input->post('food_id');
			$query="UPDATE food SET foodname='$name',category='$category',price='$price' WHERE food_id='$id'";
				if($this->food->update_food($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect(site_url('restaurant'));
				}
			}
			else{
				redirect(site_url('restaurant'));
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM food WHERE food_id='$id' LIMIT 1";
		$result=$this->food->get_food_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/update','title'=>'Food','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->food->remove_food($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('restaurant');

		}
		else{
			$this->session->set_userdata('fail','Operation fail');
			redirect('restaurant');
		}	
	}

}