<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drinks_order extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->helper('function');
		$this->load->library('cart');
	}
	public function index(){
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/drinks_orders','title'=>'DRINKS ORDERS','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}

	function get_drink_orders(){
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'report/drink_orders','title'=>'DRINKS ORDERS','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}

}