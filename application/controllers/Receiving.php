<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receiving extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
	}

	function index(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}		
		$result=null;
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/receive','title'=>'Receiving','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);

	}
	function add(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['receive_item'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name','name','required');
			$this->form_validation->set_rules('quantity','amount','required');
			$this->form_validation->set_rules('item_id','id','required');
			$this->form_validation->set_rules('cost','cost','required');
			$this->form_validation->set_rules('old_stock','stock','required');
			$this->form_validation->set_rules('old_cost','old_cost','required');
			if ($this->form_validation->run()==TRUE) {
				$cost=(double)$this->input->post('cost');
				$old_cost=$this->input->post('old_cost');
				$old_stock=$this->input->post('old_stock');
				$quantity=(int)$this->input->post('quantity');
				$id=(int)$this->input->post('item_id');
				$new_stock=(int)$old_stock+$quantity;
				$day=date("Y-m-d",time());
				$emp=$this->session->userdata('person_id');
				if ($new_stock<0) {
					$this->session->set_userdata('fail','Low Stock');
					redirect('receiving');
				}
				$new_cost=(double)$old_cost+$cost;
			$query="UPDATE drinks SET size='$new_stock',cost='$new_cost' WHERE item_id=$id";
				if($this->drink->update_item($query)){
					$query_str="INSERT INTO drink_receives VALUES('',$id,$quantity,'$day',$emp,$cost)";
					$this->drink->received_drink($query_str);
				}
			}
			else{
				redirect(site_url('receiving'));
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM drinks WHERE item_id='$id' LIMIT 1";
		$result=$this->drink->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/add_receive','title'=>'Receiving','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_drink'])) {
			$search=$this->input->post('keyword');
		}
		if (empty($search)) {
			redirect('receiving');
		}
		$query="SELECT * FROM drinks WHERE name LIKE '%$search%' OR category LIKE '%$search%'";
		$result=$this->drink->search($query);
		if (!$result) {
			redirect('receiving');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/receive','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);

	}
}