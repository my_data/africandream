<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Counter extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->library('cart');
		$this->load->model('report');
		$this->load->helper('function');
	}

	function index(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}		
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);

	}
	function search(){
		if(!$this->employee->is_logged_in()){
			
			redirect('login');
		}
		if (isset($_POST['search'])) {
		$search=$this->input->post('keyword');
		if (empty($search)) {
			$this->session->set_userdata('fail','Empty Search');
			redirect('counter');
		}
		$query="SELECT * FROM drinks WHERE name LIKE '%$search%' OR category LIKE '%$search%'";
		$result=$this->drink->search($query);
		if (!$result) {
			redirect('counter');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
		$this->load->view('template',$data);
		}
		elseif (!empty($this->uri->segment(3))) {
		$search=$this->uri->segment(3);
		$query="SELECT * FROM drinks WHERE name LIKE '%$search%'";
		$result=$this->drink->search($query);
		if (!$result) {
			redirect('counter');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/dashboard','title'=>'Sales','allowed_modules'=>$allowed_modules,'result'=>$result);
		$this->load->view('template',$data);
		}
		else{
			redirect('counter');
		}
	}

	function add_to_cart(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['add'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('itemname','Item Name','required');
			$this->form_validation->set_rules('category','drink category','required');
			$this->form_validation->set_rules('stock','drink Amount','required');
			$this->form_validation->set_rules('qty','quantity','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('item_id','ID','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('itemname');
				$category=$this->input->post('category');
				$qty=(int)$this->input->post('qty');
				$stock=(int)$this->input->post('stock');
				$price=(int)$this->input->post('price');
				$item_id=$this->input->post('item_id');
				$saler=$this->session->userdata('person_id');
				$new_stock=$stock-$qty;
				if ($new_stock<0){
					$this->session->set_userdata('fail','No stock Availabe');
					redirect('counter');
				}

		//check if item exist first
				foreach ($this->cart->contents() as $drink) {
					if ($drink['id']==$item_id) {
				$item=array('rowid'=>$drink['rowid'],'qty'=>$qty,'price'=>$price,'category'=>$category,'stock'=>$stock);
						if($this->cart->update($item)){

							redirect('counter');
							
						}
					}
				}
		//add item to cart
		$drink=array('id'=>$item_id,'name'=>$name,'qty'=>$qty,'price'=>$price,'category'=>$category,'stock'=>$stock);
				if($this->cart->insert($drink)){
					$this->session->set_userdata('success','Drink Added Successfull');
				redirect('counter');
					
				}
				
			}
			if ($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('fail','Fill all required');
				redirect('counter');	
			}
		}
		else{
			redirect('counter');	
		}
		
	}

function remove_cart_item(){
	$rowid=$this->uri->segment(3);
	
	if($this->cart->remove($rowid)){
		$this->session->set_userdata('success','Drink Removed Successfull');
		redirect('counter');
	}
}

function sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['sale'])) {
			if ($this->cart->contents()){
				$sales=0;
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$stock=$item['stock'];
					$item_id=(int)$item['id'];$qty=(int)$item['qty'];
					$cgry=$item['category'];
					$saler=$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$new_stock=($stock-$qty);
					if ($new_stock<0) {
						$this->session->set_userdata('success','No stock Availabe');
						redirect('counter');
					}
					$new_stock=(int)$new_stock;
					$price=(int)$price;
					$customer=$_POST['customer'];
					$time=date("Y-m-d",time());
					$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,'$saler',$price,1,'$customer')";
			if($this->drink->saleitem($query)){
				$query="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
					if($this->drink->update_item($query)){
					$sales++;}
				}
				else{
					redirect('counter');
				}

			}
			
			if ($sales>0) {
				if($_POST['receipt']==1){
				
				redirect('counter/receipt');
				}
				$this->cart->destroy();
				$this->session->set_userdata('success','Drink Sold Successfull');
				redirect('counter');		
				}
			else{
				redirect('counter');
			}
					
			//check if requesting for receipt and then print receipt		
		}
		else{
				redirect('counter');
			}
		}
		elseif (isset($_POST['order'])) {
			if ($this->cart->contents()) {
				$sales=0;
				foreach ($this->cart->contents() as $item) {
					$item_id=(int)$item['id'];$qty=(int)$item['qty'];
					$saler_id=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$price=(double)$price;
					$customer=$_POST['customer'];
					$time=date("Y-m-d",time());
					$query="INSERT INTO drink_order VALUES('',$item_id,$qty,$price,$saler_id,'$customer','$time',0)";
					if($this->drink->saleitem($query)){
						$sales++;
					}
			}
				if($sales>0){
					$this->cart->destroy();
					$this->session->set_userdata('success','Order Placed Successfull');
				redirect('counter');		
					
				}
				else{
					redirect('counter');
				}	
			}
			}
		elseif (isset($_POST['suspend'])) {
			if ($this->cart->contents()) {
				$sales=0;
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$stock=$item['stock'];$item_id=$item['id'];$qty=$item['qty'];
					$cgry=$item['category'];
					$saler=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$new_stock=($stock-$qty);
					$new_stock=(int)$new_stock;
					$price=(int)$price;
					$customer=$_POST['customer'];
					$time=date("Y-m-d",time());
$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,0,'$customer')";
			if($this->drink->saleitem($query)){
				$query="UPDATE drinks SET size=$new_stock WHERE item_id='$item_id'";
					if($this->drink->update_item($query)){
						$sales++;
					}
				}
			}
			if($sales>0){
					$this->cart->destroy();
					$this->session->set_userdata('success','Drink Sold Successfull');
				redirect('counter');		
				}
				else{
					$this->session->set_userdata('fail','Drink Not Sold');
				redirect('counter');		
				}	
			}
			}
			else{
			redirect('counter');
			}
		
	}

	function sale_mode(){
		$mode=$this->uri->segment(3);
		switch ($mode) {
			case 'drink':
				redirect('counter');
				break;
			case 'food':
				redirect('Sale_food');
				break;
			default:
				redirect('counter');
				break;
		}
	}

	function suspended(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$saler=(int)$this->session->userdata('person_id');
		$suspended=$this->drink->get_suspended_sales_drink($saler);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/suspended_sales','title'=>'Suspended Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

	}

	function search_suspended_sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['search_sale'])) {
			$search=$this->input->post('keyword');
			if (empty($search)) {
				redirect('counter/suspended');
			}
		$query="SELECT * FROM drinkSales WHERE name LIKE '%$search%' OR saler_id LIKE '%$search%' AND status=0";
		$suspended=$this->drink->search_sale($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'counter/suspended_sales','title'=>'Suspended Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

		}
		else{
			redirect('counter/suspended');
		}
	}


	function unsuspend(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		$sale_time=date('Y-m-d',time());
		$query="UPDATE drinkSales SET status=1,sale_time='$sale_time' WHERE sale_id='$id'";
		$suspended_sale_qry="SELECT * FROM drinkSales WHERE sale_id='$id'";
		$suspended_sale_r=$this->drink->suspended_particular($suspended_sale_qry);
		$suspended_sale_r=$suspended_sale_r->result();
		$drink_id=(int)$suspended_sale_r[0]->item_id;
		$amount=(int)$suspended_sale_r[0]->amount;
		if ($this->drink->remove_suspended($query)){
		$item_particula="SELECT size FROM drinks WHERE item_id=$drink_id";
		if($item_particula=$this->drink->suspenditem_particular($item_particula)){
			$item_particula=$item_particula->result();
			$stock=(int)$item_particula[0]->size;
			$new_stock=(int)$stock-$amount;
			$query="UPDATE drinks SET size='$new_stock' WHERE item_id=$drink_id";
			if($this->drink->update_original_item_size($query)){
				$this->session->set_userdata('success','Operation Successfull');
			redirect('counter/suspended');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('counter/suspended');
			}}
		}}

		function remove_suspended_sale(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
			$id=$this->uri->segment(3);
			if ($this->drink->delete_suspended_drink($id)) {
				$this->session->set_userdata('success','Operation Successfull');
				redirect('counter/suspended');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
			redirect('counter/suspended');
			}
		}

		function todaySales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['todaySales'])) {
		$today=date("Y-m-d",time());
		$this->session->set_userdata('from',$today);
		$this->session->set_userdata('to',$today);
		$saler=(int)$this->session->userdata('person_id');
		$total_cash="SELECT SUM(price) AS value_sum FROM drinkSales WHERE status=1 AND sale_time='$today' AND saler_id='$saler'";
		$item_sold="SELECT  DISTINCT item_id FROM drinkSales WHERE status=1 AND sale_time='$today' AND saler_id='$saler'";
		$total_cash=$this->report->sales($total_cash);
		$sold_item_name=$this->report->sales($item_sold);
	$this->session->set_userdata('sold_item_names',$sold_item_name);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='counter/todaySales';$data['title']="Summary sales";$data['cash']=$total_cash;
	$data['sold_item_name']=$sold_item_name;$this->load->vars($data);
	$this->load->view("template",$data);
		}
		else{
			redirect('counter');
		}
		}
		function allSales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['allSales'])) {
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='report/all_drinks_sales';$data['title']="ALL SALES";
	$this->load->vars($data);
	$this->load->view("template",$data);
		}
		else{
			redirect('drinks_order');
		}
		}

	function updateSale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['updateSale'])) {
			$this->form_validation->set_rules('drink_id','id','required');
			$this->form_validation->set_rules('sale_id','id','required');
			$this->form_validation->set_rules('new_sale_qty','qty','required');
			$this->form_validation->set_rules('customer','customer','required');
			$this->form_validation->set_rules('old_sale_qty','qty','required');
				if ($this->form_validation->run()==TRUE) {
				$item_id=(int)$this->input->post('drink_id');
				$sale_id=(int)$this->input->post('sale_id');
				$old_sale_qty=(int)$this->input->post('old_sale_qty');
				$new_sale_qty=(int)$this->input->post('new_sale_qty');
				$customer=$this->input->post('customer');
				if ($this->drink->update_sale($item_id,$new_sale_qty,$customer,$old_sale_qty,$sale_id)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('drinks_order');
				}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('drinks_order');
				}
			}
			else{
				redirect('drinks_order');
			}
		}
		else{
			redirect('drinks_order');
		}
	}

	function saleOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="SELECT * FROM drink_order inner join drinks on drink_order.drink_id=drinks.item_id WHERE drink_order.order_id=$order_id";
		if ($item=$this->drink->get_order_particular($query)) {
			$name=$item[0]['name'];
			$stock=(int)$item[0]['size'];
					$item_id=(int)$item[0]['item_id'];
					$qty=(int)$item[0]['qty'];
					$cgry=$item[0]['category'];
					$saler=(int)$item[0]['saler_id'];
					$price=(double)$item[0]['price'];
					$new_stock=(int)$stock-$qty;
					$customer=$item[0]['customer'];
					$time=date("Y-m-d",time());
				//preventing stock to be negative
					if ($new_stock<0){
						$this->session->set_userdata('fail','Low stock');
						redirect('drinks_order');
					}
		$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,1,'$customer')";
			if($this->drink->saleitem($query)){
				$query1="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
					if($this->drink->update_item($query1)){
						$query2="UPDATE drink_order SET status=1 WHERE order_id=$order_id";
					if($this->drink->update_item($query2)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('drinks_order');	
					}
					else{
						$this->session->set_userdata('fail','Operation fail');
						redirect('drinks_order');
					}
					}
					else{
						redirect('drinks_order');
					}
					
				}
		}
		}
		function creditSaleOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="SELECT * FROM drink_order inner join drinks on drink_order.drink_id=drinks.item_id WHERE drink_order.order_id=$order_id";
		if ($item=$this->drink->get_order_particular($query)) {
			$name=$item[0]['name'];
			$stock=(int)$item[0]['size'];
					$item_id=(int)$item[0]['item_id'];
					$qty=(int)$item[0]['qty'];
					$cgry=$item[0]['category'];
					$saler=(int)$item[0]['saler_id'];
					$price=(double)$item[0]['price'];
					$new_stock=(int)$stock-$qty;
					$customer=$item[0]['customer'];
					$time=date("Y-m-d",time());
				//preventing stock to be negative
					if ($new_stock<0){
						$this->session->set_userdata('fail','Low stock');
						redirect('drinks_order');
					}
		$query="INSERT INTO drinkSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,0,'$customer')";
			if($this->drink->saleitem($query)){
				$query1="UPDATE drinks SET size=$new_stock WHERE item_id=$item_id";
					if($this->drink->update_item($query1)){
						$query2="UPDATE drink_order SET status=1 WHERE order_id=$order_id";
					if($this->drink->update_item($query2)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('drinks_order');	
					}
					else{
						$this->session->set_userdata('fail','Operation fail');
						redirect('drinks_order');
					}
					}
					else{
						redirect('drinks_order');
					}
					
				}
		}

	}

	 function receipt(){
	 		$this->load->library('cart');
	 		$this->load->library('pdf');
			$this->load->view('receipt');
			//$this->cart->destroy();
	 }
	 function removeOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			redirect('drinks_order');
		}
		$query="DELETE FROM drink_order WHERE order_id=$order_id";
			if ($this->drink->update_item($query)) {
				$this->session->set_userdata('success','Order Canceled');
				redirect('drinks_order');
			}
			else{
				$this->session->set_userdata('fail','Operation failed Please try Again');
				redirect('drinks_order');
			}			
						
	}
}//end of controller