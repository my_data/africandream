<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accomodation extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('accomodation_model');
	}

	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$result=$this->accomodation_model->getRooms();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
	}

	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_room'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('roomname','Room Name','required');
			$this->form_validation->set_rules('category','Room category','required');
			$this->form_validation->set_rules('fee','Fee','required');
			if ($this->form_validation->run()==TRUE) {
				$roomname=$this->input->post('roomname');
				$category=$this->input->post('category');
				$fee=$this->input->post('fee');
				$query="INSERT INTO rooms VALUES('','$roomname','$category','$fee','EMPTY')";
				if($this->accomodation_model->saveRoom($query)){
					$this->session->set_userdata('success','Room Registration Successfull');
					redirect('accomodation');
				}
				else{
					$this->session->set_userdata('fail','Room Registration Fail');
					redirect('accomodation');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$result=$this->accomodation_model->getRooms();
			$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
			$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
			$this->load->view('template',$data);
			}
		}
		else {
			$result=$this->accomodation_model->getRooms();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function saleRoom(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}

		if (isset($_POST['saleRoom'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('room_id','Room ID','required');
			$this->form_validation->set_rules('customer','customer','required');
			if ($this->form_validation->run()==TRUE) {
				$roomid=(int)$this->input->post('room_id');
				$customer=$this->input->post('customer');
				$saler=(int)$this->session->userdata('person_id');
				$room_info=$this->accomodation_model->getRoomInfo($roomid);
				$price_rm=$room_info[0]->fee;
				$status=$room_info[0]->status;
				if (isset($_POST['discount'])) {
				if (!empty($_POST['discount'])) {
					$price_rm=(double)$price_rm-$_POST['discount'];
					if ($price_rm<1) {
						$this->session->set_userdata('fail','Oops! Check price Please');
						redirect('accomodation');
					}
				}}
				/*status 1 means room is full while 0 means room is released ans 2 means room is released but its credit*/
				$time=date("Y-m-d",time());
				$query="INSERT INTO soldroom VALUES('',$roomid,$saler,'$customer',$price_rm,'$time',1,'')";
				if($this->accomodation_model->saleroom($query)){
					$query="UPDATE rooms SET status='FULL' WHERE room_id=$roomid AND status='EMPTY'";
				if($this->accomodation_model->update($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('accomodation');
				}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('accomodation');
				}
				}
			}
			else{
			redirect('accomodation');
			}
		}
		elseif (isset($_POST['creditSale'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('room_id','Room Name','required');
			$this->form_validation->set_rules('customer','Room category','required');
			if ($this->form_validation->run()==TRUE) {
				$roomid=(int)$this->input->post('room_id');
				$customer=$this->input->post('customer');
				$room_info=$this->accomodation_model->getRoomInfo($roomid);
				$price_rm=$room_info[0]->fee;
				$status=$room_info[0]->status;
				$saler=(int)$this->session->userdata('person_id');
				if (!empty($_POST['discount_price'])) {
					$price_rm=(double)$_POST['discount_price'];
				}
				$time=date("Y-m-d",time());
				//status 2 means credit sale
				$query="INSERT INTO soldroom VALUES('',$roomid,$saler,'$customer',$price_rm,'$time',2,'')";
				if($this->accomodation_model->saleroom($query)){
					$query="UPDATE rooms SET status='FULL' WHERE room_id=$roomid AND status='EMPTY'";
				if($this->accomodation_model->update($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect('accomodation');
				}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('accomodation');
				}
				}
			}
			else{
			redirect('accomodation');
			}
		}
		else{redirect('accomodation');}
	}
function booking(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}

		if (isset($_POST['book'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('room_id','Room ID','required');
			$this->form_validation->set_rules('customer','customer','required');
			$this->form_validation->set_rules('day','customer','required');
			if ($this->form_validation->run()==TRUE) {
				$booked_day=date('Y-m-d',time());
				$roomid=(int)$this->input->post('room_id');
				$customer=$this->input->post('customer');
				$day=$this->input->post('day');
				$saler=(int)$this->session->userdata('person_id');
				$room_info=$this->accomodation_model->getRoomInfo($roomid);
				$price_rm=$room_info[0]->fee;
				/*status 1 means room is full while 0 means room is released ans 2 means room is released but its credit*/
				$time=date("Y-m-d",time());
				$query="INSERT INTO room_booking VALUES('',$roomid,$saler,'$customer',$price_rm,'$day','$booked_day')";
				if($this->accomodation_model->saleroom($query)){
					$this->session->set_userdata('success','Room Booked Successfull');
					redirect('accomodation');
				}
				else{
						$this->session->set_userdata('fail','Sorry!! Room Not Booked');
						redirect('accomodation');
					}
				}
				else{
						$this->session->set_userdata('fail','Sorry!! Fill Required');
						redirect('accomodation');
					}
			}
			else{
			redirect('accomodation');
			}
		}
	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_room'])) {
			$search=$this->input->post('keyword');
			if (empty($search)) {
			$this->session->set_userdata('fail','Sorry! Enter keyword To search');
			redirect('accomodation');
			}
		}
		$query="SELECT * FROM rooms WHERE name LIKE '%$search%' OR category LIKE '%$search%' order by name asc";
		$result=$this->accomodation_model->search($query);
		if (!$result) {
			$this->session->set_userdata('fail','Sorry! No Record Found');
			redirect('accomodation');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);
	}

	public function filter(){
		$no=$this->uri->segment(3);
		$result=$this->accomodation_model->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/dashboard','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'room_result'=>$result);
		$this->load->view('template',$data);


	}
	function my_sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['my_sale'])) {
				$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/my_sale','title'=>'My Sales','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
		}
		else{
			redirect('accomodation');
		}

	}
	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('roomname','name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('fee','fee','required');
			$this->form_validation->set_rules('roomid','id','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('roomname');
				$category=$this->input->post('category');
				$fee=$this->input->post('fee');
				$id=$this->input->post('roomid');
			$query="UPDATE rooms SET name='$name',category='$category',fee='$fee' WHERE room_id='$id'";
				if($this->accomodation_model->update($query)){
					$this->session->set_userdata('success','Room Updated Successfull');
					redirect(site_url('accomodation'));
				}
				else{
				$this->session->set_userdata('fail','Ooops Fail! Please Try Again');
				redirect(site_url('accomodation'));
			}
			}
			else{
				redirect(site_url('accomodation'));
			}
		}
		else{
		$id=(int)$this->uri->segment(3);
		$query="SELECT * FROM rooms WHERE room_id=$id LIMIT 1";
		$result=$this->accomodation_model->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/update','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

		public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->accomodation_model->delete_room($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('accomodation');

		}
		else{
				$this->session->set_userdata('fail','Ooops Fail! Please Try Again');
				redirect(site_url('accomodation'));
			}
	}

	function viewSoldRooms(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/soldrooms','title'=>'Sold Rooms','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}
	function deleteBook(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=(int)$this->uri->segment(3);
		$query="DELETE FROM room_booking WHERE tras_id=$id";
		if($this->accomodation_model->delete_book($query)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('accomodation/showBooking');
		}
		else{
			$this->session->set_userdata('fail','Operation fail');
			redirect('accomodation/showBooking');
		}
	}

function showBooking(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/bookings','title'=>'Bookings','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}
	function releaseRoom(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
			$id=(int)$this->uri->segment(3);
			$sale_id=(int)$this->uri->segment(4);
			$sale_type=$this->uri->segment(5);
			$query3="UPDATE soldroom SET checkout_time='' WHERE room_id=$id AND roomsale_id=$sale_id";
		//check if room is credit sale and then record the time of checked out
			if ($sale_type=='credit_sale') {
			$checkout_time=date('Y-m-d',time());	
			$query3="UPDATE soldroom SET checkout_time='$checkout_time' WHERE room_id=$id AND roomsale_id=$sale_id";
			}
		//Check if room is empty and them alert to user
			$room_status_qry="SELECT status FROM rooms WHERE status='EMPTY' AND room_id=$id";
		if ($this->accomodation_model->roomStatus($room_status_qry)) {
			$this->session->set_userdata('success','This Room Is ALready FREE');
			redirect('accomodation/viewSoldRooms');
		}
		//continue release the room
			$query1="UPDATE rooms SET status='EMPTY' WHERE room_id=$id AND status='FULL'";
			$query2="UPDATE soldroom SET status=0 WHERE room_id=$id AND roomsale_id=$sale_id";
			if ($sale_type=="credit_sale") {
			$query2="UPDATE soldroom SET status=2 WHERE room_id=$id AND roomsale_id=$sale_id";
			}
				if($this->accomodation_model->update($query1)){
					$this->accomodation_model->update($query2);
					$this->accomodation_model->update($query3);
					$this->session->set_userdata('success','Room Released Successfull');
					redirect('accomodation/viewSoldRooms');
				}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('accomodation/viewSoldRooms');
			}
		}
		function cashSale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}	
			$id=(int)$this->uri->segment(3);
			$sale_id=(int)$this->uri->segment(4);
			//Check if room is empty and them alert to user
			$room_status_qry="SELECT status FROM rooms WHERE status='EMPTY' AND room_id=$id";
		if (!$this->accomodation_model->roomStatus($room_status_qry)) {
			$this->session->set_userdata('success','Please Check-Out Room First');
			redirect('accomodation/viewSoldRooms');
		}
		//continue with cash sale
			$query2="UPDATE soldroom SET status=0 WHERE room_id=$id AND roomsale_id=$sale_id";
				if($this->accomodation_model->update($query2)){
					$this->session->set_userdata('success','Payment Successfull');
					redirect('accomodation/viewSoldRooms');
				}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('accomodation/viewSoldRooms');
			}
		}
	function editSale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_changes'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('oldroomid','id','required');
			$this->form_validation->set_rules('sale_id','id','required');
			if ($this->form_validation->run()==TRUE) {
				$saleid=$this->input->post('sale_id');
				$oldroomid=$this->input->post('oldroomid');
				$newroomid=$this->input->post('newroomid');
				$discount=$this->input->post('discount');
				$room_info=$this->accomodation_model->getRoomInfo_from_soldRooms($oldroomid,$saleid);
				$oldprice=$room_info[0]->price;
				//$start_time=$room_info[0]->day;
				if (!empty($discount)) {
					$newprice=(double)$oldprice-$discount;
					$query="UPDATE soldroom SET price=$newprice WHERE room_ID=$oldroomid AND roomsale_id=$saleid";
				if($this->accomodation_model->update($query)){
					$this->session->set_userdata('success','Operation Successfull');
					redirect(site_url('accomodation/viewSoldRooms'));
				}
				}
				elseif (!empty($newroomid)) {
					$query="UPDATE soldroom SET room_ID=$newroomid,status=1 WHERE room_ID=$oldroomid  AND roomsale_id=$saleid";
				if($this->accomodation_model->update($query)){
					$qr="UPDATE rooms SET status='FULL' WHERE room_id=$newroomid";
				if($this->accomodation_model->update($qr)){
					$qr="UPDATE rooms SET status='EMPTY' WHERE room_id=$oldroomid";
				if($this->accomodation_model->update($qr)){
					$this->session->set_userdata('success','Operation Successfull');
				redirect(site_url('accomodation/viewSoldRooms'));
				}
				
				}	
				}
				}
				else{
					redirect(site_url('accomodation/viewSoldRooms'));
				}			
			}
			else{
				redirect(site_url('accomodation'));
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM rooms WHERE room_id='$id' LIMIT 1";
		$result=$this->accomodation_model->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'accomodation/update','title'=>'Accomodation','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}	

}