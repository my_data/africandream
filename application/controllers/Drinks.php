<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Drinks extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('drink');
		$this->load->helper('function');
		$this->load->library('cart');
	}
	
	public function index(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$result=$this->drink->getall();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);
	}
	public function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_item'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name','drink Name','required');
			$this->form_validation->set_rules('category','drink category','required');
			$this->form_validation->set_rules('amount','drink Amount','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('cost','cost','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('name');
				$category=$this->input->post('category');
				$amount=$this->input->post('amount');
				$price=$this->input->post('price');
				$cost=$this->input->post('cost');
				$query="INSERT INTO drinks VALUES('$name','$category','$amount','',$price,$cost)";
				if($this->drink->saveitem($query)){
					$this->session->set_userdata('success','Drinks Added Successfull');
					redirect('drinks');
				}
			}
			if ($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('fail','Fill All Required Fields PLease!');
				redirect('drinks');
			}
		}
		else {
			redirect('drinks');
		}
	}

	public function search(){
		$search=$this->uri->segment(3);
		if (isset($_POST['search_drink'])) {
			$search=$this->input->post('keyword');
		}
		$query="SELECT * FROM drinks WHERE name LIKE '$search%' OR item_id LIKE '$search%'";
		$result=$this->drink->search($query);
		if (!$result) {
			redirect('drinks');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);

	}

	public function filter(){
		$no=$this->uri->segment(3);
		$result=$this->drink->filter($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/all','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'item_result'=>$result);
		$this->load->view('template',$data);
	}

	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_item'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name','name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('amount','amount','required');
			$this->form_validation->set_rules('item_id','id','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('cost','cost','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('name');
				$category=$this->input->post('category');
				$amount=$this->input->post('amount');
				$price=$this->input->post('price');
				$cost=$this->input->post('cost');
				$id=$this->input->post('item_id');
			$query="UPDATE drinks SET name='$name',category='$category',size='$amount',price='$price',cost='$cost' WHERE item_id='$id'";
				if($this->drink->update_item($query)){
				$this->session->set_userdata('success','Drink Update Successfull');
					redirect('drinks');
				}
				else{
				$this->session->set_userdata('fail','Fail! Item Is Up To Date');
				redirect('drinks');
				}
			}
			else{
				$this->session->set_userdata('fail','No Update Commited');
				redirect('drinks');
			}
		}
		else{
		$id=(int)$this->uri->segment(3);
		$query="SELECT * FROM drinks WHERE item_id=$id LIMIT 1";
		$result=$this->drink->get_item_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'drinks/update','title'=>'DRINKS','allowed_modules'=>$allowed_modules,'row'=>$result);
		$this->load->view('template',$data);
		}
	}

	public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->drink->remove_item($id)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('drinks');

		}
		redirect('drinks');	
	}

}