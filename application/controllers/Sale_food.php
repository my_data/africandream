<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sale_food extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('food');
		$this->load->library('cart');
		$this->load->model('report');
		$this->load->helper('food');
	}

	function index(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}		
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/sale','title'=>'Foods','allowed_modules'=>$allowed_modules);
		$this->load->view('template',$data);
	}

	function search(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['search_food'])) {
		$search=$this->input->post('keyword');
		if (empty($search)) {
			redirect('sale_food');
		}
		else{
		$query="SELECT * FROM food WHERE foodname LIKE '%$search%' OR category LIKE '%$search%'";
		$result=$this->food->search($query);
		if (!$result) {
			redirect('sale_food');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/sale','title'=>'Foods','allowed_modules'=>$allowed_modules,'food_result'=>$result);
		$this->load->view('template',$data);
		}
		}
		elseif (!empty($this->uri->segment(3))) {
			# code...
			$search=$this->uri->segment(3);
		$query="SELECT * FROM food WHERE foodname LIKE '%$search%' OR category LIKE '%$search%'";
		$result=$this->food->search($query);
		if (!$result) {
			redirect('sale_food');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/sale','title'=>'Foods','allowed_modules'=>$allowed_modules,'food_result'=>$result);
		$this->load->view('template',$data);
		}
		else{
			redirect('sale_food');
		}
		
		
	}

	function add_to_cart(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['add_to_cart'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('foodname','Name','required');
			$this->form_validation->set_rules('category','category','required');
			$this->form_validation->set_rules('amount','quantity','required');
			$this->form_validation->set_rules('price','price','required');
			$this->form_validation->set_rules('food_id','ID','required');
			if ($this->form_validation->run()==TRUE) {
				$name=$this->input->post('foodname');
				$category=$this->input->post('category');
				$price=(int)$this->input->post('price');
				$food_id=$this->input->post('food_id');
				$qty=$this->input->post('amount');
				$saler=$this->session->userdata('person_id');

		//check if food exist first in cart
				foreach ($this->cart->contents() as $food) {
					if ($food['id']==$food_id) {
				$food=array('rowid'=>$food['rowid'],'qty'=>$qty,'price'=>$price,'category'=>$category);
						if($this->cart->update($food)){
							$this->session->set_userdata('success','Operation Successfull');
							redirect('sale_food');
							
						}
					}
				}
		//add new food to cart
		$food=array('id'=>$food_id,'name'=>$name,'qty'=>$qty,'price'=>$price,'category'=>$category);
				if($this->cart->insert($food)){
					$this->session->set_userdata('success','Operation Successfull');
				redirect('sale_food');
					
				}
				
			}
			if ($this->form_validation->run()==FALSE) {
				$this->session->set_userdata('fail','All field required');
				redirect('sale_food');	
			}
		}
		else{
			redirect('sale_food');	
		}
		
	}

function remove_cart_item(){
	$rowid=$this->uri->segment(3);
	
	if($this->cart->remove($rowid)){
		$this->session->set_userdata('success','Operation Successfull');
		redirect('sale_food');
	}
}

function sale(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['salefood'])) {
			if ($this->cart->contents()) {
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$qty=$item['qty'];$food_id=$item['id'];
					$cgry=$item['category'];
					$saler=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$price=(double)$price;
					$customer=$this->input->post('customer');
					$time=date("Y-m-d",time());
$query="INSERT INTO foodSales VALUES('','$time','$name',$food_id,'$cgry',$qty,$saler,$price,1,'$customer')";
			$this->food->saleFood($query);	
			}
			$this->cart->destroy();
			}
			$this->session->set_userdata('success','Operation Successfull');
			redirect('sale_food');	
		}
		elseif (isset($_POST['suspendfood'])) {
			if ($this->cart->contents()) {
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$qty=$item['qty'];$food_id=$item['id'];
					$cgry=$item['category'];
					$saler=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$price=(double)$price;
					$customer=$this->input->post('customer');
					$time=date("Y-m-d",time());
$query="INSERT INTO foodSales VALUES('','$time','$name',$food_id,'$cgry',$qty,$saler,$price,0,'$customer')";
			$this->food->saleFood($query);
			}
			$this->cart->destroy();
			}
			$this->session->set_userdata('success','Operation Successfull');
			redirect('sale_food');
		}
	elseif (isset($_POST['order_food'])) {
			if ($this->cart->contents()) {
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$qty=$item['qty'];$food_id=$item['id'];
					$saler=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$price=(double)$price;
					$customer=$this->input->post('customer');
					$time=date("Y-m-d",time());
					$query="INSERT INTO food_order VALUES('',$food_id,$qty,$price,$saler,'$customer','$time',0)";
					$this->food->saleFood($query);
			}
			$this->cart->destroy();
			$this->session->set_userdata('success','Operation Successfull');
			redirect('sale_food');
			}
			else{
				redirect('sale_food');
			}	
		}
	elseif (isset($_POST['complementary'])) {
			if ($this->cart->contents()) {
				foreach ($this->cart->contents() as $item) {
					$name=$item['name'];$qty=$item['qty'];$food_id=$item['id'];
					$cgry=$item['category'];
					$saler=(int)$this->session->userdata('person_id');
					$price=($item['qty']*$item['price']);
					$price=(double)$price;
					$customer=$this->input->post('customer');
					$time=date("Y-m-d",time());
$query="INSERT INTO foodSales VALUES('','$time','$name',$food_id,'$cgry',$qty,$saler,$price,2,'$customer')";
			$this->food->saleFood($query);
			}
			$this->cart->destroy();
			}
			$this->session->set_userdata('success','Complementary Added');
			redirect('sale_food');
		}
		else{
			redirect('sale_food');
		}
		
	}

	function creditSales(){
		if($this->food->get_suspended_sales()==false){
			$this->index();
		}
		$suspended=$this->food->get_suspended_sales();
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'restaurant/suspended_sales','title'=>'Suspended Sales','allowed_modules'=>$allowed_modules,'suspended'=>$suspended);
		$this->load->view('template',$data);

	}

	function unsuspend(){
		$id=(int)$this->uri->segment(3);
		$sale_time=date('Y-m-d',time());
		$query="UPDATE foodSales SET status=1,sale_time='$sale_time' WHERE sale_id=$id";
		
		if ($this->food->remove_suspended($query)){
			$this->session->set_userdata('success','Operation Successfull');
			redirect('sale_food/creditSales');
		}
	}

		function remove_suspended_sale(){
			$id=$this->uri->segment(3);
			if ($this->food->delete_suspended($id)) {
				$this->session->set_userdata('success','Operation Successfull');
				redirect('sale_food/suspended');
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
			redirect('sale_food/suspended');
			}
		}
	function todaySales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['todaySales'])) {
		$today=date("Y-m-d",time());
		$saler=(int)$this->session->userdata('person_id');
		$total_cash="SELECT SUM(price) AS value_sum FROM foodSales WHERE status=1 AND sale_time='$today' AND saler_id=$saler";
		$item_sold="SELECT  DISTINCT item_id FROM foodSales WHERE status=1 AND sale_time='$today' AND saler_id=$saler";
		$total_cash=$this->report->sales($total_cash);
		$sold_items_id=$this->report->sales($item_sold);
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='restaurant/todaySales';$data['title']="Today Sales";$data['cash']=$total_cash;
	$data['sold_items_id']=$sold_items_id;$this->load->vars($data);
	$this->load->view("template",$data);
		}
		else{
			redirect('sale_food');
		}
	}
	function allFoodSales(){
			if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['allFoodSales'])) {
	$data['allowed_modules']=$this->module->get_allowed_modules($this->session->userdata('person_id'));
	$data['page']='food_report/all_food_sales';$data['title']="ALL SALES";
	$this->load->vars($data);
	$this->load->view("template",$data);
		}
		else{
			redirect('food_orders');
		}
		}
	function saleOrder(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		$order_id=(int)$this->uri->segment(3);
		if (empty($order_id)) {
			# code...
			redirect('food_orders');
		}
		$query="SELECT food_order.food_id,qty,category,saler_id,food_order.price,customer FROM food_order inner join food on food_order.food_id=food.food_id WHERE food_order.order_id=$order_id";
		if ($item=$this->food->get_order_particular($query)) {
					$name=$item[0]['name'];
					$item_id=(int)$item[0]['food_id'];
					$qty=(int)$item[0]['qty'];
					$cgry=$item[0]['category'];
					$saler=(int)$item[0]['saler_id'];
					$price=(double)$item[0]['price'];
					$customer=$item[0]['customer'];
					$time=date("Y-m-d",time());
		$query="INSERT INTO foodSales VALUES('','$time','$name',$item_id,'$cgry',$qty,$saler,$price,1,'$customer')";
			if($this->food->saleFood($query)){
				$query1="UPDATE food_order SET status=1 WHERE order_id=$order_id";
					if($this->food->update_food($query1)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('food_orders');
					}	
					else{
						$this->session->set_userdata('fail','Operation fail');
						redirect('food_orders');
					}			
		}
	}
	else{
		redirect('food_orders');
	}
	}

	function updateSaleFood(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
			}
		if (isset($_POST['updateSale'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('food_id','Name','required');
			$this->form_validation->set_rules('sale_id','category','required');
			$this->form_validation->set_rules('qty','category','required');
			if ($this->form_validation->run()==TRUE) {
				$food_id=$this->input->post('food_id');
				$sale_id=$this->input->post('sale_id');
				$qty=$this->input->post('qty');
				$retail_price=$this->food->getPrice($food_id);
				$total_price=(double)$qty*$retail_price;
				$qry="UPDATE foodSales SET price=$total_price,amount=$qty WHERE sale_id=$sale_id";
				$this->food->updateFoodSale($qry);
			}
			else{
				$this->session->set_userdata('fail','Operation fail');
				redirect('food_orders');
			}
		}
		else{
			redirect('food_orders');
		}
	}

}//end of controller