<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Other_income extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('employee');	
		$this->load->model('module');
		$this->load->model('income');
	}

	function index(){
	if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$income=$this->income->getall();		
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'income','title'=>'Income','allowed_modules'=>$allowed_modules,'income_result'=>$income);
		$this->load->view('template',$data);

	}

	function save(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['save_income'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('description','description','required');
			$this->form_validation->set_rules('money','money','required');
			if ($this->form_validation->run()==TRUE) {
				$des=$this->input->post('description');
				$source=$this->input->post('source');
				$money=(int)$this->input->post('money');
				$day=date("Y-m-d",time());
				$emp=$this->session->userdata('person_id');
				$query="INSERT INTO income VALUES('','$des',$money,'$day','$emp','$source')";
				if($this->income->saveData($query)){
						
						$this->session->set_userdata('success','Operation Successfull');
						redirect('Other_income');
					}
				else{
					$this->session->set_userdata('fail','Operation fail');
					redirect('Other_income');
				}
			}
		}
		else{
						redirect('Other_income');
				}
	}

	public function search(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$search=$this->uri->segment(3);
		$query="SELECT * FROM income WHERE des LIKE '$search%' OR day LIKE '$search%'";
		$income=$this->income->search_income($query);
		if (!$income) {
			redirect('Other_income');
		}
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'expenditure','title'=>'Expenditure','allowed_modules'=>$allowed_modules,'income_result'=>$income);
		$this->load->view('template',$data);
	}
	public function filter(){
		$no=$this->uri->segment(3);
		$exp=$this->income->filter_income($no);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'expenditure','title'=>'Expenditure','allowed_modules'=>$allowed_modules,'income_result'=>$exp);
		$this->load->view('template',$data);
	}

	function update(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		if (isset($_POST['update_income'])) {
			$id=$this->uri->segment(3);
			$this->load->library('form_validation');
			$this->form_validation->set_rules('description','description','required');
			$this->form_validation->set_rules('money','cost','required');
			$this->form_validation->set_rules('income_id','id','required');
			if ($this->form_validation->run()==TRUE) {
				$des=$this->input->post('description');
				$id=$this->input->post('income_id');
				$money=(int)$this->input->post('money');
				$query="UPDATE income SET des='$des',fund=$money WHERE income_id='$id'";
				if($this->income->update_income($query)){
						$this->session->set_userdata('success','Operation Successfull');
						redirect('Other_income');
					}
				else{
					$this->session->set_userdata('fail','Operation fail');
						redirect('Other_income');
				}
			}
		}
		else{
		$id=$this->uri->segment(3);
		$query="SELECT * FROM income WHERE income_id='$id' LIMIT 1";
		$income=$this->income->get_income_particular($query);
		$allowed_modules=$this->module->get_allowed_modules($this->session->userdata('person_id'));
		$data=array('page'=>'update_income','title'=>'Update Income','allowed_modules'=>$allowed_modules,'row'=>$income);
		$this->load->view('template',$data);
		}
	}
	public function delete(){
		if(!$this->employee->is_logged_in()){
			redirect('login');
		}
		$id=$this->uri->segment(3);
		if($this->income->remove_income($id)){
			$this->session->set_userdata('success','Operation Successfull');
				redirect('Other_income');

		}	
	}
}
